package main_app;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ParseCSV {

	AnswerCard answerKey = new AnswerCard();
	AnswerCard answerCard = new AnswerCard();
	List<AnswerCard> listAnswerCard = new ArrayList<>();
    public int nrOfQu=0, nrOp=0;
    
	public ParseCSV(String nameFile) 
	{

		Scanner scan = new Scanner(System.in);
		//String nameFile = "";
        String line = "";
        String cvsSplitBy = ";";
        //System.out.print("Wprowadz arkusz z poprawnymi odpowiedziami: ");
       // nameFile = scan.nextLine();
        boolean [] tabAnswers = new boolean[10000];
       // String [] tabAnswers = new String[1000];
       // boolean chosenAns;
        String[] chosen;
        String csvFile = "D:\\Dokumenty\\eclipse-workspace\\Lab01\\plikCsv\\"+nameFile;
      //  csvFile=csvFile.concat(nameFile);
        //System.out.print(csvFile);

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
        	
            while ((line = br.readLine()) != null) 
            {
            	
                chosen = line.split(cvsSplitBy);
                
              // for (int i=0; i<chosen.length; i++)
              //  System.out.print(chosen[i]+"..." + i + "\n");
                
                for (int i=0; i<chosen.length; i++)
                {
                	tabAnswers[i] = Boolean.parseBoolean(chosen[i]);
                	if(nrOp==i)
                		nrOp++;
                }
                	
                	
               // tabAnswers[i] = chosen[i];
                
              //  for (int i=0; i<chosen.length; i++)
               // System.out.print(tabAnswers[i] + "--" + i +"\n");
                nrOfQu++;
                //System.out.print(nrOp);
                answerCard.SetOptionsQuestions(nrOfQu, nrOp);
                answerCard.ReadAnswerCard(tabAnswers);
                
            }

        } catch (IOException e) 
        {
            e.printStackTrace();
        }
		
	}
	
	public int GetNrOp()
	{
		return nrOp;
	}
	
	public int GetNrQue()
	{
		return nrOfQu;
	}
	public AnswerCard GetAnswerCard()
	{
		return answerCard;
	}
	
}
