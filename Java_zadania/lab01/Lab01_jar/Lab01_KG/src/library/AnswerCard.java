package library;

import java.util.List;
import java.util.ArrayList;


/**
 * Klasa karty odpowiedzi
 * @author Klaudia Gora
 */
public class AnswerCard {

		int numberOfQuestions = 0;
		int numberOfOptions = 0;
		List<ChosenAnswer> listOfAnswers = new ArrayList<>();
		ChosenAnswer chosenAnswer;
		
		/**
		 * Konstruktor dla losowego tworzenia kart odpowiedzi
		 * @param nrOfQuestions
		 * @param nrOfOptions
		 */
		public AnswerCard (int nrOfQuestions, int nrOfOptions)
		{
			numberOfQuestions = nrOfQuestions;
			numberOfOptions = nrOfOptions;
			chosenAnswer = new ChosenAnswer(nrOfOptions);
			
			for (int i=0; i<nrOfQuestions; i++)
			{
				chosenAnswer.SetRandomAnswer();
				listOfAnswers.add(chosenAnswer);
			}
			
		}
		
		/**
		 * Pusty konstruktor
		 */
		public AnswerCard()
		{
			 
		}
		
		/**
		 * Funkcja pobierajaca ilosc pytan oraz ilosc opcji odpowiedzi
		 * @param nrOfQuestions ilosc pytan 
		 * @param nrOfOptions ilosc opcji
		 */
		public void SetOptionsQuestions(int nrOfQuestions, int nrOfOptions)
		{
			numberOfQuestions = nrOfQuestions;
			numberOfOptions = nrOfOptions;
			chosenAnswer = new ChosenAnswer(nrOfOptions);
		}
		
		
		/**
		 * Funkcja ktora przyjmuje tablice odpowiedzi wczytanych z pliku oraz przypisuje je do wybranych odpowiedzi obiektu ChosenAnswer 
		 * @param tabAnswers tablica wczytanych odpowiedzi
		 */
		public void ReadAnswerCard (boolean[] tabAnswers)
		{	
			//for (int i=0; i<numberOfOptions; i++)
              //  System.out.print(tabAnswers[i]  +"\n");
			
				chosenAnswer = new ChosenAnswer(numberOfOptions);
				chosenAnswer.SetAnswer(tabAnswers);
				
				listOfAnswers.add(chosenAnswer);
			
		}
		
		/**
		 * Funkcja wypisujaca karte odpowiedzi
		 */
		public void DisplayCard()
		{
			for (int i=0; i<listOfAnswers.size(); i++)
			{
				System.out.print("\nZad " + i + ":\n");
				for(int j = 0; j< numberOfOptions; j++)
				{
					System.out.print(listOfAnswers.get(i).chosenAnswer[j] + " ");
				}
			}
		}
		
		
		
}
