package library;
import java.util.List;
import java.util.ArrayList;

/** 
 * Zestaw kart testowych true/false
 * @author Klaudia Gora 226174
 */

public class AnswerCardSet {

	List<AnswerCard> listAnswerCard = new ArrayList<>();
	AnswerCard answerCard;
	AnswerCard answerKey;
	public int nrOfQuestions;
	public int nrOfOptions;
	public int nrSet;
	
	/**
	 * Pusty konstruktor dla wczytywanego zestawu
	 */
	public AnswerCardSet()
	{
		
	}
	
	/**
	 * Konstruktor dla losowego zestawu kart 
	 * @param numberOfSet liczba zestawow
	 * @param nrOfQuestions liczba pytan w zestawie
	 * @param nrOfOptions liczba odpowiedzi
	 */
	public AnswerCardSet(int numberOfSet, int nrOfQuestions, int nrOfOptions) 
	{
		this.nrOfQuestions = nrOfQuestions;
		this.nrOfOptions = nrOfOptions;
		this.nrSet = numberOfSet;
		answerKey = new AnswerCard(nrOfQuestions, nrOfOptions);
		
		
		for (int i = 0; i<numberOfSet; i++)
		{
			answerCard = new AnswerCard(nrOfQuestions, nrOfOptions);
			listAnswerCard.add(answerCard);
		}
	}
	
	/**
	 * Funkcja przypisujaca karte do listy
	 * @param answerCard karta z odpowiedziami
	 */
	public void AddToListSet(AnswerCard answerCard) 
	{
		listAnswerCard.add(answerCard);
	}
	
	/**
	 * Funkcja przypisujaca karte klucz do listy
	 * @param answerKey karta klucz
	 */
	public void AddAnswerKey(AnswerCard answerKey)
	{
		this.answerKey = answerKey;
	}
	
	/**
	 * Funkcja generujaca losowe zestawy
	 * @param numberOfSet
	 * @param answerC
	 * @param answerK
	 */
	public void ReadenAnswerCardSet(int numberOfSet, AnswerCard answerC, AnswerCard answerK) 
	{
		this.nrOfQuestions = nrOfQuestions;
		this.nrOfOptions = nrOfOptions;
		nrSet = numberOfSet;
		answerKey = answerK;
		
		
		for (int i = 0; i<numberOfSet; i++)
		{
			answerCard = new AnswerCard(nrOfQuestions, nrOfOptions);
			listAnswerCard.add(answerCard);
		}
	}
	
	/**
	 * Funkcja przypisujaca 
	 * @param nrQ ilosc pytan
	 * @param nrO ilosc opcji
	 * @param nrS ilosc zestawow
	 */
	public void TakeNrOpAndNrQue(int nrQ, int nrO, int nrS) {
		nrOfQuestions = nrQ;
		nrOfOptions = nrO;
		nrSet = nrS;
	}
	
	/**
	 * Funkcja zwracajaca karte
	 * @param index numer karty
	 * @return listAnswerCard.get(index) karte odpowiedzi o danych indeksie
	 */
	public AnswerCard GetCard(int index)
	{
		return listAnswerCard.get(index);
	}
	
	/**
	 * Funkcja zwracajaca liste odpowiedzi
	 * @return listAnswerCard liste odpowiedzi
	 */
	public List <AnswerCard> GetList()
	{
		return listAnswerCard;
	}
	
	/**
	 * Funkcja zwracajaca karte klucz
	 * @return answerKey karte klucz
	 */
	public AnswerCard GetKeyCard()
	{
		return answerKey;
	}
	
	/**
	 * Funkcja wyswietlajaca karte klucz
	 */
	public void DisplayKeyCard()
	{
		answerKey.DisplayCard();
	}
	
	/**
	 * Fukcja wyswietlajaca karty
	 */
	public void DisplayCard()
	{
		for(int i=0; i<nrSet; i++)
		answerCard.DisplayCard();
	}
}
