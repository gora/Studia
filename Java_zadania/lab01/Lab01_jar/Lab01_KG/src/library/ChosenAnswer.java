package library;

import java.util.Random;

/**
 * Klasa wybranych odpowiedzi
 * @author Klaudia
 *
 */
public class ChosenAnswer {

	int nrOfOptions = 0;
	public boolean chosenAnswer[];
	
	/**
	 * Konstruktor przypisujacy ilosc mozliwych opcji
	 * @param nrOfOptions numer odpowiedzi
	 */
	ChosenAnswer(int nrOfOptions)
	{
		this.nrOfOptions = nrOfOptions;
		chosenAnswer = new boolean[nrOfOptions];
	}
	
	/**
	 * Funkcja ustawiajaca losowe odpowiedzi
	 */
	public void SetRandomAnswer() 
	{
		Random random= new Random();
		
		for (int i=0; i<nrOfOptions; i++)
		{
			chosenAnswer[i] =  random.nextBoolean();
		}
	}
	
	/**
	 * Funkcja przypisujaca wczytane odpowiedzi 
	 * @param tabAnswers tablica odpowiedzi
	 */
	public void SetAnswer(boolean[] tabAnswers) 
	{
		for(int i=0; i<nrOfOptions; i++)
		chosenAnswer[i] = tabAnswers[i];
	}
}
