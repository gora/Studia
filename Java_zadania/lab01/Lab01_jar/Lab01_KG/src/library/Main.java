package library;

import java.util.Scanner;

public class Main {

	public static void main (String[] args)
	{
		int nrSet = 5;
		
		AnswerCardSet aCardSet = new AnswerCardSet();
		Scanner s = new Scanner(System.in);
		StatisticsCalculation statistic = new StatisticsCalculation();
		ScoreHistogram scoreH;
		String nameFile;
		AnswerCard answerCard, answerKey;
		for(int i=0; i<nrSet; i++)
		{
			System.out.print("\n\nZestaw" +i+ ":");
			nameFile = "new" +i +".csv";
			ParseCSV parse = new ParseCSV(nameFile);
			answerCard = parse.GetAnswerCard();
			aCardSet.AddToListSet(answerCard);
			answerCard.DisplayCard();
		}
		
		nameFile = "new" + nrSet +".csv";
		ParseCSV parse = new ParseCSV(nameFile);
		answerKey = parse.GetAnswerCard();
		aCardSet.AddAnswerKey(answerKey);
		System.out.print("\n\nKarta Odpowiedzi: ");
		answerKey.DisplayCard();
		aCardSet.TakeNrOpAndNrQue(parse.GetNrQue(), parse.GetNrOp(), nrSet);
		
		
		
		//AnswerCard  ans;
		//ParseCSV parse = new ParseCSV();
		//ans = parse.GetAnswerCard();
		//ans.DisplayCard();

		
		
		/*System.out.print("Ilosc zestawow: ");
		nrCardSet = s.nextInt();
		System.out.print("Ilosc pytan: ");
		nrQuestions = s.nextInt();
		System.out.print("Ilosc odpowiedzi: ");
		nrAnswers = s.nextInt();
		
		aCardSet = new AnswerCardSet(nrCardSet, nrQuestions, nrAnswers);
		//aCardSet.SetAnswerCardAndKeyCardSet();*/
		statistic.ScoreReceived(aCardSet);
		scoreH = new ScoreHistogram(statistic);
		scoreH.DisplayAmountOfMark2();
	}
}
