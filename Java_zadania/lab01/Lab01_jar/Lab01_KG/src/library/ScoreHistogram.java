package library;

/**
 * Klasa obliczania ilosci osob ktore nie zaliczyly testu oraz wyswietlajaca histogram z ocen
 * @author Klaudia
 *
 */
public class ScoreHistogram {

	int amount2 = 0, amount3 = 0, amount35 = 0, amount4 = 0, amount45 = 0, amount5 = 0, amount55=0;
	
	/**
	 * Funkcja zliczajaca oceny i wyswietlajaca z nich histogram 
	 * @param statCalculation 
	 */
	public ScoreHistogram(StatisticsCalculation statCalculation)
	{
		
		for(int i=0; i< statCalculation.listOfScore.size(); i++)
		{
				if(statCalculation.listOfScore.get(i) == 2)
					amount2 ++;
				else if(statCalculation.listOfScore.get(i) == 3)
					amount3 ++;
				else if(statCalculation.listOfScore.get(i) == 3.5)
					amount35 ++;
				else if(statCalculation.listOfScore.get(i) == 4)
					amount4 ++;
				else if(statCalculation.listOfScore.get(i) == 4.5)
					amount45 ++;
				else if(statCalculation.listOfScore.get(i) == 5)
					amount5 ++;
				else if(statCalculation.listOfScore.get(i) == 5.5)
					amount55 ++;

		}
		System.out.print("\n");
		System.out.print("2:    " );
		for(int i=0; i< amount2; i++)
			System.out.print("*");
		System.out.print("\n");
		
		System.out.print("3:    " );
		for(int i=0; i< amount3; i++)
			System.out.print("*");
		System.out.print("\n");
		
		System.out.print("3.5:  " );
		for(int i=0; i< amount35; i++)
			System.out.print("*");
		System.out.print("\n");
		
		System.out.print("4:    " );
		for(int i=0; i< amount4; i++)
			System.out.print("*");
		System.out.print("\n");
		
		System.out.print("4.5:  " );
		for(int i=0; i< amount45; i++)
			System.out.print("*");
		System.out.print("\n");
		
		System.out.print("5:    " );
		for(int i=0; i< amount5; i++)
			System.out.print("*");
		System.out.print("\n");
		
		System.out.print("5.5:   " );
		for(int i=0; i< amount55; i++)
			System.out.print("*");
		System.out.print("\n");
		

	}
	
	/**
	 * Funkcja wyswietlajaca ilosc niezaliczonych prac
	 */
	public void DisplayAmountOfMark2()
	{
		System.out.print("\nIlosc niezaliczonych prac: " +amount2);
	}
}
