package library;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa wyliczajaca punkty i oceny z testow
 * @author Klaudia
 *
 */
public class StatisticsCalculation {

	AnswerCardSet aCard;
	AnswerCard ansCard, keyCard;
	List<Double> listOfScore = new ArrayList<Double>();
	List<Integer> listOfGoodAnswers = new ArrayList<Integer>();
	public float nrOpt =0, nrQu = 0;
	
	/**
	 * Konstruktor wyliczajacy punkty i oceny
	 * @param aCard zestaw kart
	 */
	public void ScoreReceived( AnswerCardSet aCard)
	{
		int goodAnswers = 0;
		double procent = 0, mark =0;
		keyCard = aCard.GetKeyCard();
		nrOpt = aCard.nrOfOptions;
		nrQu = aCard.nrOfQuestions;

		for(int k=0; k< aCard.nrSet; k++)
		{
			ansCard = aCard.GetCard(k);
			for (int i=0; i<aCard.nrOfQuestions; i++)
			{
				
				for(int j=0; j < aCard.nrOfOptions; j++)
				if(keyCard.listOfAnswers.get(i).chosenAnswer[j] == ansCard.listOfAnswers.get(i).chosenAnswer[j])
					goodAnswers++;
			}
			procent = (goodAnswers/(nrQu*nrOpt))*100;
			
			listOfGoodAnswers.add(goodAnswers);
			
			if(procent >= 98 )
				mark = 5.5;
				else if(procent >= 91 && procent <=97)
					mark = 5;
				else if(procent >= 61 && procent <=90)
					mark = 4.5;
				else if(procent >= 51 && procent <=60)
					mark = 4;
				else if(procent >= 41 && procent <=50)
					mark = 3.5;
				else if(procent >= 25 && procent <=40)
					mark = 3;
				else 
					mark = 2;
			
			listOfScore.add(mark);
			goodAnswers = 0;
		}
		System.out.print("\n");
	}
	
	/**
	 * Funkcja zwracajaca liste ocen
	 * @return listOfScore lista ocen
	 */
	public List<Double> GetScoreList()
	{
		return listOfScore;
	}
	
	/**
	 * Funkcja wyswietlajaca liste ocen
	 */
	public void DisplayScoreList()
	{
		for (int i=0; i<aCard.nrSet; i++)
		{
			System.out.print(i + ": " + listOfScore.get(i) +"\n");
		}
	}
	
	/**
	 * Funkcja zwracajaca liste punktow
	 * @return
	 */
	public List<Integer> GetGoodAnswersList()
	{
		return listOfGoodAnswers;
	}
}
