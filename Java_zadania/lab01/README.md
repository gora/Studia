Lab 1. Własna biblioteka i javadoc.

Należy napisać program do analizy kartami testów wyboru. Aplikacja powinna składać się z biblioteki pomocniczej 
(w postaci pliku .jar) oraz aplikacji głównej wykorzystującej tę bibliotekę.


Biblioteka powinna zawierać: 

Klasę karty odpowiedzi. Powinna zawierać informacje (np. w postaci odpowiednie kolekcji) jakie (które) odpowiedzi 
zostały zaznaczone na karcie. Liczba pytań oraz liczba możliwych opcji (stała dla każdego pytania) powinny być parametrami klasy.

Klasę zestawu kart odpowiedzi (testu, egzaminu itp.). Jest to kolekcja kart odpowiedzi z wyszczególnioną kartą, która stanowi klucz odpowiedzi.

Klasy do wyliczania statystyk. Statystyki powinny być wyliczane dla danego zestawu kart. Statystykami tymi mogą być 
(wystarczą 2 lub 3): histogram (rozkład) zdobytych punktów ("wykres" liczby kart z zestawu dla których zdobyto daną liczbę punktów), 
histogram ocen, histogram średniej poprawności odpowiedzi na poszczególne pytania, liczba prac niezaliczonych, 
średnia liczba punktów zdobytych przez studenta, pytanie, na które studenci odpowiadali najgorzej/najlepiej.

Aplikacja może być w postaci tekstowej (konsolowej). Aplikacja powinna mieć dokumentację (stworzoną narzędziem javadoc). 
Aplikacja powinna umożliwiać wczytanie zestawu kart odpowiedzi oraz klucza odpowiedzi (specjalna karta) oraz wyliczanie 
wybranych statystyk dla wczytanego zestawu.


Zestaw kart powinien być zapisany w katalogu (tzn. jeden katalog na jeden zestaw/test/egzamin). Poszczególne karty powinny 
być przechowywane w formacie .csv. Sposób nazywania plików i sposobu oznaczania pliku klucza jest dowolny.