Lab 2. Okienka, internacjonalizacja, zasoby.

Wymagania: umiejętność tworzenia aplikacji okienkowych (Swing lub JavaFX itp.), tworzenie zasobów (w celu utworzenia ikony aplikacji), 
techniki wspierania internacjonalizacji/ustawień lokalizacji w Java'ie.


Należy napisać aplikację okienkową służacą do zarządzania kolekcją obiektów (książek, płyt, samochodów itp.). 
Obiekty są zapisane w plikach na dysku (nie ma specjalnych założeń odnośnie sposobu/formatu zapisu). 
Aplikacja powinna umożliwiać co najmniej odczyt repozytorium z dysku i jego przeglądanie. 
Obiekty powinny posiadać kilka atrybutów, z czego przynajmniej jeden powinien być obrazkiem, 
co najmniej jeden powinien być wyrażony w jakiejś jednostce (długość, cena itp.), zaś co najmniej jeden powinien być datą.


Interfejs aplikacji powinien umożliwiać na zmianę danych lokalizacyjnych (język oraz kraj) na bieżaco. 
Zmiana ustawień powinna wpływać na sposób prezentacji danych w tym nazw jednostek i sposobu zapisu (np. kropka dziesiętna lub przecinek). 
Poza tym należy stosować określenia stosowne do liczby (np. w języku polskim: 1 obiekt, 2 obiekty, 5 obiektów, w języku angielskim 1 object, 2 objects) 
przy użyciu mechanizmu wariantów lub innych technik. Aplikacja powinna wspierać co najmniej ustawienia lokalizacyjne polski/Polska, angielski/USA i angielski/Wielka Brytania.


Aplikacja powinna wykorzystywać zasoby w celu zdefiniowania ikony aplikacji.