package package_app;

public class Book {

	String title;
	String date;
	Float price;
	String nameImage;
	
	public Book(String title, String date, String price1, String nameImage)
	{
		this.date = date;
		price = Float.parseFloat(price1);
		this.title = title;
		this.nameImage = nameImage;
		
	}
}
