package package_app;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ParserTxt {

    String [] tableBook; 
    Book book;
    List <Book> listOfBook = new ArrayList<>();
    
	public ParserTxt(String nameFile)
	{
		Scanner scan = new Scanner(System.in);
        String line = "";
        String cvsSplitBy = ";";        
        String csvFile = "D:\\Dokumenty\\eclipse-workspace1\\Lab02_Kg\\repo\\"+nameFile+".txt";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
        	
            while ((line = br.readLine()) != null) 
            {
            	
            	tableBook = line.split(cvsSplitBy);
                	
            	for(int i=0; i<tableBook.length; i++)
            	{
            		System.out.print(tableBook[i]+ "\n");
            	}
            		
                book = new Book(tableBook[0], tableBook[1], tableBook[2], tableBook[3]);
                listOfBook.add(book);
            }

        } catch (IOException e) 
        {
            e.printStackTrace();
        }
		
	}
	
	public void DisplayListOfBook()
	{
    	for(int i=0; i<listOfBook.size(); i++)
    	{
    		System.out.print(listOfBook.get(i).title + ", " + listOfBook.get(i).date + ", " + listOfBook.get(i).price + ", " + listOfBook.get(i).nameImage + ", " + "\n");
    	}
	}
	
	public String GetNameOfImage(int i)
	{
		return listOfBook.get(i).nameImage;
	}
	
	public List<Book> GetListBook()
	{
		return listOfBook;
	}
}
