package package_app;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.ListSelectionModel;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

//import javafx.scene.control.Alert;

import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Window_Application {

	private JFrame frame;
	private ParserTxt parse;
	String[] columnNames = {"Tytul","Data", "Cena"};  
	DefaultTableModel model = new DefaultTableModel(columnNames,0);  
	private JTable table = new JTable(model);


	Locale[] locales = { new Locale("en", "US"), new Locale("pl", "PL")}; 
	static Locale currentLocale = Locale.getDefault();
	public static ResourceBundle myResources;

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window_Application window = new Window_Application();
					window.frame.setVisible(true);
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window_Application() {
		initialize();
		myResources = ResourceBundle.getBundle("leb", currentLocale);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setBounds(100, 100, 511, 329);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JComboBox comboBoxFile = new JComboBox();
		comboBoxFile.setModel(new DefaultComboBoxModel(new String[] {"plik1", "plik2"}));
		comboBoxFile.setBounds(10, 11, 170, 22);
		frame.getContentPane().add(comboBoxFile);
		
		JButton btnAdd = new JButton("Dodaj");
		JLabel lblImage = new JLabel("Zdjecie");
		
		JComboBox comboBoxLang = new JComboBox();
		comboBoxLang.setModel(new DefaultComboBoxModel(new String[] {"PL", "ENG"}));
		comboBoxLang.setBounds(312, 11, 177, 22);
		frame.getContentPane().add(comboBoxLang);
		JButton btnChoose = new JButton("Wybierz");
		btnChoose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
	        	if(comboBoxLang.getSelectedIndex() > - 1)
				{
	        		switch(comboBoxLang.getSelectedIndex())
	        		{
	        			case 0:
	        			{
	        				currentLocale = locales[1];
	        		        break;
	        			}
	        				
	        			case 1:
	        			{
	        				currentLocale = locales[0];
	        		        break;
	        			}
	        		}
	        		
	        		myResources = ResourceBundle.getBundle("leb", currentLocale);
	        		btnChoose.setText(myResources.getString("chooseKey"));
	        		btnAdd.setText(myResources.getString("addKey"));
	        		lblImage.setText(myResources.getString("imaKey"));
	        		

	                 Enumeration<TableColumn> e = table.getColumnModel().getColumns();
	                 int i =0;
	                 while( e.hasMoreElements( ) )
	                 {
	                	 if(i==0)
	                	   e.nextElement().setHeaderValue(myResources.getString("titKey"));
	                	 if(i==1)
	                		 e.nextElement().setHeaderValue(myResources.getString("datKey"));
	                	 if(i==2)
	                		 e.nextElement().setHeaderValue(myResources.getString("priKey"));
	                	 i++;
	                 }
	                   
	                 table.getTableHeader().repaint();
	                 
				}
	        }
		});
		btnChoose.setBounds(385, 43, 104, 23);
		frame.getContentPane().add(btnChoose);
		
		

		lblImage.setBackground(new Color(255, 255, 255));
		lblImage.setHorizontalAlignment(SwingConstants.CENTER);
		lblImage.setIcon(null);
		lblImage.setBounds(363, 79, 126, 170);
		frame.getContentPane().add(lblImage);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane.setBounds(20, 77, 330, 206);
		frame.getContentPane().add(scrollPane);
		

		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = table.getSelectedRow();
				System.out.print(row);
				String nameImage = parse.GetNameOfImage(row);
				ImageIcon imgThisImg = new ImageIcon("D:\\Dokumenty\\eclipse-workspace1\\Lab02_Kg\\repo\\"+nameImage);
				lblImage.setIcon(imgThisImg);
				//lblImage.setDisabledIcon(imgThisImg);
			}
		});
		scrollPane.setViewportView(table);
		
		
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				model.getDataVector().removeAllElements();
				model.fireTableDataChanged(); 
				
				String nameFile = comboBoxFile.getSelectedItem().toString();
				parse = new ParserTxt(nameFile);
				List <Book> listOfBook = new ArrayList<>();
				listOfBook = parse.GetListBook();
					
				for(Book book: listOfBook)
				{
					table.getModel();
					System.out.print(book.title+ ", " + book.date + ","+ book.price + "\n");
					model.addRow(new Object [] {book.title, book.date, book.price});
				}
			}
		});
		btnAdd.setBounds(190, 11, 87, 23);
		frame.getContentPane().add(btnAdd);
		
	}
}
