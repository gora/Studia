Lab 3. Słabe/miękkie referencje i wątki.


Wymagania: wiedza dotycząca działania i użycia słabych i miękkich referencji. 
Tworzenie i synchronizacja wątków w Java'ie (także narzędzia typu java.util.concurrent).


Należy utworzyć metodę, której argumentem będzie liczba int/long (pełniąca funkcję ziarna) i która generuje kolekcję wartości typu double. 
Lista powinna być duża – rozmiar powinien zależeć od ziarna i być w zakresie od jednego do kilku megabajtów. 
Wartości w kolekcji powinny być zależne od ziarna tzn. dwukrotne użycie tego samego ziarna wygeneruje taką samą kolekcję (rozmiar i wartości w kolekcji).


Nalezy utworzyć pamięć podręczną dla tworzonych kolekcji: kluczem jest wartość ziarna, a wartością kolekcja odpowiadająca temu ziarnu. 
Należy wykorzystać mechanizm miękkich referencji, w celu zapewnienia, że Garbage Collector w przypadku braku wolnej pamięci usunie część wpisów, 
by zrobić miejsce na nowy wpis. Jak działanie pamięci podręcznej zmieni się przy użyciu zwykłych (silnych) lub słabych referencji (zamiast miękkich)?


Należy utworzyć kilka (co najmniej 5) wątków. Każdy wątek periodycznie próbuje obliczyć pewną statystykę dla kolekcji o losowym ziarnie. 
Statystyki mogą być różnorodne: średnia, wariancja, rozstęp, minimum, maksimum, mediana, liczba elementów dodatnich itp. 
Wątki pracują na pamięci podręcznej: jeśli wątek ma obliczyć statystykę dla kolekcji nie będącej obecnie w pamięci podręcznej, 
to musi najpierw tą kolekcję wygenerować (z ziarna) i umieścić ją w pamięci. Dostęp do pamięci podręcznej powinien być kontrolowany (synchronizacja).


Każdy wątek powinien drukować log swojej działalności (obliczanie statystyki, umieszczanie kolekcji w pamięci podręcznej) 
do pliku (osobnego dla każdego wątku) oraz na konsolę (wspólną dla wszystkich wątków). Ponadto należy wykorzystać odpowiednie mechanizmy 
(np. kolejki referencji) by zliczać sytuacje, w których jakaś kolekcja została usunięta z pamięci podręcznej. Pod koniec pracy aplikacji 
na konsoli powinna się pojawić informacja ile razy nastąpiło usunięcie wpisu z pamięci podręcznej.


Ograniczyć możliwe wartości ziarna (np. od 0 do kilku tysięcy). W razie potrzeby należy wykorzystać opcje maszyny wirtualnej Java'y służące 
do kontrolowania rozmiaru sterty (-Xms, -Xmx), w celu ułatwienia obserwacji działania miękkich referencji.