package app;

public class Collection {
	
	public int key;
	public double [] value;
	
	public Collection()
	{
		this.key = 0;
	}
	
	public Collection(int seed)
	{
		this.key = seed;
		value = new double[seed*1000];
		
		for (int j=0; j<seed*1000; j++)
		{
			value[j]=1.5*(seed^10)*100000000;
		}
	}
	

}
