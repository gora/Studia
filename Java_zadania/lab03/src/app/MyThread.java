package app;


import java.io.FileWriter;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;



public class MyThread extends Thread{
	
	double score=0, sum=0, amount =0;
	int seed =0;
	double[] value;
	Collection coll;
	private final ReentrantLock lock = new ReentrantLock(false);
	boolean create = false;
	boolean exist = false;
	//AtomicInteger amountOfNull = new AtomicInteger();
	int amountNull =0;
	String name = null;
	
	public MyThread(String name) 
	{
		this.name = name;
	}

	/*public synchronized int getCountNull()
	{
		return amountOfNull++;
	}*/
	
	public void WriteToFile(String text) throws IOException
	{
		FileWriter write = new FileWriter(name);
		write.write(text);
        write.close();
	}
	
	@Override
	public void run() {	
		lock.lock();;
		int k=0;
		try
		{
			FileWriter writeFile = new FileWriter(name);

			while(k<200)
			{
				synchronized(Main.listNull)
				{
					synchronized(Main.listColl)
					{
						int seed = ThreadLocalRandom.current() .nextInt(1, 5000);
						this.seed = seed;
						int size = Main.listColl.size();
						for(int i=0; i<size; i++)
						{
							coll = (Collection)Main.listColl.get(i).get();
							if(coll == null)
							{
								amountNull++;
								//amountOfNull.addAndGet(amountNull);
								Main.listColl.remove(i);
								size --;
								i--;
							}
							else
							{
								if(coll.key == seed)
								{
									exist = true;
									
								}
							}
		
						}
						
						if(exist == false)
						{
							coll = new Collection(seed);
							Main.listColl.add(new SoftReference<Collection>(coll));
							//Thread.sleep(100);
							create = true;
						}
						
						for(int i=0; i< coll.value.length; i++)
						{
							sum += coll.value[i];
							amount ++;
						}
						score = sum/amount;
						if(create == true)
						{
							String b =Thread.currentThread().getName() +" ("+seed+") - srednia:  " + score +" --- tworzenie kolekcji ---ilosc usunietych kolekcji " + amountNull + "\n";
							System.out.print(b);
							writeFile.write(b);
							writeFile.write(System.lineSeparator());
						}
						else
							System.out.print(Thread.currentThread().getName() +" ("+seed+") - srednia:  " + score +"---ilosc usunietych kolekcji " + amountNull + "\n");
						exist = false;
						create = false;
						k++;
					}
				}
			}
			writeFile.close();
			Main.listNull.add(amountNull);
			int sum=0;
			for(int i=0; i<Main.listNull.size();i++)
			{
				sum +=Main.listNull.get(i);
			}
			System.out.println("---ilosc usunietych kolekcji---- " + sum + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {

			lock.unlock();
		}
	}
}
