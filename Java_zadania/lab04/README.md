Lab 4. Refleksja i ładowanie klas.


Wymagania: wiedza dotycząca działania mechanizmu refleksji oraz ładowania klas (ewentualnie własne ładowacze klas).


Należy napisać program podający dokładne lub przybliżone rozwiązanie problemu komiwojażera (zakładamy graf pełny). 
Program powinienen posiadać możliwość wczytania grafu z dysku (format danych dowolny: może to być pełna macierz, 
lista współrzędnych punktów, itp.). Dane grafu można przechowywać w programie jako jednowymiarową tablicę.


Początkowo aplikacja nie powinna posiadać żadnych algorytmów rozwiązania problemu komiwojażera. 
Algorytmy te powinny być dostarczane w postaci skompilowanych plików class. Klasy te należy dodawać do programu 
i wykorzystywać z użyciem mechanizmu refleksji i ładowaczy klas. Interfejs aplikacji powinien umożliwić załadowanie nowych algorytmów, 
umożliwiać użycie aktualnie załadowanych algorytmów i wyświetlać informacje o nich (klasy algorytmów powinny mieć metody zawierające nazwę i krótki opis algorytmu).


Możliwymi do wykorzystania algorytmami są: przegląd zupełny, algorytm najbliższego sąsiada, algorytm losowy, algorytm 2-opt itp. 
Należy też zaimplementować algorytm bestOf, który powinien wykonywać co najmniej 2 z poprzednich algorytmów i zwracać lepszy z wyników 
(klasa algorytmu bestOf musi odpalać metody klas tych 2 innych algorytmów).


Kod bajtowy ładowanych klas powinien być umieszczany w dedykowanym do tego celu katalogu. 
Metody ładowanych klas mogą mieć atrybuty typu podstawowego, String lub tablice jednowymiarowych tych typów. 
Załadowane klasy powinno dać się wyładować, czego potwierdzenie powinno dać się zauważyć korzystając z jconsole.