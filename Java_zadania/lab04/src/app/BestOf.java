package app;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BestOf {

	String betterDistance;
	int [][]adjacencyMatrix;
	
	public BestOf(int [][]matrix)
	{
		this.adjacencyMatrix = matrix;
		Class c = BruteForceSolver.class;
		String className = c.getName();
		System.out.print(className+"\n");

		Method [] classMethods = c.getMethods();
		for(Method m: classMethods)
		{
			System.out.print("Metoda - " + m.getName() + "\n");
		}
		
		Object con = null;

				try {
					con = c.getConstructor(new Class[] {int[][].class}).newInstance(adjacencyMatrix);
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
				Method alg = null;
				try {
					alg = c.getDeclaredMethod("getBestCost", null);
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 int bestCost = 0;
				 try {
					bestCost = (int) alg.invoke(con, null);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


				 //------------------------
				 
					Class c2 = NearestNeighborSolver.class;
					String className2 = c2.getName();
					System.out.print(className2+"\n");

					Method [] classMethods2 = c2.getMethods();
					for(Method m: classMethods2)
					{
						System.out.print("Metoda - " + m.getName() + "\n");
					}
					
					Object con2 = null;

							try {
								con2 = c2.getConstructor(new Class[] {int[][].class}).newInstance(adjacencyMatrix);
							} catch (InstantiationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (NoSuchMethodException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SecurityException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					
							Method alg2 = null;
							try {
								alg2 = c2.getDeclaredMethod("getBestCost", null);
							} catch (NoSuchMethodException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SecurityException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							 int bestCost2 = 0;
							 try {
								bestCost2 = (int) alg2.invoke(con2, null);
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	

							System.out.print("1: " + bestCost + ", 2: " + bestCost2 + "\n");
							if (bestCost<bestCost2)
							{
								betterDistance = className + " : " + bestCost;
							}
							else 
								betterDistance = className2 + " : " + bestCost2;
	}
	
	public String getBestCost() 
	{	
		return betterDistance;
	}
}
