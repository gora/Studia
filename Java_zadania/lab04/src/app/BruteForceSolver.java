package app;

import java.util.ArrayList;
import java.util.List;

public class BruteForceSolver {

	  	private ArrayList<Integer> cities = new ArrayList<>();
	    private ArrayList<List<Integer>> permutations;
	    private ArrayList<Integer> shortestTour;
	    private int shortestDistance;
	    private StringBuilder sb;
	    int [][] adjacencyMatrix; 
	    int total = 0;
	    public BruteForceSolver(int [][]matrix) {
	    	
	    	this.adjacencyMatrix = matrix;
	        permutations = new ArrayList<List<Integer>>();
	        for(int i=0; i<matrix.length; i++)
	        	cities.add(i);
	        
	        permutations.add(cities);
	        shortestTour = new ArrayList<Integer>();
	        shortestDistance = 0;
	        
	        generatePermutations();
	    }

	    public void generatePermutations() {
	        int lastItemIndex = cities.size() - 1;
	         
	        
	        shortestDistance = getTotalDistance(cities);
	        shortestTour.clear();

	        
	        for (int j = 0; j < cities.size(); j++) {

	            for (int trail = lastItemIndex; trail >= 0; trail--) {

	                int head = (trail - 1);
	                if (head >= 0) {
	                    if (cities.get(trail) > cities.get(head)) {
	                        int lastSlot = lastItemIndex;
	                        while (cities.get(head) > cities.get(lastSlot)) {
	                            lastSlot--;
	                        }
	                        swap(lastSlot, head);
	                        sort(head);
	                        trail = lastItemIndex + 1;
	                    
	                        int totalDistance = getTotalDistance(cities);
	                        //System.out.println(cities.toString() + " Dist.:" + totalDistance);

	                        if (shortestDistance > totalDistance) {
	                            shortestDistance = totalDistance;
	                            shortestTour.clear();
	                            shortestTour.addAll(cities);
	                        }
	                        
	                    }
	                }
	            }
	        }
	    }

	    public int getBestCost(){
	        return shortestDistance;
	    }
	    
	    public ArrayList<Integer> getShortestTour(){
	        return shortestTour;
	    }
	    
	    private void sort(int m) {
	        int index = (cities.size() - 1);
	        int n = m;
	        for (int i = m + 1; i < index; i++) {
	            for (int j = n + 1; j < index; j++) {
	                if (cities.get(j) > cities.get(j + 1)) {
	                    swap(j, (j + 1));
	                }
	            }
	        }
	    }

	    private int getFactorial(int n) {
	        int fact = 1;
	        for (int i = 1; i <= n; i++) {
	            fact = fact * i;
	        }
	        return fact;
	    }

	    private void swap(int j, int k) {
	        Integer tmp = cities.get(j);
	        cities.set(j, cities.get(k));
	        cities.set(k, tmp);
	    }
	    
	    private int getTotalDistance(ArrayList<Integer> cities) {
	        
	        
	        for (int i = 0; i < cities.size()-1; i++) {

	        		total += getDistance(cities.get(i), cities.get(i+1));
	             
	        }

	        return total;
	    }
	    
	    
	    private int getDistance(int i,  int j) {
	        return adjacencyMatrix[i][j];
	    }
}
