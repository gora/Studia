package app;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.ScrollPane;
import java.awt.TextArea;

public class Main {

	private JFrame frame;
	ParserTxt parser;
	ArrayList<Integer> shortestTour;
	ArrayList<Object> listOfObjects;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 543, 316);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JComboBox cBoxFile = new JComboBox();
		cBoxFile.setModel(new DefaultComboBoxModel(new String[] {"p1", "p2"}));
		cBoxFile.setBounds(119, 23, 186, 22);
		frame.getContentPane().add(cBoxFile);
		
		JButton btnWybierz = new JButton("Wybierz");
		btnWybierz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parser = new ParserTxt(cBoxFile.getSelectedItem().toString());
				
				//NearestNeighborSolver nn = new NearestNeighborSolver(parser.getMatrix());
               // nn.getBestCost();
				
				//BruteForceSolver bruteForce = new BruteForceSolver(parser.getMatrix());
             //   int rr = bruteForce.getBestCost();
				
				// listOfObject.add(obj);
				
				
			}
		});
		btnWybierz.setBounds(333, 22, 97, 25);
		frame.getContentPane().add(btnWybierz);
		
		JLabel lblWynik = new JLabel("Wynik 1:");
		lblWynik.setBounds(12, 124, 56, 16);
		frame.getContentPane().add(lblWynik);
		
		JLabel lblWynik_1 = new JLabel("Wynik 2:");
		lblWynik_1.setBounds(12, 153, 56, 16);
		frame.getContentPane().add(lblWynik_1);
		
		JLabel lblOne = new JLabel("");
		lblOne.setBounds(119, 124, 56, 16);
		frame.getContentPane().add(lblOne);
		
		TextArea textArea = new TextArea();
		textArea.setBounds(224, 99, 291, 162);
		frame.getContentPane().add(textArea);
		
		JLabel lblTwo = new JLabel("");
		lblTwo.setBounds(119, 153, 56, 16);
		frame.getContentPane().add(lblTwo);
		
		JLabel lblThree = new JLabel("");
		lblThree.setBounds(12, 211, 243, 16);
		frame.getContentPane().add(lblThree);
		
		JLabel lblNajlepszyWynik = new JLabel("Najlepszy wynik:");
		lblNajlepszyWynik.setBounds(12, 182, 114, 16);
		frame.getContentPane().add(lblNajlepszyWynik);
		
		JLabel lblWybierzPlik = new JLabel("Wybierz plik:");
		lblWybierzPlik.setBounds(12, 26, 81, 16);
		frame.getContentPane().add(lblWybierzPlik);
		
		JLabel lblWybierzKlase = new JLabel("Wybierz klase:");
		lblWybierzKlase.setBounds(12, 60, 114, 16);
		frame.getContentPane().add(lblWybierzKlase);
		
		JComboBox comboBoxClass = new JComboBox();
		comboBoxClass.setModel(new DefaultComboBoxModel(new String[] {"BruteForce", "NearestNeighbor", "Best of"}));
		comboBoxClass.setBounds(119, 58, 186, 22);
		frame.getContentPane().add(comboBoxClass);
		
		JButton btnDodaj = new JButton("Dodaj");
		btnDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (comboBoxClass.getSelectedItem().toString()=="BruteForce")
				{	
					Class c = BruteForceSolver.class;
					String className = c.getName();
					String text;
					System.out.print(className+"\n");
					text = className +"\n";
					Method [] classMethods = c.getMethods();
					int i=0;
					for(Method m: classMethods)
					{
						if (i<3)
						text += m.getName() + "\n";
						i++;
						//System.out.print("Metoda - " + m.getName() + "\n");
					}
					text+= "\nMetoda przegląda wszystkie stworzone podzbiory\n następnie wybiera z nich najlepsze rozwiazanie\n\n"
							+ "Funkcja przetwarza tylko macierze n x n\n";
					textArea.setText(text);
					Object con = null;

							try {
								con = c.getConstructor(new Class[] {int[][].class}).newInstance(parser.getMatrix());
							} catch (InstantiationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (NoSuchMethodException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SecurityException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					
							Method alg = null;
							try {
								alg = c.getDeclaredMethod("getBestCost", null);
							} catch (NoSuchMethodException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SecurityException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							 int bestCost = 0;
							 try {
								bestCost = (int) alg.invoke(con, null);
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							System.out.print(bestCost);
							String cost = Integer.toString(bestCost);
							lblOne.setText(cost);
							
				}
				else if (comboBoxClass.getSelectedItem().toString()=="NearestNeighbor")
				{
					Class c = NearestNeighborSolver.class;
					String className = c.getName();
					String text;
					System.out.print(className+"\n");
					text = className +"\n";
					Method [] classMethods = c.getMethods();
					int i=0;
					for(Method m: classMethods)
					{
						if (i<2)
						text += m.getName() + "\n";
						i++;
						//System.out.print("Metoda - " + m.getName() + "\n");
					}
					text+= "\nMetoda przeszukuje najbliższe sąsiedztwa\nnastępnie wybiera z nich najkrótszą ścieżkę\n\n"
							+ "Funkcja przetwarza tylko macierze n x n\n";
					textArea.setText(text);
					
					
					Object con = null;

							try {
								con = c.getConstructor(new Class[] {int[][].class}).newInstance(parser.getMatrix());
							} catch (InstantiationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (NoSuchMethodException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SecurityException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						Method alg = null;
							try {
								alg = c.getDeclaredMethod("getBestCost", null);
							} catch (NoSuchMethodException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SecurityException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							 int bestCost = 0;
							 try {
								bestCost = (int) alg.invoke(con, null);
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							System.out.print(bestCost);
							String cost = Integer.toString(bestCost);
							lblTwo.setText(cost);

				}
				else if (comboBoxClass.getSelectedItem().toString()=="Best of")
				{
					Class c = BestOf.class;
					String className = c.getName();
					String text;
					System.out.print(className+"\n");
					text = className +"\n";
					Method [] classMethods = c.getMethods();
					int i=0;
					for(Method m: classMethods)
					{
						if (i<1)
						text += m.getName() + "\n";
						i++;
						//System.out.print("Metoda - " + m.getName() + "\n");
					}
					
					text+= "\nMetoda porównuje algorytm \nBruteForce i NearestNeighbor,\na następnie wybiera lepszy wynik\n\n"
							+ "Funkcja przetwarza tylko macierze n x n\n";
					textArea.setText(text);
					Object con = null;

							try {
								con = c.getConstructor(new Class[] {int[][].class}).newInstance(parser.getMatrix());
							} catch (InstantiationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (NoSuchMethodException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SecurityException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						Method alg = null;
							try {
								alg = c.getDeclaredMethod("getBestCost", null);
							} catch (NoSuchMethodException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SecurityException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							 String bestCost = "";
							 try {
								bestCost = (String) alg.invoke(con, null);
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalArgumentException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							System.out.print(bestCost);
							lblThree.setText(bestCost);

				}
				
			}
		});
		btnDodaj.setBounds(333, 56, 97, 25);
		frame.getContentPane().add(btnDodaj);
	}
}
