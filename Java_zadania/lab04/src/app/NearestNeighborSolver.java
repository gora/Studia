package app;

import java.util.ArrayList;

public class NearestNeighborSolver {

	// final Tour tour;
	    final int[][] adjacencyMatrix;
	    
	    ArrayList<Integer> visitedCities;
	    ArrayList<Integer> bestTourSoFar;
	    
	    int tmpTourCost;
	    int costOfBestTourSoFar;

	    
	    public NearestNeighborSolver(int [][]matrix) {
	    
	        this.adjacencyMatrix = matrix;
	        tmpTourCost = 0;
	        visitedCities = new ArrayList<Integer>();
	        bestTourSoFar = new ArrayList<Integer>();
	        costOfBestTourSoFar = 1000000000; 
	    }

	    public ArrayList<Integer> getShortestTour() {
	        for(int city = 0; city < adjacencyMatrix.length; city++){
	            determineShortestTour(city);
	        }
	        
	        //printSolution(bestTourSoFar);
	        System.out.println("\nTour cost = " + costOfBestTourSoFar);

	        return bestTourSoFar;
	    }
	    
	    public int getBestCost() {
	        for(int city = 0; city < adjacencyMatrix.length; city++){
	            determineShortestTour(city);
	        }

	        return costOfBestTourSoFar;
	    }

	    private void determineShortestTour(int node){

	        int initialNode = node;
	        ArrayList<Integer> tmpSolution = new ArrayList<Integer>();
	        visitedCities.clear();
	        tmpTourCost = 0;
	        
	        tmpSolution.add(node + 1); 
	        visitedCities.add(node); 
	        
	        while (visitedCities.size() < adjacencyMatrix.length) {
	           
	            node = getNearestNode(node);
	            visitedCities.add(node);
	            tmpSolution.add(node + 1);
	        }
	        
	        tmpTourCost += adjacencyMatrix[node][initialNode]; 
	  
	        // Update best-tour-so-far and its cost.
	        if (tmpTourCost < costOfBestTourSoFar) {
	            costOfBestTourSoFar = tmpTourCost;
	            bestTourSoFar = (ArrayList<Integer>) tmpSolution.clone();
	        }        
	        
	    }
	    
	    private int getNearestNode(int currentNode) {
	        double edge = -1.0;
	        int nearestNode = -1;


	        for (int i = (adjacencyMatrix.length - 1); i > -1; i--) {
	            
	            if (isMarkedVisited(i)){
	                continue;
	            }
	            
	            if (-1 == adjacencyMatrix[currentNode][i]){
	                continue;
	            }
	                        
	            if ((-1.0 == edge) && (-1 != adjacencyMatrix[currentNode][i])) {
	               edge = adjacencyMatrix[currentNode][i];
	            }            
	                        
	            if ((adjacencyMatrix[currentNode][i] <= edge)) { 
	                edge = adjacencyMatrix[currentNode][i];
	                nearestNode = i; 
	            }
	        }

	        tmpTourCost += adjacencyMatrix[currentNode][nearestNode];
	        return nearestNode;
	    }

	    private boolean isMarkedVisited(int node) {
	        boolean found = false;

	        for (int i = 0; i < visitedCities.size(); i++) {
	            if (node == visitedCities.get(i)) {
	                found = true;
	                break;
	            }
	        }
	        return found;
	    }//end of isMarkedVisited();
	    
	    private void printSolution(ArrayList<Integer> solution) {
	        double cost = 0.0;
	        double edge = 0.0;
	        int currentNode = -1;
	        int nextNode = -1;
	            
	        for (int i = 1; i < solution.size(); i++) {
	            currentNode = solution.get(i - 1);
	            nextNode = solution.get(i);           
	            
	            edge = adjacencyMatrix[currentNode -1][nextNode - 1];
	            cost += edge;
	            
	            System.out.println("Node: " + currentNode);            
	            System.out.println("Nearest Node: " + nextNode  + "; Edge: " + edge);
	            System.out.println();
	        }
	        currentNode = nextNode;
	        nextNode = solution.get(0);
	        edge = adjacencyMatrix[currentNode - 1][nextNode - 1];
	        cost += edge;      
	        
	        System.out.println("Node: " + currentNode);
	        System.out.println("Nearest Node: " + nextNode + "; Edge: " + edge);
	        
	    }
}
