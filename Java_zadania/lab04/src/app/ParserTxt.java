package app;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ParserTxt {

	//int N;
	String [][] matrixRead = new String[10][];
	int [][] matrix;
	int index=0;
	
	public ParserTxt(String nameFile)
	{

		String cvsSplitBy = " ";
        String line = "";
        String csvFile = "D:\\Dokumenty\\eclipse-workspace1\\Lab04_ladnowanieKlas\\repo\\"+nameFile+".txt";
        
		 try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
	        	
	            while ((line = br.readLine()) != null) 
	            {
	            	matrixRead[index] = line.trim().split(cvsSplitBy);
	            	index++;
	            }

	        } catch (IOException e) 
	        {
	            e.printStackTrace();
	        }
		 
		matrix = new int[matrixRead.length][matrixRead.length];
		 // System.out.println(matrixRead.length);
		 for(int i=0; i<matrixRead.length; i++)
		 {
			 for(int j=0; j<matrixRead.length; j++)
			 {
				
				 matrix[i][j] = Integer.parseInt(matrixRead[i][j]);
				// System.out.println(matrix[i][j]);
			 }
				
		 }
		 
		 for(int i=0; i<matrixRead.length; i++)
		 {
			 for(int j=0; j<matrixRead.length; j++)
				 System.out.print(matrix[i][j] + " ");
			 System.out.print("\n");
		 }
	}
	
	public int[][] getMatrix()
	{
		return matrix;
	}
	
}
