
package rmi.client;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientInterface extends Remote{
    public void messageFromServer(String message) throws RemoteException;
    public void updateUserList(String[] currentUsers) throws RemoteException;
    public String[] getFiles()throws RemoteException;
    void acceptFileChunk(byte[] bufor, String part) throws RemoteException;
    public void acceptFileChunkFull(byte[] bufor) throws RemoteException;
}
