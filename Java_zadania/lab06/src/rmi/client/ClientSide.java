
package rmi.client;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.ConnectException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import rmi.server.ServerInterface;


public class ClientSide extends UnicastRemoteObject implements ClientInterface{
    private static final long serialVersionUID = 7468891722773409712L;
    ClientVIEW rmiGUI;
    private String hostName = "localhost";
    private String serviceName = "GroupChatService";
    private String clientServiceName;
    private String name;
    protected ServerInterface serverInt;
    protected boolean connectionProblem = false;
    String directory = System.getProperty("user.dir") + "/Download";
    String listOfFIles[];
    

    public ClientSide(ClientVIEW rmiGUI, String userName) throws RemoteException
    {
        super();
        this.rmiGUI = rmiGUI;
        this.name = userName;
        this.clientServiceName = "ClientListenService_" + userName;
        listOfFIles = getFiles();
    }
    
    
    public void startClient() throws RemoteException, MalformedURLException, NotBoundException 
    {		
	String[] details = {name, hostName, clientServiceName};	
	try 
        {
            Naming.rebind("rmi://" + hostName + "/" + clientServiceName, this);
            serverInt = ( ServerInterface)Naming.lookup("rmi://" + hostName + "/" + serviceName);	
	} 
	catch (ConnectException  e) 
        {
            JOptionPane.showMessageDialog(null,"Problem z polaczeniem");
            connectionProblem = true;
            e.printStackTrace();
	}
	catch(NotBoundException | MalformedURLException me)
        {
            connectionProblem = true;
            me.printStackTrace();
	}
	if(!connectionProblem)
        {
            registerWithServer(details);
	}	
	System.out.println("Server otwarty\n");
    }
    
    public void registerWithServer(String[] details) 
    {		
	try
        {
            serverInt.passIdentity(this.ref);//now redundant ??
            serverInt.registerNode(details);			
	}
	catch(Exception e)
        {
            e.printStackTrace();
	}
    }

    
    @Override
    public void messageFromServer(String message) throws RemoteException 
    {
	System.out.println( message );
	rmiGUI.textAreaShow.append( message );
	rmiGUI.textAreaShow.setCaretPosition(rmiGUI.textAreaShow.getDocument().getLength());
    }

    @Override
    public void updateUserList(String[] currentUsers) throws RemoteException 
    {
        if(currentUsers.length < 2)
        {
            rmiGUI.btnPrivateSend.setEnabled(false);
	}
        rmiGUI.comboBoxUsers.removeAllItems();
	rmiGUI.setClientPanel(currentUsers);

    } 
    @Override
    public String[] getFiles()
    {
        File serverDir=new File(directory);
        String file[]=serverDir.list();
        return file;
    } 
    
    @Override
    public void acceptFileChunk(byte[] bufor, String part) throws RemoteException {
        File path = new File(System.getProperty("user.dir") +"/PlikiUsers/"+name+"/plik_"+part);
        System.out.print(path);
        FileOutputStream out = null;
        
        try {
            out = new FileOutputStream(path);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            out.write(bufor);
        } catch (IOException ex) {
            Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("\nDodalo sie");
        //Scanner bb = new Scanner(System.in);
    }
    
    @Override
    public void acceptFileChunkFull(byte[] bufor) throws RemoteException {
        //byte[] bufor2 = new byte[(int) bufor.length];
        File path = new File(System.getProperty("user.dir") +"/PlikiUsersFull/"+name+"/plik");
        System.out.print(path);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(path);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            out.write(bufor);
        } catch (IOException ex) {
            Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("\nDodalo sie");
        //Scanner bb = new Scanner(System.in);
    }
}
