/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmi.client;

import java.awt.BorderLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import static java.lang.System.in;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Klaudia
 */
public class ClientVIEW extends javax.swing.JFrame {
    private ClientSide chatClient;
    private String name, message;
    private DefaultListModel<String> listModel, listModalFile;
    private String fileList[];
    private byte[]buforIn;
    byte[] file;
    //private String  listFile[];
    /**
     * Creates new form Client
     */
    public ClientVIEW() {
        initComponents();
        //btnSend.setEnabled(false);
        btnPrivateSend.setEnabled(false);
        //btnDownload.setEnabled(false);
        String[] currClients={""};
        setClientPanel(currClients); 
        fileList = getFiles();
        setFilePanel(fileList);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        textFieldName = new javax.swing.JTextField();
        btnStart = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnPrivateSend = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        textAreaShow = new javax.swing.JTextArea();
        comboBoxUsers = new javax.swing.JComboBox<>();
        comboBoxFile = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("frame");

        btnStart.setText("connect");
        btnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartActionPerformed(evt);
            }
        });

        jLabel1.setText("Files:");

        jLabel2.setText("Users:");

        btnPrivateSend.setText("send file");
        btnPrivateSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrivateSendActionPerformed(evt);
            }
        });

        textAreaShow.setColumns(20);
        textAreaShow.setRows(5);
        jScrollPane4.setViewportView(textAreaShow);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(comboBoxFile, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(textFieldName)
                            .addComponent(comboBoxUsers, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnStart, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(btnPrivateSend, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(215, 215, 215)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(textFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnStart))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboBoxUsers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(comboBoxFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPrivateSend)))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartActionPerformed
        name = textFieldName.getText();
        if(name.length() != 0 && btnStart.getText()=="connect")
        {
                this.setTitle(name);
                createNewFolder(name);
            try {
                getConnected(name);
            } catch (RemoteException ex) {
                Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NotBoundException ex) {
                Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MalformedURLException ex) {
                Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
            }
			if(!chatClient.connectionProblem)
                        {
                            btnStart.setText("disconnect");
                            //btnSend.setEnabled(true);
			}
        }
        else if(btnStart.getText()=="disconnect")
        {
            try 
            {
                sendMessage(name + ": zamknal czat");
                chatClient.serverInt.leaveChat(name);
                System.exit(0);
            } 
            catch (RemoteException e) 
            {
                e.printStackTrace();
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Wprowadz imie");
        }
    }//GEN-LAST:event_btnStartActionPerformed

    
    private void btnPrivateSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrivateSendActionPerformed
   int item = comboBoxFile.getSelectedIndex();
   String fileList[] = getFiles();
   String path = "C:\\Users\\Klaudia\\Desktop\\NetBeansProjects\\Lab6_RMI-good\\Download\\" + fileList[item];
   file =null;
        try {
                file = chatClient.serverInt.downloadFileFromServer(path);
            } catch (RemoteException ex) {
                Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
            }
        try {
            chatClient.serverInt.uploadFile(file);
            chatClient.serverInt.broadcastMessageFull(file);
        } catch (RemoteException ex) {
            Logger.getLogger(ClientVIEW.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnPrivateSendActionPerformed
    
    private void sendMessage(String chatMessage) throws RemoteException 
    {
        chatClient.serverInt.updateChat(name, chatMessage);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClientVIEW.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClientVIEW.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClientVIEW.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClientVIEW.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ClientVIEW().setVisible(true);
            }
        });
        
        
    }

    
    private void getConnected(String userName) throws RemoteException, NotBoundException, MalformedURLException
    {
		//remove whitespace and non word characters to avoid malformed url
	String cleanedUserName = userName.replaceAll("\\s+","_");
	cleanedUserName = userName.replaceAll("\\W+","_");
	try 
        {		
            chatClient = new ClientSide(this, cleanedUserName);
            chatClient.startClient();
	} 
        catch (RemoteException e)
        {
            e.printStackTrace();
	}
    }
    

    

    
     public void setClientPanel(String[] currClients) {  	
        
        for(int i=0; i<currClients.length; i++){
            comboBoxUsers.addItem(currClients[i]);
        }
        if(currClients.length > 1){
        	btnPrivateSend.setEnabled(true);
        }  
    }
     
     public void setFilePanel(String [] currFile)
     {
       
         for(int i=0; i<currFile.length; i++){
            comboBoxFile.addItem(currFile[i]);
         }
        
     }
     
     private boolean createNewFolder(String folder) {
        try {
            String dir = System.getProperty("user.dir") + "/PlikiUsers/" + folder;
            String dir2 = System.getProperty("user.dir") + "/PlikiUsersFull/" + folder;
            boolean result = false;
            File directory = new File(dir);
            File directory2 = new File(dir2);
            
            if (!directory.exists()) {
                result = directory.mkdir();

                if (result) {
                    System.out.println("Stworzono folder");
                } else {
                    return false;
                }
            }
            
            if (!directory2.exists()) {
                result = directory2.mkdir();

                if (result) {
                    System.out.println("Stworzono folder");
                } else {
                    return false;
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        return true;
    }
     
    public String[] getFiles()
    {
        File serverDir=new File(System.getProperty("user.dir") + "/Download");
        String file[]=serverDir.list();
        return file;
    } 
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnPrivateSend;
    private javax.swing.JButton btnStart;
    public javax.swing.JComboBox<String> comboBoxFile;
    public javax.swing.JComboBox<String> comboBoxUsers;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    public javax.swing.JScrollPane jScrollPane4;
    public javax.swing.JTextArea textAreaShow;
    private javax.swing.JTextField textFieldName;
    // End of variables declaration//GEN-END:variables
}
