
package rmi.server;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.RemoteRef;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;
import java.util.Vector;
import rmi.client.ClientInterface;


public class ChatServer extends UnicastRemoteObject implements ServerInterface {
    String line = "---------------------------------------------\n";
    private Vector<Users> chatters;
    private static final long serialVersionUID = 1L;
	
	//Constructor
    public ChatServer() throws RemoteException {
	super();
	chatters = new Vector<Users>(10, 1);
    }
    
    public static void main(String[] args)
    {
	startRMIRegistry();	
	String hostName = "localhost";
	String serviceName = "GroupChatService";
		
	if(args.length == 2)
        {
            hostName = args[0];
            serviceName = args[1];
	}
		
	try
        {
            ServerInterface hello = new ChatServer();
            Naming.rebind("rmi://" + hostName + "/" + serviceName, hello);
            System.out.println("rmi://" + hostName + "/" + serviceName);
            System.out.println("Czat serwer rozpoczety");
	}
	catch(Exception e)
        {
            System.out.println("Problem z polaczeniem do servera");
	}	
}
    
    public static void startRMIRegistry() {
	try 
        {
            java.rmi.registry.LocateRegistry.createRegistry(1099);
            System.out.println("RMI Server gotowy");
	}
	catch(RemoteException e) 
        {
            e.printStackTrace();
	}
    }
    public void updateChat(String name, String nextPost) throws RemoteException 
    {
	String message =  name + " : " + nextPost + "\n";
	sendToAll(message);
    }
    
    // Receive a new client remote reference
    @Override
    public void passIdentity(RemoteRef ref) throws RemoteException 
    {	
        try
        {
            System.out.println(line + ref.toString());
	}
        catch(Exception e)
        {
            e.printStackTrace();
	}
    }//end passIDentity
    
    @Override
    public void registerNode(String[] details) throws RemoteException 
    {	
	System.out.println(new Date(System.currentTimeMillis()));
	System.out.println(details[0] + " has joined the chat session");
	System.out.println(details[0] + "'s hostname : " + details[1]);
	System.out.println(details[0] + "'sRMI service : " + details[2]);
	registerNode1(details);
    }
    
    private void registerNode1(String[] details)
    {		
        try
        {
            ClientInterface nextClient = ( ClientInterface )Naming.lookup("rmi://" + details[1] + "/" + details[2]);
            chatters.addElement(new Users(details[0], nextClient));
            sendToAll(details[0] + ": dolaczyl do rozmowy.\n");
            updateUserList();		
        }
        catch(RemoteException | MalformedURLException | NotBoundException e)
        {
            e.printStackTrace();
        }
    }
    
    private void updateUserList() 
    {
	String[] currentUsers = getUserList();	
	for(Users c : chatters)
        {
            try 
            {
		c.getClient().updateUserList(currentUsers);
            } 
            catch (RemoteException e) 
            {
		e.printStackTrace();
            }
	}	
    }
    
    private String[] getUserList()
    {
	// generate an array of current users
	String[] allUsers = new String[chatters.size()];
	for(int i = 0; i< allUsers.length; i++)
        {
            allUsers[i] = chatters.elementAt(i).getName();
	}
	return allUsers;
    }
    
    public void sendToAll(String newMessage)
    {	
	for(Users c : chatters)
        {
            try 
            {
		c.getClient().messageFromServer(newMessage);
            } 
            catch (RemoteException e) 
            {
		e.printStackTrace();
            }
	}	
    }
    
    @Override
    public void leaveChat(String userName) throws RemoteException
    {
	for(Users c : chatters)
        {
            if(c.getName().equals(userName))
            {
		System.out.println(line + userName + " zakonczyl sesje");
		System.out.println(new Date(System.currentTimeMillis()));
		chatters.remove(c);
		break;
            }
        }		
        if(!chatters.isEmpty())
        {
            updateUserList();
	}			
    }
    
    public synchronized void uploadFile(byte[] message) throws RemoteException 
    {
        String part = "";
        int len1 = message.length/3;
        int len2 = len1 * 2;
        int len3 = message.length;
        byte[] buf_temp = new byte[len1 + 1];
        int i=0;
        for(Users c : chatters)
        {
            if(i==0) {
                part = "1";
                for(int j=0; j<len1; j++) {
                    buf_temp[j] = message[j];
                }
            }
            if(i==1) {
                part = "2";
                for(int j=len1; j<len2; j++) {
                    buf_temp[j-len1] = message[j];
                }
            }
            if(i==2) {
                part = "3";
                for(int j=len2; j<message.length; j++) {
                    buf_temp[j-len2] = message[j];
                }
            }
            try 
            {
                if(i<3)
                {
                c.getClient().acceptFileChunk(buf_temp, part);
                c.getClient().messageFromServer(c.getName()+": wysłano plik\n");
                }
            } 
            catch (RemoteException e) 
            {
                e.printStackTrace();
            }
            i++;
        }
    }
    
    public synchronized void broadcastMessageFull(byte[] message) throws RemoteException 
    {
        for(Users c : chatters)
        {
            try 
            {
                c.getClient().acceptFileChunkFull(message);
            } 
            catch (RemoteException e) 
            {
                e.printStackTrace();
            }
        }
    }
    @Override
    public byte[] downloadFileFromServer(String serverpath) throws RemoteException {
					
        byte [] mydata;	

        File serverpathfile = new File(serverpath);			
        mydata=new byte[(int) serverpathfile.length()];
        FileInputStream in;
        try {
                in = new FileInputStream(serverpathfile);
                try {
                        in.read(mydata, 0, mydata.length);
                } catch (IOException e) {

                        e.printStackTrace();
                }						
                try {
                        in.close();
                } catch (IOException e) {

                        e.printStackTrace();
                }

        } catch (FileNotFoundException e) {

                e.printStackTrace();
        }		
			
	return mydata;
				 
	}

}
