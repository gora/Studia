
package rmi.server;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.RemoteRef;


public interface ServerInterface extends Remote {
    public void updateChat(String userName, String chatMessage)throws RemoteException;
    public void passIdentity(RemoteRef ref)throws RemoteException;
    public void registerNode(String[] details)throws RemoteException;
    public void leaveChat(String userName)throws RemoteException;
    void uploadFile(byte[] message) throws RemoteException;
    void broadcastMessageFull(byte[] message) throws RemoteException;
    public byte[] downloadFileFromServer(String servername) throws RemoteException;
}
