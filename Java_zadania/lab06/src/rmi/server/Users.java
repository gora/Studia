/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmi.server;

import rmi.client.ClientInterface;


public class Users {
    public String name;
    public ClientInterface client;
    public Users(String name, ClientInterface client){
		this.name = name;
		this.client = client;
    }
    
    //getters and setters
    public String getName() {
	return name;
    }
    public ClientInterface getClient(){
	return client;
    }
    
}
