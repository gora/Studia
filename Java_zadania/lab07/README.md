Lab 7. Bazy danych i XML w Java'ie.


Wymagania: wiedza o sposobie komunikacji z baza danych z użyciem JDBC oraz przetwarzania XML-a z użyciem JAXB.


Należy utworzyć prostą bazę danych SQL i zapełnić ją przykładowymi danymi (można użyć dowolnych narzędzi typu 
PHPMyAdmin czy prostych skryptów SQL-owych uruchamianych z konsoli). Warunkiem jest by baza zawierała co najmniej 3 tabele, 
z czego dwie powiązane relacją a jedną niepowiązaną (tą ostatnią może być tabela użytkowników z danymi logowania itp.). 
Tematyka bazy danych (urząd, szpital, hotel, warsztat itp.) dowolna.


Należy stworzyć prostą aplikację bazodanową z użyciem JDBC. Należy stworzyć przynajmniej po jednym przykładzie z następujących możliwości: 
wyświetlanie danych (SELECT), dodawanie danych do bazy (INSERT), modyfikacja danych w bazie (UPDATE). Mile widziane również logowanie. 
Należy zwracać uwagę na obsługę błędów i odpowiednią komunikację z użytkownikiem.


W tej samej aplikacji należy dodatkowo wykorzystać JAXB do importowania i eksportowania części danych z/do bazy z użyciem XML-a. 
Można do tego wykorzystać tabelę niepowiązaną (nie mającą relacji z innymi tabelami), jak tabela użytkowników.