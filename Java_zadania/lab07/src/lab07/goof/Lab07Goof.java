/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab07.goof;
import java.util.List;
import model.Student;
import model.Ocena;
import studia.Studia;
 
public class Lab07Goof {
    public static Studia b;
    public static List<Student> studenci;
    public static List<Ocena> oceny;
    
    public Studia getB(){
        return this.b;
    }
    
    public  Lab07Goof() {
        b = new Studia();
        b.insertStudenta("Karol", "Maciaszek", "Eka");
        b.insertStudenta("Piotr", "Wojtecki", "Eka");
        b.insertStudenta("Abdul", "Dabdul", "Eka");
 
        b.insertOcena("4.5", 2);
        b.insertOcena("5.0", 4);
        b.insertOcena("3.0", 5);
 
        studenci = b.selectStudenci();
        oceny = b.selectOceny();
 
        System.out.println("Lista studentow: ");
        for(Student c: studenci)
            System.out.println(c);
 
        System.out.println("Lista ocen:");
        for(Ocena k: oceny)
            System.out.println(k);
 
        b.closeConnection();
    }
}