package model;
 
public class Ocena {
    private int id;
    private int ects;
    private String ocena;
    
    private String autor;
 
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getEcts() {
        return ects;
    }
    public void setEcts(int ects) {
        this.ects = ects;
    }
    public String getOcena() {
        return ocena;
    }
    public void setOcena(String ocena) {
        this.ocena = ocena;
    }
 
    public Ocena() {}
    public Ocena(int id, String ocena, int ects) {
        this.id = id;
        this.ocena = ocena;
        this.ects = ects;
    }
 
    @Override
    public String toString() {
        return "["+id+"] - "+ocena+" - "+ects;
    }
}