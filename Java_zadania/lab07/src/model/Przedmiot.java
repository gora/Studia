package model;
 
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Przedmiot {
    private int id;
    private int idProwadzacego;
    private String nazwa;
 
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public int getIdProwadzacego() {
        return idProwadzacego;
    }
    public void setIdProwadzacego(int idProwadzacego) {
        this.idProwadzacego = idProwadzacego;
    }
    public String getNazwa() {
        return nazwa;
    }
    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
 
    public Przedmiot() {}
    public Przedmiot(int id, String nazwa, int idProwadzacego) {
        this.id = id;
        this.idProwadzacego = idProwadzacego;
        this.nazwa = nazwa;
 
    }
}