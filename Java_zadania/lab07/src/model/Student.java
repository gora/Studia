package model;
 
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Student {
    private int id;
    private String imie;
    private String nazwisko;
    private String kierunek;
 
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getImie() {
        return imie;
    }
    public void setImie(String imie) {
        this.imie = imie;
    }
    public String getNazwisko() {
        return nazwisko;
    }
    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }
    public String getKierunek() {
        return kierunek;
    }
    public void setKierunek(String kierunek) {
        this.kierunek = kierunek;
    }
 
    public Student() { }
    public Student(int id, String imie, String nazwisko, String kierunek) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.kierunek = kierunek;
    }
 
    @Override
    public String toString() {
        return "["+id+"] - "+imie+" "+nazwisko+" - "+kierunek;
    }
}