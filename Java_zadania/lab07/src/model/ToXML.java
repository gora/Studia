package model;


import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import model.Student;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Klaudia
 */
public class ToXML {
    private JAXBContext context;
    int i=0;

    
    public void serializationStu(int id, String imie, String nazwisko, String kierunek) throws JAXBException{
        this.context = JAXBContext.newInstance(Student.class);
        Marshaller marshaller = this.context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new Student(id, imie, nazwisko, kierunek), new File("D:\\Dokumenty\\Studia\\Semestr VI\\DPP\\jenkins\\jmath\\lab07-goof\\xml\\student"+i+".xml"));
        marshaller.marshal(new Student(id, imie, nazwisko, kierunek), System.out);
        i++;
    }
    
    public void serializationPro(int id, String imie, String nazwisko) throws JAXBException{
        this.context = JAXBContext.newInstance(Prowadzacy.class);
        Marshaller marshaller = this.context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new Prowadzacy(id, imie, nazwisko), new File("D:\\Dokumenty\\Studia\\Semestr VI\\DPP\\jenkins\\jmath\\lab07-goof\\xml\\prowadzacy"+i+".xml"));
        marshaller.marshal(new Prowadzacy(id, imie, nazwisko), System.out);
        i++;
    }
    
    public void serializationPrz(int id, String nazwa, int idp) throws JAXBException{
        this.context = JAXBContext.newInstance(Przedmiot.class);
        Marshaller marshaller = this.context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new Przedmiot(id, nazwa, idp), new File("D:\\Dokumenty\\Studia\\Semestr VI\\DPP\\jenkins\\jmath\\lab07-goof\\xml\\przedmiot"+i+".xml"));
        marshaller.marshal(new Przedmiot(id, nazwa, idp), System.out);
        i++;
    }
}
