package studia;
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
 
import model.Student;
import model.Ocena;
import model.Prowadzacy;
import model.Przedmiot;
 
public class Studia {
 
    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:studia4.db";
 
    private Connection conn;
    private Statement stat;
 
    public Studia() {
        try {
            Class.forName(Studia.DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
            e.printStackTrace();
        }
 
        try {
            conn = DriverManager.getConnection(DB_URL);
            stat = conn.createStatement();
        } catch (SQLException e) {
            System.err.println("Problem z otwarciem polaczenia");
            e.printStackTrace();
        }
 
        createTables();
    }
 
    public boolean createTables()  {
        String createStudenci = "CREATE TABLE IF NOT EXISTS studenci (id_studenta INTEGER PRIMARY KEY AUTOINCREMENT, imie varchar(255), nazwisko varchar(255), kierunek varchar(255))";
        String createOceny = "CREATE TABLE IF NOT EXISTS oceny (id_oceny INTEGER PRIMARY KEY AUTOINCREMENT, ocena varchar(255), ects int)";
        String createProwadzacy = "CREATE TABLE IF NOT EXISTS prowadzacy (id_prowadzacego INTEGER PRIMARY KEY AUTOINCREMENT, imie varchar(255), nazwisko varchar(255))";
        String createPrzedmiot = "CREATE TABLE IF NOT EXISTS przedmiot (id_przedmiotu INTEGER PRIMARY KEY AUTOINCREMENT, nazwa varchar(255), id_prowadzacego int)";
        try {
            stat.execute(createStudenci);
            stat.execute(createOceny);
            stat.execute(createProwadzacy);
            stat.execute(createPrzedmiot);
        } catch (SQLException e) {
            System.err.println("Blad przy tworzeniu tabeli");
            e.printStackTrace();
            return false;
        }
        return true;
    }
 
    public boolean insertStudenta(String imie, String nazwisko, String kierunek) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into studenci values (NULL, ?, ?, ?);");
            prepStmt.setString(1, imie);
            prepStmt.setString(2, nazwisko);
            prepStmt.setString(3, kierunek);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad przy wstawianiu czytelnika");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean insertProwadzacego(String imie, String nazwisko) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into prowadzacy values (NULL, ?, ?);");
            prepStmt.setString(1, imie);
            prepStmt.setString(2, nazwisko);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad przy wstawianiu prowadzacego");
            e.printStackTrace();
            return false;
        }
        return true;
    }
 
    public boolean insertOcena(String ocena, int ects) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into oceny values (NULL, ?, ?);");
            prepStmt.setString(1, ocena);
            prepStmt.setInt(2, ects);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad przy wypozyczaniu");
            return false;
        }
        return true;
    }
 
    public boolean insertPrzedmiot(int idProwadzacego, String nazwa) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into przedmiot values (NULL, ?, ?);");
            prepStmt.setString(1, nazwa);
            prepStmt.setInt(2, idProwadzacego);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad przy wypozyczaniu");
            return false;
        }
        return true;
    }
    
    public void deleteStudent(int id) throws SQLException {
            Statement state = conn.createStatement();
        try {
            state.execute("DELETE FROM studenci WHERE id_studenta=" + id + ";");
        } catch (SQLException ex) {
            Logger.getLogger(Studia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deletePrzedniot(int id) throws SQLException {
        Statement state = conn.createStatement();
        try {
            state.execute("DELETE FROM przedmiot WHERE id_przedmiotu=" + id + ";");
        } catch (SQLException ex) {
            Logger.getLogger(Studia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean editStudent(int id, String imie, String nazwisko, String kierunek){
        
        try {
            Statement state = conn.createStatement();
            state.execute("UPDATE studenci SET imie=\""+imie+"\", nazwisko=\""+nazwisko+"\", kierunek = \""+kierunek+"\" WHERE id_studenta ="+id);
            //state.execute("UPDATE studenci SET imie="Sofia", nazwisko="+nazwisko+", kierunek = "+kierunek+" WHERE id_studenta ="+id);

            } catch (SQLException ex) {
                Logger.getLogger(Studia.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        return true;
    }
 
    public List<Student> selectStudenci() {
        List<Student> studenci = new LinkedList<Student>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM studenci");
            int id;
            String imie, nazwisko, kierunek;
            while(result.next()) {
                id = result.getInt("id_studenta");
                imie = result.getString("imie");
                nazwisko = result.getString("nazwisko");
                kierunek = result.getString("kierunek");
                studenci.add(new Student(id, imie, nazwisko, kierunek));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return studenci;
    }
 
    public List<Ocena> selectOceny() {
        List<Ocena> oceny = new LinkedList<Ocena>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM oceny");
            int id, ects;
            String ocena;
            while(result.next()) {
                id = result.getInt("id_oceny");
                ocena = result.getString("ocena");
                ects = result.getInt("ects");
                oceny.add(new Ocena(id, ocena, ects));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return oceny;
    }
    
        public List<Przedmiot> selectPrzedmiot() {
        List<Przedmiot> przedmioty = new LinkedList<Przedmiot>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM przedmiot");
            int id,id_pro;
            String nazwa;
            while(result.next()) {
                id = result.getInt("id_przedmiotu");
                nazwa = result.getString("nazwa");
                id_pro = result.getInt("id_prowadzacego");
                przedmioty.add(new Przedmiot(id, nazwa, id_pro));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return przedmioty;
    }
        
        public List<Prowadzacy> selectProwadzacy() {
        List<Prowadzacy> prowadzacy = new LinkedList<Prowadzacy>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM prowadzacy");
            int id;
            String imie, nazwisko;
            while(result.next()) {
                id = result.getInt("id_prowadzacego");
                imie = result.getString("imie");
                nazwisko = result.getString("nazwisko");
                prowadzacy.add(new Prowadzacy(id, imie, nazwisko));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return prowadzacy;
    }
    
    
 
    public void closeConnection() {
        try {
            conn.close();
        } catch (SQLException e) {
            System.err.println("Problem z zamknieciem polaczenia");
            e.printStackTrace();
        }
    }
}