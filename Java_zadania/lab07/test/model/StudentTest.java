/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Klaudia
 */
public class StudentTest {
    
    private JAXBContext context;

    @Before
    public void init() throws JAXBException {
        this.context = JAXBContext.newInstance(Student.class);
    }

    
    @Test
    public void serialization() throws JAXBException{
        Marshaller marshaller = this.context.createMarshaller();
        marshaller.marshal(new Student(1, "Jan", "Nowak", "Eka"), new File("student.xml"));
        
    
    }
    
    
}
