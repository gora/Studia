Lab 8. Komunikacja sieciowa z użyciem SOAP.


Wymagania: wiedza o gniazdkach sieciowych, protokole HTTP, SOAP, XML i wdrażaniu aplikacji serwerowych. Można też korzystać z frameworku Apache Axis2 (z Java EE).


Należy zaprojektować warstwowy sieciowy system wymiany wiadomości, składający się z pewnej liczby osobnych aplikacji 
(konsolowych lub okienkowych) w architekturze takiej jak na poniższym obrazku. Strzałki reprezentują możliwy kierunek przekazywania wiadomości.


Zadanie należy zrealizowac z użyciem SOAP-a. Należy wykorzystać odpowiednie części koperty (envelope) SOAP-a: 
treść samej wiadomości powinna być przenoszona w ciele (body), zaś dane potrzebne do realizacji komunikacji powinny znaleźć się w nagłówku (header). 
Należy wykorzystać gniazdka TCP/IP (np. z wykorzystaniem pakietu javax.xml.soap.*) lub framework Apache Axis/Axis2.


System powinien wspierać 3 tryby adresowania: 

Unicast: dane wysyłane do konkretnego węzła.

Layer broadcast: dane wysyłane do wszystkich węzłów konkretnej warstwy.

Broadcast: dane wysyłane do wszystkich węzłów.

Interfejs (konsolowy lub okienkowy) każdej aplikacji powinien być podzielony na dwie części: 

Część do wysyłania wiadomości. Powinna umożliwić wybór adresata/trybu adresacji, zredagowanie wiadomości oraz jej wysłanie.

Część do odbierania wiadomości. Powinna wyświetlać odebrane wiadomości przeznaczone dla danego węzła wraz z nadawcą. 
Powinna też w osobny sposób (np. w nawiasach lub wyszarzone) wyświetlać informacje diagnostyczne np. fakt przesłania wiadomości dalej.
