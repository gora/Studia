import javax.xml.soap.SOAPHeader;

public interface IServer {
	void headerRecieved(SOAPHeader header);
	void messageRecieved(String message);
	void sendMessage(SOAPHeader destination);
}
