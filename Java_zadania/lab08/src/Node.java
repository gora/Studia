import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.xml.soap.*;
import static java.lang.System.out;
import org.w3c.dom.NodeList;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Map;
import java.util.TreeMap;
import java.awt.event.ActionEvent;

public class Node extends JFrame implements IServer{

	private JPanel contentPane;
	private JTextField textName;
	private JTextField textPort;
	private JTextField textLayer;
	private JTextField textDestination;
	private JTextField textMessage;
	private JLabel labelOutput;
	
	private String name;
	private int port;
	private String layer;
	private ServerSocket server;
	private Thread thread;
	private Map<String,Integer> neighbours;
	private SOAPHeader recievedDestination;
	private String message = "none";
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Node frame = new Node();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Node() {
		String test = "z2 x2 y2";
		String[] table = test.split(" ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 423, 262);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textName = new JTextField();
		textName.setBounds(57, 11, 86, 20);
		contentPane.add(textName);
		textName.setColumns(10);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(10, 14, 46, 14);
		contentPane.add(lblName);
		
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 65, 46, 14);
		contentPane.add(lblPort);
		
		JLabel lblLayer = new JLabel("Layer:");
		lblLayer.setBounds(10, 39, 46, 14);
		contentPane.add(lblLayer);
		
		textPort = new JTextField();
		textPort.setBounds(57, 62, 86, 20);
		contentPane.add(textPort);
		textPort.setColumns(10);
		
		textLayer = new JTextField();
		textLayer.setBounds(57, 36, 86, 20);
		contentPane.add(textLayer);
		textLayer.setColumns(10);
		
		JLabel lblOutput = new JLabel("Output:");
		lblOutput.setBounds(10, 102, 46, 14);
		contentPane.add(lblOutput);
		
		labelOutput = new JLabel("");
		labelOutput.setBounds(57, 102, 319, 14);
		contentPane.add(labelOutput);
		
		neighbours = new TreeMap<String, Integer>();
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				layer = textLayer.getText();
				name = textName.getText() + layer;
				port = Integer.parseInt(textPort.getText());
				btnSave.setEnabled(false);
				createNeighbours();
				try {
					MessageFactory factory = MessageFactory.newInstance();
					SOAPMessage soapMsg = factory.createMessage();
					SOAPPart part = soapMsg.getSOAPPart();
					SOAPEnvelope envelope = part.getEnvelope();
					recievedDestination = envelope.getHeader();
					recievedDestination.addTextNode("Destination");
					recievedDestination.setValue("none");
					soapMsg.writeTo(out);
				} catch (SOAPException e1) {
					e1.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				thread = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							server = new ServerSocket(port);
							while (true) {
								Socket sc = server.accept();
								InputStream is = sc.getInputStream();
								BufferedReader in = new BufferedReader(new InputStreamReader(is)); 
								String inputLine = in.readLine();
								//messageRecieved(inputLine);
								InputStream iss = new ByteArrayInputStream(inputLine.getBytes());
								SOAPMessage soap = MessageFactory.newInstance().createMessage(null, iss);
								recievedDestination = soap.getSOAPHeader();
								NodeList msg = soap.getSOAPBody().getElementsByTagNameNS("*", "message");
								message = msg.item(0).getChildNodes().item(0).getNodeValue();
								messageRecieved(message);
								headerRecieved(recievedDestination);
								
								sc.close();
								message = "none";
							}
						} catch(Exception e){
							e.printStackTrace();
						}
					}
				});
				thread.start();
			}
		});
		btnSave.setBounds(153, 61, 89, 23);
		contentPane.add(btnSave);
		
		JLabel lblMessage = new JLabel("Message:");
		lblMessage.setBounds(10, 137, 64, 14);
		contentPane.add(lblMessage);
		
		JLabel lblDestination = new JLabel("Destination:");
		lblDestination.setBounds(10, 162, 81, 14);
		contentPane.add(lblDestination);
		
		textDestination = new JTextField();
		textDestination.setBounds(86, 159, 160, 20);
		contentPane.add(textDestination);
		textDestination.setColumns(10);
		
		textMessage = new JTextField();
		textMessage.setBounds(86, 129, 273, 20);
		contentPane.add(textMessage);
		textMessage.setColumns(10);
		
		JButton btnSend = new JButton("Send");
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(recievedDestination.getValue().equals("none")) {
					recievedDestination.setValue(textDestination.getText());
					sendMessage(recievedDestination);
				}
				else
					sendMessage(recievedDestination);
			}
		});
		btnSend.setBounds(276, 158, 89, 23);
		contentPane.add(btnSend);
	}

	public void createNeighbours() {
		if(name.equals("z1")) {
			neighbours.put("x1", 2100);
			neighbours.put("z2", 3000);
		}
		if(name.equals("z2")) {
			neighbours.put("z1", 2000);
			neighbours.put("x2", 3100);
			neighbours.put("z3", 4000);
		}
		if(name.equals("z3")) {
			neighbours.put("z2", 3000);
			neighbours.put("x3", 4100);
		}
		if(name.equals("x1")) {
			neighbours.put("y1", 2200);
		}
		if(name.equals("x2")) {
			neighbours.put("y2", 3200);
		}
		if(name.equals("x3")) {
			neighbours.put("y3", 4200);
		}
		if(name.equals("y1")) {
			neighbours.put("z1", 2000);
		}
		if(name.equals("y2")) {
			neighbours.put("z2", 3000);
		}
		if(name.equals("y3")) {
			neighbours.put("z2", 4000);
		}
	}
	public void send(String host, int port){
		
		MessageFactory factory;
		String soapMessage = null;
		try {
			factory = MessageFactory.newInstance();
			SOAPMessage soapMsg = factory.createMessage();
			SOAPPart part = soapMsg.getSOAPPart();

			SOAPEnvelope envelope = part.getEnvelope();
			SOAPHeader header = envelope.getHeader();
			SOAPBody body = envelope.getBody();

			if(recievedDestination.getValue().equals("none"))
				header.setValue(textDestination.getText());
			else if(recievedDestination.getValue().startsWith("w")) {
				if(Character.toString(recievedDestination.getValue().charAt(1)).equals(this.layer)) {
					if(header.getAttribute("Visited").equals(""))
						header.setAttribute("Visited", recievedDestination.getAttribute("Visited")+" "+this.name);
					else
						header.setAttribute("Visited", this.name);
				}
				header.setValue(recievedDestination.getValue());
			}
			else
				header.setValue(recievedDestination.getValue());
			if(message.equals("none")) {
				Name messageElement = envelope.createName("message", "", "app");
				SOAPBodyElement element = body.addBodyElement(messageElement);
				element.setValue(textMessage.getText());
			}
			else {
				Name messageElement = envelope.createName("message", "", "app");
				SOAPBodyElement element = body.addBodyElement(messageElement);
				element.setValue(message);
			}
			
			Socket s = new Socket(host, port);
			PrintStream out = new PrintStream(s.getOutputStream(), true);
			soapMsg.writeTo(out);
			recievedDestination.setValue("none");
			message = "none";
			recievedDestination.removeAttribute("Visited");
			s.close();
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}
	
	@Override
	public void headerRecieved(SOAPHeader header) {
		if(header.getValue().startsWith("w")) {
			String[] parts = header.getAttribute("Visited").split(" ");
			if(parts.length<3)
				sendMessage(header);
			else
				recievedDestination.setValue("none");
		}
		else if(!header.getValue().equals(name)) {
			sendMessage(header);
		}
		else
			recievedDestination.setValue("none");
	}
	
	@Override
	public void messageRecieved(String message) {
		if(recievedDestination.getValue().startsWith("a"))
			labelOutput.setText(message);
		else if(recievedDestination.getValue().startsWith("w")) {
			if(Character.toString(recievedDestination.getValue().charAt(1)).equals(this.layer)) {
				labelOutput.setText(message);
			}
		}
		else {
			if(recievedDestination.getValue().equals(this.name))
				labelOutput.setText(message);
		}
	}

	@Override
	public void sendMessage(SOAPHeader destination) {
		if(destination.getValue().startsWith("a")) {
			sendToAll();
		}
		else if(destination.getValue().startsWith("w")) {
			sendToOneNode(destination);
		}
		else {
			sendToOneNode(destination);
		}
	}
	
	private void sendToAll() {
		recievedDestination.setValue("w1");
		sendMessage(recievedDestination);
		recievedDestination.setValue("w2");
		sendMessage(recievedDestination);
		recievedDestination.setValue("w3");
		sendMessage(recievedDestination);
	}
	
	private void sendToOneNode(SOAPHeader destination) {
		String layer = Character.toString(destination.getValue().charAt(1));
		if(this.layer.equals(layer)) {
			int port = 0;
			for(Map.Entry<String, Integer> entry : neighbours.entrySet()) {
			    String key = entry.getKey();
				if(Character.toString(key.charAt(1)).equals(layer) ) {
			    	port = entry.getValue();
			    	break;
		    	}
			}
			this.send("localhost", port);
		}
		else if(!this.layer.equals(layer) && neighbours.size()!=1) {
			int port = 0;
			for(Map.Entry<String, Integer> entry : neighbours.entrySet()) {
			    String key = entry.getKey();
			    if(Character.toString(key.charAt(1)).equals(layer)){
		    		port = entry.getValue();
		    		break;
			    }
			    else if(!Character.toString(key.charAt(1)).equals(this.layer)) {
			    	port = entry.getValue();
		    	}
			}
			this.send("localhost", port);
		}
		else if(!this.layer.equals(layer) && neighbours.size()==1) {
			int port = 0;
			for(Map.Entry<String, Integer> entry : neighbours.entrySet()) {
			    String key = entry.getKey();
		    	port = entry.getValue();
		    	break;
		    	
			}
			this.send("localhost", port);
		}
		else {
			int port = 0;
			for(Map.Entry<String, Integer> entry : neighbours.entrySet()) {
			    String key = entry.getKey();
				if(!Character.toString(key.charAt(1)).equals(layer) ) {
			    	port = entry.getValue();
			    	break;
		    	}
			}
			this.send("localhost", port);
		}
	}
}
