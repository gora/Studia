/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;
import java.io.*;
import java.security.*;
import java.security.spec.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.*;
import javax.crypto.spec.*;
/**
 *
 * @author Klaudia
 */
public class FileEncryption {
	
	public static final int AES_Key_Size = 256;
	Cipher pkCipher, aesCipher;
	byte[] aesKey;
	SecretKeySpec aeskeySpec;
	
        public void Hello(){
            System.out.print("Witaj w clasie FileEncryption");
            try {
                makeKey();
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(FileEncryption.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
	public FileEncryption() throws GeneralSecurityException {
            pkCipher = Cipher.getInstance("RSA");
	    aesCipher = Cipher.getInstance("AES");     
	}
	
	public void makeKey() throws NoSuchAlgorithmException {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
	    kgen.init(AES_Key_Size);
	    SecretKey key = kgen.generateKey();
	    aesKey = key.getEncoded();
	    aeskeySpec = new SecretKeySpec(aesKey, "AES");
	}
        
	public void encrypt() throws IOException, InvalidKeyException {
		aesCipher.init(Cipher.ENCRYPT_MODE, aeskeySpec);
                File in = new File("C:\\Users\\Klaudia\\Desktop\\NetBeansProjects\\Lab09app\\File\\1.txt");
                File out = new File("C:\\Users\\Klaudia\\Desktop\\NetBeansProjects\\Lab09app\\File\\1.txt_e");
		FileInputStream is = new FileInputStream(in);
		CipherOutputStream os = new CipherOutputStream(new FileOutputStream(out), aesCipher);
		copy(is, os);
		os.close();
	}
	
	/**
	 * Decrypts and then copies the contents of a given file.
	 */
	public void decrypt() throws IOException, InvalidKeyException {
		aesCipher.init(Cipher.DECRYPT_MODE, aeskeySpec);
		File in = new File("C:\\Users\\Klaudia\\Desktop\\NetBeansProjects\\Lab09app\\File\\1.txt_e");
                File out = new File("C:\\Users\\Klaudia\\Desktop\\NetBeansProjects\\Lab09app\\File\\1.txt_d");
		CipherInputStream is = new CipherInputStream(new FileInputStream(in), aesCipher);
		FileOutputStream os = new FileOutputStream(out);
		copy(is, os);
		is.close();
		os.close();
	}
	
	/**
	 * Copies a stream.
	 */
	private void copy(InputStream is, OutputStream os) throws IOException {
		int i;
		byte[] b = new byte[1024];
		while((i=is.read(b))!=-1) {
			os.write(b, 0, i);
		}
	}
}