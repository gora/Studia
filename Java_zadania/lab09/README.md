Lab 9. Wykorzystanie szyfrowania oraz polityki bezpieczeństwa przy ładowaniu klas.


Wymagania: do realizacji zadania potrzebna jest wiedza o kluczach, cyfrowym podpisywaniu paczek jar, weryfikacji podpisów itp. 
(warto przejrzeć następujące strony https://docs.oracle.com/javase/tutorial/security/index.html, https://docstore.mik.ua/orelly/java-ent/security/ch12_02.htm).


Należy napisać program, który pozwoli użytkownikowi zaszyfrować i rozszyfrować zadany plik. 
Program powinien korzystać z klasy, która została dostarczona w podpisanej cyfrowo paczce jar. 
Podczas realizacji zadania należy wygenerować klucz prywatny i publiczny. 
Należy podpisać cyfrowo jar zawierający skompilowane klasy. Należy wykorzystać pliki polityki i zezwolenia (permissions).