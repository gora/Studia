Lab 10. Wdrożenie aplikacji Java z wykorzystaniem JavaWS.


Wymagania: do realizacji zadania potrzebna będzie wiedza o JavaWS (Java Web Start) oraz Java Network Launching Protocol (JNLP).


Należy napisać program, który można będzie uruchomić poprzez JavaWS. Podczas realizacji zadania należy wygenerować plik JNLP 
oraz przetestować jego załadowanie na lokalnej maszynie. Podczas realizacji ćwiczenia można skorzystać z narzędzi typu serwer 
Apache Tomcat (na nim opublikowane powinny być plik JNLP oraz jar z aplikacją).


Aplikacja powinna być okienkowa. Tematyka samej aplikacji jest dowolna (można np. wykorzystać program z laboratorium 2), 
należy jednak zwrócić uwagę na ograniczenia wynikające z mechanizmów JavaWS/JNPL.