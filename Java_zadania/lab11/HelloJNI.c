#include <jni.h>
#include <stdio.h>
#include "HelloJNI.h"
#include <string.h>

 
// Implementation of native method sayHello() of HelloJNI class
JNIEXPORT void JNICALL Java_HelloJNI_sayHello(JNIEnv *env, jobject thisObj) {
   printf("Hello World!\n");
   return;
}

JNIEXPORT jboolean JNICALL Java_HelloJNI_isPrime(JNIEnv *env, jobject thisObj, jint num) {
   jboolean isPr = 0;
   for(int i=2; i<num; i++)
   {
	if(num%i==0) {
	   if(i==num)	
	     isPr = 1;
	   else 
	     isPr = 0;
	break;	
	}

   }
	
   return isPr;
}

JNIEXPORT jfloatArray JNICALL Java_HelloJNI_forEachElement(JNIEnv *env, jobject thisObj, jfloatArray oldArray, jfloat val, jstring op) {
	
	const char *newString = (*env)->GetStringUTFChars(env, op, NULL);
	jsize size = (*env)->GetArrayLength(env, oldArray);

	jfloat *arrOut = NULL;
	arrOut = (*env)->GetFloatArrayElements(env,oldArray, 0);

	if(strcmp(newString, "add")==0)
	{
		for(int i=0; i<size; i++)
		{
			arrOut[i] = arrOut[i]+ val; 
		}
	}
	else if(strcmp(newString, "sub")==0)
	{
		for(int i=0; i<size; i++)
		{
			arrOut[i] = arrOut[i]- val; 
		}
	}
	else if(strcmp(newString, "mul")==0)
	{
		for(int i=0; i<size; i++)
		{
			arrOut[i] = arrOut[i]*val; 
		}
	}
	else if(strcmp(newString, "div")==0)
	{
		for(int i=0; i<size; i++)
		{
			arrOut[i] = arrOut[i]/val; 
		}
	}
	else
		printf("Nie ma takiej opcji\n");

	(*env) -> SetFloatArrayRegion(env, oldArray, 0, size, arrOut);
   	return oldArray;
}
