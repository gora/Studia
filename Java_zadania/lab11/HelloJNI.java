import java.util.Scanner;
public class HelloJNI {
   static {
      System.loadLibrary("hello"); // Load native library at runtime
                                   // hello.dll (Windows) or libhello.so (Unixes)
   }
 
   // Declare a native method sayHello() that receives nothing and returns void
   private native void sayHello();
   private native boolean isPrime(int num);
   private native float[] forEachElement(float [] array, float val, String op);

   // Test Driver
   public static void main(String[] args) {
	System.out.println("-------------------zad 1------------------");
	new HelloJNI().sayHello();  // invoke the native method
	int num;
	System.out.println("-------------------zad 2------------------");

	System.out.println("Podaj liczbe:");
	Scanner n = new Scanner(System.in);
	num = Integer.parseInt(n.nextLine());
	boolean ans2 = new HelloJNI().isPrime(num);  // invoke the native method
	if(ans2 == false)
		System.out.println(num + "- nie jest liczba pierwsza");
	else 
		System.out.println(num + "- jest liczba pierwsza");

	System.out.println("-------------------zad 3------------------");
	float val;
	String op;
	System.out.println("Podaj opcje add, sub, mul, div:");
	Scanner n2 = new Scanner(System.in);
	op = n2.nextLine();

	System.out.println("Podaj liczbe:");
	Scanner n1 = new Scanner(System.in);
	val = Float.parseFloat(n1.nextLine());
	float [] array = {1f, 2f, 3f, 4f};
	

	float [] ansArr;
	ansArr = new HelloJNI().forEachElement(array, val, op);
	if(ansArr == null)
		System.out.print("Cos poszlo nie tak");
	else
		for(int i=0; i< ansArr.length; i++)
		{
			System.out.print(ansArr[i] + "|");
		}
	System.out.println("\n");
   }
}
