Lab 11. Rozbudowa aplikacji Java o funkcje zaimplementowane w kodzie natywnym.


Wymagania: wiedza o sposobie implementacji i wykorzystaniu metod natywnych (JNI) oraz znajomość tworzenia bibliotek ładowanych dynamicznie w języku C/C++.


Należy stworzyć aplikację Java'y, której jedna z klas zawiera metody w kodzie natywnym. Metody te to:


void helloWorld() – bezargumentowa metoda wypisująca napis "Hello world!".

bool isPrime( int num ) – jednoargumentowa funkcja zwracająca informację czy podana liczba całkowita num jest pierwsza.

float [] forEachElement( float [] array , float val , String op ) – trzyargumentowa funkcja, 
która do każdego elementu tablicy array dodaje/odejmuje/itp. wartość val w zależności od wartości 
parametru op (4 możliwe wartości: "add", "subtract", "multiply", "divide"). Funkcja może zmieniać oryginalną 
tablicę lub zwracać nową (w tym pierwszym przypadku funkcja ma typ zwracany void).

Należy zaprezentować działanie aplikacji (wywoływanie metod z powyższej klasy). Kod natywny może być realizowany w C lub C++. Przykładowy tutorial JNI.