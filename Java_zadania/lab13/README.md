Lab 13. Implementacja i uruchomienie aplikacji bazującej na JavaFX.


Wymagania: wiedza o tworzeniu aplikacji z wykorzystaniem JavaFX.


Należy stworzyć aplikację z wykorzystaniem JavaFX. Tematyka aplikacji jest dowolna (można wykorzystać zadania z poprzednich laboratoriów). 
Na wyższą ocenę należy jednak zaprezentować bardziej zaawansowane elementy GUI niż same okna, przyciski i etykiety. 
Przykładami bardziej zaawansowanych elementów są combo boxy, checkboxy, list boxy, menu, scrollbary, zakładki (tabs), menu kontekstowe.