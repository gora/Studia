/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafx;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import static java.util.Date.parse;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import static javafx.geometry.Pos.values;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import javax.swing.JFrame;


/**
 *
 * @author Klaudia
 */
public class JavaFX extends Application {        
    public static void main(String[] args) {
        launch(args);

    }
    
    String tekst = "";
    String font = "";
    int fontSize = 12;
    Boolean overWeigh = false;
    String color = "BLACK";
    
        
    final Button button = new Button ("Dodaj");
    final Button buttonGen = new Button ("Wygeneruj png i zamknij");
    final Button buttonCh = new Button ("Zmień");
    final Label notification = new Label ();
    final TextField nick = new TextField("");
    final TextArea text = new TextArea ("");
    final Label labelText = new Label("");
    final Slider slider = new Slider();
    final ColorPicker colorPicker = new ColorPicker();
    final TabPane tabPane = new TabPane();
    final Label label1= new Label("Zmień czciąke:");
    final Label label2= new Label("Pogrub czciąke:");
    final Label label3= new Label("Powieksz czciąke:");
    
    String address = " ";
    
    public Image generateImage(Label label) 
    {
        label.setMinSize(385, 150);
        label.setMaxSize(385, 150);
        label.setPrefSize(385, 150);
        labelText.setStyle("");
        label.setWrapText(true);
        Scene scene = new Scene(new Group(label));
        WritableImage img = new WritableImage(450, 250) ;
        scene.snapshot(img);
        return img ;
    }
    
    public void saveToFile(Image image) {
        String path = "C:\\Users\\Klaudia\\Desktop\\NetBeansProjects\\JavaFX\\zdj\\"+nick.getText().toString()+".png";
        File outputFile = new File(path);
        BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
        try {
          ImageIO.write(bImage, "png", outputFile);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
    }
    
    @Override public void start(Stage stage) {
        stage.setTitle("Dekorator tekstu");
        Scene scene = new Scene(new Group(), 400, 420);
        labelText.setStyle("-fx-border-color: black;");
        labelText.setMinSize(385, 150);
        labelText.setAlignment(Pos.CENTER);
        colorPicker.setValue(Color.BLACK);
        final ComboBox comboBox = new ComboBox();
        CheckBox checkWei = new CheckBox("Pogrubiona");
        slider.setMin(25);
        slider.setMax(100);
        

        
        comboBox.getItems().addAll(
            "Arial","Garamond","Jokerman","Segoe Script","Edwardian Script ITC","Old English Text MT","Verdena"
        );
        comboBox.setValue("Arial");
        
        
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tekst = nick.getText().toString();
                labelText.setText(tekst);
            }
        });
        
        final ProgressBar pb = new ProgressBar(0);
        final ProgressIndicator pi = new ProgressIndicator(0);
 
        slider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                double pom = (double) new_val;
                fontSize = (int) pom;
                pb.setProgress(new_val.doubleValue()/50);
                pi.setProgress(new_val.doubleValue()/50);
            }
        });
        
        buttonGen.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
              Image img = generateImage(labelText); 
              saveToFile(img);
              stage.close();
            }
        });
        
        buttonCh.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                font = comboBox.getValue().toString();
                labelText.setFont(new Font(comboBox.getValue().toString(), fontSize));
                if(checkWei.isSelected())
                    labelText.setFont(Font.font(comboBox.getValue().toString(), FontWeight.BOLD, fontSize));
                labelText.setTextFill(colorPicker.getValue());
                
            }
        });
        
        GridPane grid = new GridPane();
        grid.setVgap(4);
        grid.setHgap(10);
        grid.setPadding(new Insets(5, 5, 5, 5));
        grid.add(new Label("Podaj tekst: "), 0, 0);
        
        grid.add(button, 3, 0);
        grid.add(nick, 1, 0, 2, 1);
        
        //grid.add(new Label("Subject: "), 7, 0);
//        grid.add(subject, 1, 1, 3, 1);     

        grid.add(labelText, 0, 3, 4, 1);
        
        grid.add(new Label("Zmień czciąke: "), 0, 5);
        grid.add(comboBox, 1, 5);
        
        grid.add(new Label("Pogrub czciąke: "), 0, 6);
        grid.add(checkWei, 1, 6);
        grid.add(buttonCh, 3, 6);
        
        grid.add(new Label("Powieksz czciąke: "), 0, 7);
        grid.add(slider, 1, 7);
        
        grid.add(new Label("Zmień kolor: "), 0, 8);
        grid.add(colorPicker, 1, 8);
        grid.add(new Label(" "), 0, 9);
        grid.add(new Label(" "), 0, 10);
        grid.add(buttonGen, 1, 11);
//        grid.add (notification, 1, 3, 3, 1);
        
//        for (int i = 0; i < 2; i++) {
//            Tab tab = new Tab();
//            tab.setText("Tab" + i);
//            HBox hbox = new HBox();
//            hbox.getChildren().add(new Label("Tab" + i));
//            VBox box = new VBox();
//            if(i==0)
//            {
//                hbox.getChildren().addAll(nick,button);
//                hbox.getChildren().add(labelText);
//                hbox.getChildren().add(comboBox);
//                hbox.getChildren().add(checkWei);
//                hbox.getChildren().add(slider);
//                hbox.getChildren().add(colorPicker);
//                hbox.getChildren().add(buttonCh);
//                
//                box.getChildren().add(nick);//,button,labelText, checkWei ,slider,colorPicker, buttonCh, buttonGen);
//            }
//            // hbox.getChildren().addAll(nick,button,labelText, checkWei ,slider,colorPicker, buttonCh, buttonGen);
//            hbox.setAlignment(Pos.CENTER);
//            tab.setContent(hbox);
//            tabPane.getTabs().add(tab);
//        }
//        BorderPane borderPane = new BorderPane();
//        borderPane.prefHeightProperty().bind(scene.heightProperty());
//        borderPane.prefWidthProperty().bind(scene.widthProperty());
//        
//        borderPane.setCenter(tabPane);

//        borderPane.prefHeightProperty().bind(scene.heightProperty());
//        borderPane.prefWidthProperty().bind(scene.widthProperty());
        

        
        Group root = (Group)scene.getRoot();
        root.getChildren().add(grid);
        stage.setScene(scene);
        stage.show();
    }    
}

