Lab 14. Stworzenie narzędzia do monitorowania i zmiany przebiegu działania aplikacji.


Wymagania: wiedza o zarządzaniu aplikacjami Java'y za pomocą JMX (MBean, MBeanServer, MXBean).


Należy napisać program wielowątkowy cyklicznie analizujący treść plików tekstowych. Założenia są następujące:


Na dysku istnieje pewna liczba (najlepiej co najmniej 20 lub 30) plików tekstowych.

Początkowa liczba wątków analizujących to 3.

Dostępna jest pamięć podręczna zawartości plików (np. mapa Stringów).

Początkowy rozmiar pamięci podręcznej (maksymalna liczba pamiętanych plików) to 5.

Wątki analizujące działają cyklicznie:

Wątek losuje plik do analizy (zakładamy stałą liczbę plików).

Wątek sprawdza czy dane pliku są w pamięci podręcznej, jeśli nie to należy plik wczytać do pamięci podręcznej 
(usuwając dane jakiegoś istniejącego pliku w przypadku przekroczenia dopuszczalnego rozmiaru pamięci podręcznej).

Wylosowanie metody analizy pliku.

Analiza pliku i wypisaniu wyniku na ekran (numer wątku, rodzaj analizy, wynik).

Możliwymi metodami analizy są np. policzenie czy tekst zawiera więcej spółgłosek niż samogłosek, 
wypisanie histogramu/częstości występowania samogłosek, wypisanie najczęściej występującego słowa itp. 
Rozmiar plików należy dobrać tak, aby czas wczytywania w przypadku braku pliku w pamięci podręcznej 
zauważalnie wpływał na wydajność aplikacji. Można też sztucznie wydłużyć czas wczytania pliku poprzez uśpienie wątku na chwilę.


Dostęp do pamięci podręcznej musi odbywać się poprzez sekcję krytyczną.


Korzystając z JMX należy stworzyć w aplikacji ziarenko MBean lub MXBean posiadające:


Właściwość pozwalającą ustawić liczbę analizujących wątków (od 0 wzwyż).

Właściwość pozwalającą ustawić rozmiar pamięci podręcznej (od 1 wzwyż).

Metodę zwracającą wiadomość o stanie aplikacji tj. (1) liczbie wątków, (2) stanie pamięci podręcznej 
(liczba zajętych wpisów, liczba wolnych wpisów, zajmowana pamięć), 
(3) procencie błędów pamięci podręcznej (ile razy wczytano plik z dysku dzielone przez liczbę analiz pliku).

Należy wykorzystać jmc lub jconsole by podłączyć się do aplikacji i skorzystać z utworzonego ziarenka w celu monitorowania i zmiany przebiegu działania aplikacji.