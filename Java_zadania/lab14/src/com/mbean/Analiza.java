/*
 * Analiza.java
 *
 * Created on 10 czerwca 2018, 18:50
 */
package com.mbean;

import com.mbean.AnalizaMBean;
import java.io.File;
import javax.management.*;

/**
 * Class Analiza
 *
 * @author Klaudia
 */
public class Analiza implements AnalizaMBean {
    private int threadCount;
    private int cacheCount;
    private float an;
    public Analiza(int threadCount, int cacheCount) 
    {
        this.cacheCount = cacheCount;
        this.threadCount = threadCount;
    }

    @Override
    public int getNrThread() {
        return threadCount;
    }

    @Override
    public int getNrCache() {
        return cacheCount;
    }

    @Override
    public float getRaport() {
        return an;
    }

    @Override
    public void setNrThread(int t) {
        this.threadCount = t;
    }

    @Override
    public void setNrCache(int c) {
        this.cacheCount = c;
    }
    
    //public int get

    @Override
    public void setAns(float w) {
        this.an = w; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float getAns() {
       return an; //To change body of generated methods, choose Tools | Templates.
    }
    
}
