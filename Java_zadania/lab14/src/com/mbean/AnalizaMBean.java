/*
 * AnalizaMBean.java
 *
 * Created on 10 czerwca 2018, 18:50
 */
package com.mbean;

import java.io.File;

/**
 * Interface AnalizaMBean
 *
 * @author Klaudia
 */
public interface AnalizaMBean {
    
    public int getNrThread();
    public void setNrThread(int t);
    public int getNrCache();
    public void setNrCache(int c);
    public void setAns(float w);
    public float getAns();
    public float getRaport();
    
}
