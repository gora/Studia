/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbean;

import com.thread.AnalizeThread;
import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

/**
 *
 * @author Klaudia
 */
public class AnalizaManagement {
    public static Map<String, String> cache = new HashMap<>();
    public static String tabPom[] ;
    static Path currPath = Paths.get("");
    static String path = currPath.toAbsolutePath().toString() + "\\File";
    static File file = new File(path);
    static File[] fileTab = file.listFiles();
    private static final int DEFAULT_NR_THREADS=10;
    private static final int DEFAULT_NR_CACHE=10;
    public static float load=1;
    public static float analFile=1;
     public static void main(String[] args) throws MalformedObjectNameException, InterruptedException, InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException, IOException {
        //Get the MBean server
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        
        //register the MBean
        Analiza mBean = new Analiza(DEFAULT_NR_THREADS, DEFAULT_NR_CACHE);
        ObjectName name = new ObjectName("com.mbean:type=Analiza");
        for(File s : fileTab)
             System.out.println(s.getName());
        ProcessBuilder p = new ProcessBuilder("jConsole");
        p.start();
        float wynik =0;
        mbs.registerMBean(mBean, name);
        do{
            for(int i=0; i<mBean.getNrThread(); i++)
            {
                AnalizeThread anaThread = new AnalizeThread(i, fileTab, mBean.getNrCache());
                Thread thread = new Thread(anaThread);
                thread.start();
            }
            Thread.sleep(2000);
            wynik = load/analFile;
            mBean.setAns(wynik);
        }while(mBean.getNrThread() !=0);
        
    }

}
