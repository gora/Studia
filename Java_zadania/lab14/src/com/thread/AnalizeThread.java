/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thread;

import com.mbean.AnalizaManagement;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Klaudia
 */
public class AnalizeThread extends Thread {

    //public Map<String, String> cache = new HashMap<>();
    ;// = new String[5];
    public int counter;
    public File[] fileTab;
    public File chooseFile;
    public int maxCacheSize;
    public int ans;
    public int id;
    public int ran;
    AnalizaManagement anal;
    
    public AnalizeThread(int i, File fileTab[], int maxCacheSize){
        this.fileTab = fileTab;
        this.maxCacheSize = maxCacheSize;
        anal.tabPom= new String[maxCacheSize];
        this.id = i;
        counter =0;
        ans =0;
        ran =0;
    }
    
    public synchronized void randFile()
    {
        Random gen = new Random();
        chooseFile = fileTab[gen.nextInt(20)];
    }
    
    public synchronized void checkFile()
    {
        Boolean check =false;
        //System.out.println(chooseFile.getName());
        for(int i=0; i<anal.cache.size();i++)
        {
            //System.out.println(anal.tabPom[i]);
            if(anal.cache.containsKey(chooseFile.getName()))
            {
                check = true;
                //com.mbean.AnalizaManagement.noload ++;
                break;
                
                
            }
        }
        if(!check)

            if(counter == maxCacheSize-1)
            {
                //String pom = anal.tabPom[counter];
                synchronized(this){anal.cache.remove(anal.cache.keySet().stream().findFirst().get());}
               
                anal.load ++;
                //anal.tabPom[counter]=chooseFile.getName();
                String fileContents = "";
                Scanner scanner = null;
                try {
                     scanner = new Scanner(chooseFile);
                    while(scanner.hasNextLine()) {
                        fileContents = fileContents + scanner.nextLine() + " ";
                        //fileContents.append(scanner.nextLine() + lineSeparator);
                    }
                } catch (FileNotFoundException ex) {
                Logger.getLogger(AnalizeThread.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                    scanner.close();
                }
                anal.cache.put(chooseFile.getName(), fileContents);
            }
            else 
            {
                anal.load ++;
                //anal.tabPom[counter]=chooseFile.getName();
                counter++;
                String fileContents = "";
                Scanner scanner = null;
            try {
                scanner = new Scanner(chooseFile);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(AnalizeThread.class.getName()).log(Level.SEVERE, null, ex);
            }
                try {
                    while(scanner.hasNextLine()) {
                        fileContents = fileContents + scanner.nextLine() + " ";
                        //fileContents.append(scanner.nextLine() + lineSeparator);
                    }
                } finally {
                    scanner.close();
                }
                anal.cache.put(chooseFile.getName(), fileContents);
            }
        
    }
    
    public synchronized void methodAnalize()
    {
        Random gen = new Random();
        ran = gen.nextInt(3);
        String pom;
        
        switch (ran) {
            case 0:
              {
                //System.out.println("ile slow w pliku");
                pom = anal.cache.get(chooseFile.getName());
                //System.out.println(pom+"");
                anal.analFile ++;
                boolean word = false;
                int endOfLine = pom.length() - 1;
                //System.out.println("\n\n\n\n"+pom.length()+"\n\n\n");
                for (int i = 0; i < pom.length(); i++) {
                    // if the char is a letter, word = true.
                    if (Character.isLetter(pom.charAt(i)) && i != endOfLine) {
                        word = true;
                        // if char isn't a letter and there have been letters before,
                        // counter goes up.
                    } else if (!Character.isLetter(pom.charAt(i)) && word) {
                        ans++;
                        word = false;
                    } else if (Character.isLetter(pom.charAt(i)) && i == endOfLine) {
                        ans++;
                    }
                }
              }
            case 1:
              {
                //System.out.println("ile jest \"a\"");
                pom = anal.cache.get(chooseFile.getName());
                anal.analFile ++;
                Pattern pattern = Pattern.compile("([aA])"); //case insensitive, use [g] for only lower
                Matcher matcher = pattern.matcher(pom);
                while (matcher.find()) ans++;
              }
            case 2:
              {
                pom = anal.cache.get(chooseFile.getName());
                String string2[]=pom.split(":");
                anal.analFile ++;
                for (int i=0; i<string2.length; i++)
                {
                    if(string2[i]=="to")
                        ans ++;
                }
              }
            default:
              {
              }
          }
                
    }
    
    public void run() {
            randFile();
            checkFile();
//            try {
//                if(!)
//                {
                    methodAnalize();
                    if(ran==0)
                        System.out.println("Thread " + id + " - ilosc wyrazow w pliku = " + ans);
                    else if(ran==1)
                        System.out.println("Thread " + id + " - ilosc 'a' w pliku = " + ans);
                    else if(ran==2)
                        System.out.println("Thread " + id + " - ilosc 'to' w pliku = " + ans);
//                }
//            } catch (FileNotFoundException ex) {
//                Logger.getLogger(AnalizeThread.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException ex) {
//                Logger.getLogger(AnalizeThread.class.getName()).log(Level.SEVERE, null, ex);
//            }
            ans =0;
        
             
             
    }
}
