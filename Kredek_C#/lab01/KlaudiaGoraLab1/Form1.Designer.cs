﻿namespace KlaudiaGoraLab1
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.TextBoxHaslo = new System.Windows.Forms.TextBox();
            this.LebelZaloguj = new System.Windows.Forms.Button();
            this.labelLogin = new System.Windows.Forms.Label();
            this.labelHaslo = new System.Windows.Forms.Label();
            this.TextBoxLogin = new System.Windows.Forms.TextBox();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonPetla = new System.Windows.Forms.Button();
            this.buttonPolacz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.BackColor = System.Drawing.SystemColors.Control;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelName.Location = new System.Drawing.Point(16, 11);
            this.labelName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelName.MaximumSize = new System.Drawing.Size(13, 12);
            this.labelName.MinimumSize = new System.Drawing.Size(267, 27);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(267, 27);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Klaudia Gora";
            // 
            // TextBoxHaslo
            // 
            this.TextBoxHaslo.Location = new System.Drawing.Point(103, 170);
            this.TextBoxHaslo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxHaslo.Name = "TextBoxHaslo";
            this.TextBoxHaslo.Size = new System.Drawing.Size(132, 22);
            this.TextBoxHaslo.TabIndex = 1;
            // 
            // LebelZaloguj
            // 
            this.LebelZaloguj.Location = new System.Drawing.Point(259, 140);
            this.LebelZaloguj.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LebelZaloguj.Name = "LebelZaloguj";
            this.LebelZaloguj.Size = new System.Drawing.Size(100, 28);
            this.LebelZaloguj.TabIndex = 2;
            this.LebelZaloguj.Text = "zaloguj";
            this.LebelZaloguj.UseVisualStyleBackColor = true;
            this.LebelZaloguj.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Location = new System.Drawing.Point(25, 123);
            this.labelLogin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(42, 17);
            this.labelLogin.TabIndex = 3;
            this.labelLogin.Text = "login:";
            // 
            // labelHaslo
            // 
            this.labelHaslo.AutoSize = true;
            this.labelHaslo.Location = new System.Drawing.Point(19, 178);
            this.labelHaslo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHaslo.Name = "labelHaslo";
            this.labelHaslo.Size = new System.Drawing.Size(46, 17);
            this.labelHaslo.TabIndex = 4;
            this.labelHaslo.Text = "hasło:";
            // 
            // TextBoxLogin
            // 
            this.TextBoxLogin.Location = new System.Drawing.Point(103, 123);
            this.TextBoxLogin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TextBoxLogin.Name = "TextBoxLogin";
            this.TextBoxLogin.Size = new System.Drawing.Size(132, 22);
            this.TextBoxLogin.TabIndex = 5;
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(259, 278);
            this.buttonClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(100, 28);
            this.buttonClose.TabIndex = 8;
            this.buttonClose.Text = "Zamknij";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonPetla
            // 
            this.buttonPetla.Location = new System.Drawing.Point(259, 178);
            this.buttonPetla.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPetla.Name = "buttonPetla";
            this.buttonPetla.Size = new System.Drawing.Size(100, 28);
            this.buttonPetla.TabIndex = 6;
            this.buttonPetla.Text = "Pętla";
            this.buttonPetla.UseVisualStyleBackColor = true;
            this.buttonPetla.Click += new System.EventHandler(this.buttonPetla_Click);
            // 
            // buttonPolacz
            // 
            this.buttonPolacz.Location = new System.Drawing.Point(259, 214);
            this.buttonPolacz.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPolacz.Name = "buttonPolacz";
            this.buttonPolacz.Size = new System.Drawing.Size(100, 28);
            this.buttonPolacz.TabIndex = 7;
            this.buttonPolacz.Text = "Połącz";
            this.buttonPolacz.UseVisualStyleBackColor = true;
            this.buttonPolacz.Click += new System.EventHandler(this.buttonPolacz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 321);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonPolacz);
            this.Controls.Add(this.buttonPetla);
            this.Controls.Add(this.TextBoxLogin);
            this.Controls.Add(this.labelHaslo);
            this.Controls.Add(this.labelLogin);
            this.Controls.Add(this.LebelZaloguj);
            this.Controls.Add(this.TextBoxHaslo);
            this.Controls.Add(this.labelName);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox TextBoxHaslo;
        private System.Windows.Forms.Button LebelZaloguj;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Label labelHaslo;
        private System.Windows.Forms.TextBox TextBoxLogin;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonPetla;
        private System.Windows.Forms.Button buttonPolacz;
    }
}

