﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KlaudiaGoraLab1
{
    public partial class Form1 : Form
    {
        //licznik kliknieć
        int licznik = 0;

        //zmienna tekstowa
        string text = "przykladowy ekran";

        public Form1()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (TextBoxHaslo.Text == "test" && TextBoxLogin.Text == "test")
            {
                {
                    Logged logged = new Logged();
                    logged.Show();
                    logged.Text = TextBoxLogin.Text;
                }

            }
            else
                MessageBox.Show("Zły login lub haslo");

            //Close();
        }

        /// <summary>
        /// Przykładowa pętla
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPetla_Click(object sender, EventArgs e)
        {
            for (int i = 1; i <= 10; i++)
            {
                TextBoxLogin.Text += (i+ " ");
            }
        }

        private void buttonPolacz_Click(object sender, EventArgs e)
        {
            if(TextBoxLogin.Text == "test" && TextBoxHaslo.Text == "test")
            {
                Logged logged = new Logged();
                logged.Show();
                logged.Text = TextBoxLogin.Text;
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
