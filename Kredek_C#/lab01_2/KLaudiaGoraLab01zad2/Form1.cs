﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KLaudiaGoraLab01zad2
{
    public partial class Form1 : Form
    {
        private int _ticks;
        public Form1()
        {
            InitializeComponent();
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _ticks++;
            Random losKolor = new Random();
            this.Text = _ticks.ToString();
            this.BackColor = Color.FromArgb(losKolor.Next(255), losKolor.Next(255), losKolor.Next(255));

            button1.BackColor = Color.FromArgb(losKolor.Next(255), losKolor.Next(255), losKolor.Next(255));
            button2.BackColor = Color.FromArgb(losKolor.Next(255), losKolor.Next(255), losKolor.Next(255));
            button3.BackColor = Color.FromArgb(losKolor.Next(255), losKolor.Next(255), losKolor.Next(255));
            button4.BackColor = Color.FromArgb(losKolor.Next(255), losKolor.Next(255), losKolor.Next(255));
            button5.BackColor = Color.FromArgb(losKolor.Next(255), losKolor.Next(255), losKolor.Next(255));
            button6.BackColor = Color.FromArgb(losKolor.Next(255), losKolor.Next(255), losKolor.Next(255));
            if (_ticks == 10)
            {
                button1.BackColor = Color.Black;
                button2.BackColor = Color.Black;
                button3.BackColor = Color.Black;
                button4.BackColor = Color.Black;
                button5.BackColor = Color.Black;
                button6.BackColor = Color.Black;
                this.Text = "Koniec";
                this.BackColor = Color.Black;
                timer1.Stop();
            }

        }

    

        
    }
}
