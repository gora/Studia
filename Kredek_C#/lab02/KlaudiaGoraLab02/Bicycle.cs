﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlaudiaGoraLab02
{
    class Bicycle : Vehicle
    {
        protected string Mark { get; set; }
        protected int Weight { get; set; }
        private int Frame { get; set; }

        public Bicycle(string mark, int weight, int frame)
        {
            Mark = mark;
            Weight = weight;
            Frame = frame;
        }

        public void DisplayDescriptionOfBicycle()
        {
            SetNumberOfWindows(0);
            GetAmountOfWheel(2);
            GetNumberOfPassengers(1);
            GetRequiredAgeToDriveTheVehicle(0);
            Console.WriteLine("Mark: ", Mark);
            Console.WriteLine("weight: ", Weight);
            Console.WriteLine("frame:", Frame);
            DisplayDescription();
           
        }

    }
}
