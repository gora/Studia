﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlaudiaGoraLab02
{
    class Car : Vehicle
    {
        protected string Mark { get; set; }
        protected int Weight { get; set; }

        public Car (string mark, int weight)
        {
            Mark = mark;
            Weight = weight;
        }

      
        public void DisplayDescriptionOfCar()
        {
            SetNumberOfWindows(6);
            GetAmountOfWheel(4);
            GetNumberOfPassengers(5);
            GetRequiredAgeToDriveTheVehicle(18);
            Console.WriteLine("Mark: ", Mark);
            Console.WriteLine("weight: ", Weight);
            DisplayDescription();
            

        }
    }
}
