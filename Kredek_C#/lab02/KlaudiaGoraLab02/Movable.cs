﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlaudiaGoraLab02
{
    interface Movable
    {
        void SetPriceOfTheVehicle(int amountOfWheel);
        void SetTypeOfDrive(string typeOfDrive);
        void SetPriceAndType();

    }
}
