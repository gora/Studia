﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlaudiaGoraLab02
{
    class Program
    {
        static void Main(string[] args)
        {
            Movable vehicle = new Vehicle();
            int price;
            string type;

            Console.WriteLine("\nPrice of the car:");
            price = int.Parse(Console.ReadLine());
            vehicle.SetPriceOfTheVehicle(price);
            Console.WriteLine("Type of drive: ");
            type = Console.ReadLine();
            vehicle.SetTypeOfDrive(type);

            Car car1 = new Car("Audi", 3200); // nie mogę znależć dlaczego nie wchodzą mi te dane do konstruktora
            Car car2 = new Car("Fiat", 2970);
            Car car3 = new Car("Volvo", 4000);

            car1.DisplayDescriptionOfCar();
            Console.WriteLine("\n");
            car2.DisplayDescriptionOfCar();
            Console.WriteLine("\n");
            car3.DisplayDescriptionOfCar();
            Console.WriteLine("\n");

            Bicycle bicycle1 = new Bicycle("Cross", 15, 13); 
            Bicycle bicycle2 = new Bicycle("Cube", 21, 19);
            Bicycle bicycle3 = new Bicycle("UniBike", 10, 13);


            bicycle1.DisplayDescriptionOfBicycle();
            Console.WriteLine("\n");
            bicycle2.DisplayDescriptionOfBicycle();
            Console.WriteLine("\n");
            bicycle3.DisplayDescriptionOfBicycle();
            Console.WriteLine("\n");

            Console.ReadLine();

        }
    }
}
