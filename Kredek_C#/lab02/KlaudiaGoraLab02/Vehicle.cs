﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlaudiaGoraLab02
{
    class Vehicle : Base, Movable
    {
        protected int amountOfWheel;
        protected string typeOfDrive;
        protected int numberOfPassengers;
        protected int priceOfTheVehicle;
        protected int requiredAgeToDriveTheVehicle;


        void Movable.SetPriceOfTheVehicle(int priceOfTheVehicle)
        {
            this.priceOfTheVehicle = priceOfTheVehicle;
        }
        void Movable.SetTypeOfDrive( string typeOfDrive)
        {
            this.typeOfDrive = typeOfDrive;
        }
        void Movable.SetPriceAndType()
        {
            Console.WriteLine("price :" + priceOfTheVehicle + " type: " + typeOfDrive);
        }

        public void GetAmountOfWheel( int amountOfWheel)
        {
            this.amountOfWheel = amountOfWheel;
        }

        public void GetNumberOfPassengers(int numberOfPassengers)
        {
            this.numberOfPassengers =numberOfPassengers;
        }

        public void GetRequiredAgeToDriveTheVehicle(int requiredAgeToDriveTheVehicle)
        {
            this.requiredAgeToDriveTheVehicle = requiredAgeToDriveTheVehicle;
        }

        public void DisplayDescription()
        {
            Console.WriteLine("number of windows: " + numberOfWindows + " ,number of wheels: " + amountOfWheel + ", of passengers :" + numberOfPassengers + ", age of driver " + requiredAgeToDriveTheVehicle + ", colour: " +GetColor() );
        }

    }
}
