﻿namespace KlaudiaGoraLab3
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewZOO = new System.Windows.Forms.DataGridView();
            this.buttonShowAllAnimals = new System.Windows.Forms.Button();
            this.buttonShowAllSloths = new System.Windows.Forms.Button();
            this.buttonAddAnimal = new System.Windows.Forms.Button();
            this.textBoxAmount = new System.Windows.Forms.TextBox();
            this.textBoxSpecies = new System.Windows.Forms.TextBox();
            this.labelSpecies = new System.Windows.Forms.Label();
            this.labelAmount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewZOO)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewZOO
            // 
            this.dataGridViewZOO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewZOO.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewZOO.Name = "dataGridViewZOO";
            this.dataGridViewZOO.RowTemplate.Height = 24;
            this.dataGridViewZOO.Size = new System.Drawing.Size(609, 403);
            this.dataGridViewZOO.TabIndex = 0;
            // 
            // buttonShowAllAnimals
            // 
            this.buttonShowAllAnimals.Location = new System.Drawing.Point(87, 459);
            this.buttonShowAllAnimals.Name = "buttonShowAllAnimals";
            this.buttonShowAllAnimals.Size = new System.Drawing.Size(89, 23);
            this.buttonShowAllAnimals.TabIndex = 1;
            this.buttonShowAllAnimals.Text = "Show All";
            this.buttonShowAllAnimals.UseVisualStyleBackColor = true;
            this.buttonShowAllAnimals.Click += new System.EventHandler(this.buttonShowAllAnimals_Click);
            // 
            // buttonShowAllSloths
            // 
            this.buttonShowAllSloths.Location = new System.Drawing.Point(275, 459);
            this.buttonShowAllSloths.Name = "buttonShowAllSloths";
            this.buttonShowAllSloths.Size = new System.Drawing.Size(117, 23);
            this.buttonShowAllSloths.TabIndex = 2;
            this.buttonShowAllSloths.Text = "Show All Sloths";
            this.buttonShowAllSloths.UseVisualStyleBackColor = true;
            this.buttonShowAllSloths.Click += new System.EventHandler(this.buttonShowAllSloths_Click);
            // 
            // buttonAddAnimal
            // 
            this.buttonAddAnimal.Location = new System.Drawing.Point(749, 272);
            this.buttonAddAnimal.Name = "buttonAddAnimal";
            this.buttonAddAnimal.Size = new System.Drawing.Size(136, 23);
            this.buttonAddAnimal.TabIndex = 3;
            this.buttonAddAnimal.Text = "Add Animal";
            this.buttonAddAnimal.UseVisualStyleBackColor = true;
            this.buttonAddAnimal.Click += new System.EventHandler(this.buttonAddAnimal_Click);
            // 
            // textBoxAmount
            // 
            this.textBoxAmount.Location = new System.Drawing.Point(749, 181);
            this.textBoxAmount.Name = "textBoxAmount";
            this.textBoxAmount.Size = new System.Drawing.Size(100, 22);
            this.textBoxAmount.TabIndex = 4;
            // 
            // textBoxSpecies
            // 
            this.textBoxSpecies.Location = new System.Drawing.Point(749, 130);
            this.textBoxSpecies.Name = "textBoxSpecies";
            this.textBoxSpecies.Size = new System.Drawing.Size(100, 22);
            this.textBoxSpecies.TabIndex = 5;
            // 
            // labelSpecies
            // 
            this.labelSpecies.AutoSize = true;
            this.labelSpecies.Location = new System.Drawing.Point(667, 130);
            this.labelSpecies.Name = "labelSpecies";
            this.labelSpecies.Size = new System.Drawing.Size(62, 17);
            this.labelSpecies.TabIndex = 6;
            this.labelSpecies.Text = "Species:";
            // 
            // labelAmount
            // 
            this.labelAmount.AutoSize = true;
            this.labelAmount.Location = new System.Drawing.Point(667, 181);
            this.labelAmount.Name = "labelAmount";
            this.labelAmount.Size = new System.Drawing.Size(60, 17);
            this.labelAmount.TabIndex = 7;
            this.labelAmount.Text = "Amount:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 531);
            this.Controls.Add(this.labelAmount);
            this.Controls.Add(this.labelSpecies);
            this.Controls.Add(this.textBoxSpecies);
            this.Controls.Add(this.textBoxAmount);
            this.Controls.Add(this.buttonAddAnimal);
            this.Controls.Add(this.buttonShowAllSloths);
            this.Controls.Add(this.buttonShowAllAnimals);
            this.Controls.Add(this.dataGridViewZOO);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewZOO)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewZOO;
        private System.Windows.Forms.Button buttonShowAllAnimals;
        private System.Windows.Forms.Button buttonShowAllSloths;
        private System.Windows.Forms.Button buttonAddAnimal;
        private System.Windows.Forms.TextBox textBoxAmount;
        private System.Windows.Forms.TextBox textBoxSpecies;
        private System.Windows.Forms.Label labelSpecies;
        private System.Windows.Forms.Label labelAmount;
    }
}

