﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KlaudiaGoraLab3
{
    public partial class Form1 : Form
    {
        SqlConnection sqlConnection;
        SqlDataAdapter sqlDataAdapter;
        public Form1()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source =KLALAP; database = ZOO; Trusted_Connection=yes");
        }

        private void buttonShowAllAnimals_Click(object sender, EventArgs e)
        {
            Animals.ShowAllAnimals(sqlConnection, dataGridViewZOO);
        }

        private void buttonShowAllSloths_Click(object sender, EventArgs e)
        {
            Sloths.ShowAllSloths(sqlConnection, dataGridViewZOO);
        }

        private void buttonAddAnimal_Click(object sender, EventArgs e)
        {
            Animals.AddAnimal(sqlConnection, dataGridViewZOO, textBoxSpecies.Text, textBoxAmount.Text);

        }
    }
}
