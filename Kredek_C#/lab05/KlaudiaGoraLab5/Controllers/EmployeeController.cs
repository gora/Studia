﻿using KlaudiaGoraLab5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KlaudiaGoraLab5.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Emplyee
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            List<Employee> employee;

            using (var ctx = new PizzaContext())
            {

                employee = ctx.Employees.ToList();
            }
            return View(employee);
        }

        public ActionResult Add()
        {
            return View(new Employee());
        }

        [HttpPost]
        public ActionResult Add(Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return View(new Employee());
            }
            using (var ctx = new PizzaContext())
            {
                ctx.Employees.Add(employee);
                ctx.SaveChanges();
            }

            return RedirectToAction("List");
        }


        public ActionResult Edit(int id)
        {
            Employee employee;
            using (var ctx = new PizzaContext())
            {
                employee = ctx.Employees.FirstOrDefault(p => p.Id == id);
            }
            return View(employee);
        }

        [HttpPost]
        public ActionResult Edit(Employee model, int id)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Edit");
            }
            Employee employee;
            using (var ctx = new PizzaContext())
            {
                employee = ctx.Employees.FirstOrDefault(p => p.Id == id);
                employee.Name = model.Name;
                employee.Surname = model.Surname;
                ctx.SaveChanges();
            }
            return RedirectToAction("List");
        }
        public ActionResult Remove(int id)
        {
            Employee employee;
            using (var ctx = new PizzaContext())
            {
                employee = ctx.Employees.FirstOrDefault(p => p.Id == id);
            }
            return View(employee);
        }

        [HttpPost]
        public ActionResult Remove(Employee model, int id)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Remove");
            }
            Employee employee;
            using (var ctx = new PizzaContext())
            {
                employee = ctx.Employees.FirstOrDefault(p => p.Id == id);
                //pizza.Name = model.Name;
                //pizza.Ingredients = model.Ingredients;
                ctx.Employees.Remove(employee);
                ctx.SaveChanges();
            }
            return RedirectToAction("List");
        }
    }
}