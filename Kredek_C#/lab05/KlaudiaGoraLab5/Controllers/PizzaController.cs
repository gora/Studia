﻿using KlaudiaGoraLab5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KlaudiaGoraLab5.Controllers
{
    public class PizzaController : Controller
    {
        // GET: Pizza
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            List<Pizza> pizzas;

            using (var ctx = new PizzaContext())
            {

               pizzas = ctx.Danie.ToList();
            }
            return View(pizzas);
        }

        public ActionResult Add()
        {
            return View(new Pizza());
        }

        [HttpPost]
        public ActionResult Add(Pizza pizza)
        {
            if (!ModelState.IsValid)
            {
                return View(new Pizza());
            }
            using (var ctx = new PizzaContext())
            {
                ctx.Danie.Add(pizza);
                ctx.SaveChanges();
            }

            return RedirectToAction("List");
        }


        public ActionResult Edit(int id)
        {
            Pizza pizza;
            using (var ctx = new PizzaContext())
            {
                pizza = ctx.Danie.FirstOrDefault(p => p.Id == id);
            }
            return View(pizza);
        }

        [HttpPost]
        public ActionResult Edit(Pizza model, int id)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Edit");
            }
            Pizza pizza;
            using (var ctx = new PizzaContext())
            {
                pizza = ctx.Danie.FirstOrDefault(p => p.Id == id);
                pizza.Name = model.Name;
                pizza.Ingredients = model.Ingredients;
                ctx.SaveChanges();
            }
            return RedirectToAction("List");
        }
        [HttpPost]
        public ActionResult Remove(Pizza model, int id)
        {
            using (var ctx = new PizzaContext())
            {
                var pizza = ctx.Danie.SingleOrDefault(p => p.Id == id);
                ctx.Danie.Remove(pizza);
                ctx.SaveChanges();
            }

            return RedirectToAction("List");
        }
    }

       
  
}