﻿using KlaudiaGoraLab5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KlaudiaGoraLab5.Controllers
{
    public class PostController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            List<Post> posts;

            using (var ctx = new PizzaContext())
            {

                posts = ctx.Posts.ToList();
            }
            return View(posts);
        }

        public ActionResult Add()
        {
            return View(new Post());
        }

        [HttpPost]
        public ActionResult Add(Post posts)
        {
            if (!ModelState.IsValid)
            {
                return View(new Post());
            }
            using (var ctx = new PizzaContext())
            {
                ctx.Posts.Add(posts);
                Post.createdat = DateTime.Now;
                ctx.SaveChanges();
            }

            return RedirectToAction("List");
        }


        public ActionResult Edit(int id)
        {
            Post posts;
            using (var ctx = new PizzaContext())
            {
                posts = ctx.Posts.FirstOrDefault(p => p.Id == id);
            }
            return View(posts);
        }

        [HttpPost]
        public ActionResult Edit(Post model, int id)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Edit");
            }
            Post posts;
            using (var ctx = new PizzaContext())
            {
                posts = ctx.Posts.FirstOrDefault(p => p.Id == id);
                posts.Title = model.Title;
                posts.Body = model.Body;
                ctx.SaveChanges();
            }
            return RedirectToAction("List");
        }


        [HttpPost]
        public ActionResult Remove(Post model, int id)
        {
            using (var ctx = new PizzaContext())
            {
                var post = ctx.Posts.SingleOrDefault(p => p.Id == id);
                ctx.Posts.Remove(post);
                ctx.SaveChanges();
            }

            return RedirectToAction("List");
        }
    }
}