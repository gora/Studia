﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace KlaudiaGoraLab5.Models 
{
    public class PizzaContext : DbContext
    {
        public PizzaContext() : base("Pizzeria")
        { }

        // public IDbSet<Pizza> Pizzass { get; set; }

        public IDbSet<Pizza> Danie { get; set; }
        public IDbSet<Employee> Employees { get; set; }
        public IDbSet<Post> Posts { get; set; }
    }
}