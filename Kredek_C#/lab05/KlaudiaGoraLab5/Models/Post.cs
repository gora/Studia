﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KlaudiaGoraLab5.Models
{
    public class Post
    {
        public static DateTime createdat { get; internal set; }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}