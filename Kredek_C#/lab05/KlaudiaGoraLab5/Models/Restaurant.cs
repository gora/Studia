﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KlaudiaGoraLab5.Models
{
    public class Restaurant
    {
        [Key]
        public int Id { get; set; }
        public string NazwaRestauracji { get; set; }
        public string Adres { get; set; }
    }
}