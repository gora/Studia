import { Router } from '@angular/router';
import { HttpServiceLogin } from './../modal-login/http.service.login';
import { MatSnackBar } from '@angular/material';
import { SingleGroupInt } from './../group-info/SingleGroupInt';
import { HttpServiceAllGroups } from './../all-groups/http.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.css']
})
export class AddGroupComponent implements OnInit {
  constructor(private httpService: HttpServiceAllGroups, private snackBar: MatSnackBar, private loginService: HttpServiceLogin,
    private router: Router) { }

  form = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
    description: new FormControl('', [Validators.maxLength(250)])
  });

  ngOnInit() {
    if (!this.checkToken()) {
      this.snackBar.open('Nie możesz dodawać grup! Zaloguj się.', 'Zamknij');
      this.router.navigate(['/login']);
    }
  }

  checkToken() {
    const accessToken = this.loginService.AccesToken;

    if (accessToken !== '') {
      return true;
    } else {
      return false;
    }
  }

  get name() {
    return this.form.get('name');
  }

  get description() {
    return this.form.get('description');
  }

  addGroup() {
    const newGroup = new SingleGroupInt();
    newGroup.Name = this.name.value;
    newGroup.Description = this.description.value;

    this.httpService.addGroup(newGroup).subscribe(group => {
      if (newGroup.Description === group.Description) {
        this.snackBar.open('Dodano grupę.', 'Zamknij');
        this.router.navigate(['/groups']);
      } else {
        this.snackBar.open('Błąd podczas dodawania grupy!', 'Zamknij');
      }
    });
  }
}
