import { Component, OnInit, ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

import { SingleMeeting } from '../meeting-info/SingleMeeting';
import { HttpServiceEvents } from './../all-meetings/http.service';
import { LocalizationInt } from './LocalizationInt';
import { MeetingValidators } from './meeting.validators';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { HttpServiceLogin } from '../modal-login/http.service.login';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-add-meeting',
  templateUrl: './add-meeting.component.html',
  styleUrls: ['./add-meeting.component.css']
})
export class AddMeetingComponent implements OnInit {

  @ViewChild('search')
  public searchElementRef: ElementRef;
  constructor(private httpService: HttpServiceEvents, private snackBar: MatSnackBar, private router: Router,
  private loginService: HttpServiceLogin, private mapsAPILoader: MapsAPILoader,
  private ngZone: NgZone, private route: ActivatedRoute) {

    this.route.params.subscribe(params => {
      console.log(params);
      if (params['id']) {
        console.log(params.id);
        this.updatedComponentId = params.id;
        this.getMeeting(params.id);
        this.update = true;
      }
    });
   }
  update = false;
  updatedComponentId: number;
  public latitude: number;
  public longitude: number;
  public locationName: String;
  public searchControl: FormControl;
  public zoom: number;
  newMeeting = new SingleMeeting();
  meeting = new SingleMeeting();

  form = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
    description: new FormControl('', [Validators.maxLength(250)]),
    eventDate: new FormControl('', [Validators.required, MeetingValidators.shouldBeInFuture])
  });

  ngOnInit() {
    if (!this.checkToken()) {
      this.snackBar.open('Nie możesz dodawać grup! Zaloguj się.', 'Zamknij');
      this.router.navigate(['/login']);
    } else {
    }

    this.zoom = 4;
    this.latitude = 39.8282;
    this.longitude = -98.5795;
    // create search FormControl
    this.searchControl = new FormControl();
    // set current position
    this.setCurrentPosition();

    // Load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          // Verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          // Set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  get name() {
    return this.form.get('name');
  }

  get description() {
    return this.form.get('description');
  }

  get eventDate() {
    return this.form.get('eventDate');
  }

  checkToken() {
    const accessToken = this.loginService.AccesToken;

    if (accessToken !== '') {
      return true;
    } else {
      return false;
    }
  }

  addSingleMeeting() {
    this.newMeeting.Name = this.name.value;
    this.newMeeting.Description = this.description.value;
    this.newMeeting.EventDate = this.eventDate.value;
    this.newMeeting.Localization = new LocalizationInt();
    this.newMeeting.Localization.Latitude = this.latitude.toString();
    this.newMeeting.Localization.Longitude = this.longitude.toString();

    console.log(this.longitude + ' x ' + this.latitude);

    this.httpService.addMeeting(this.newMeeting).subscribe(meeting => {
      if (meeting.Description === this.newMeeting.Description) {
        this.snackBar.open('Dodano spotkanie.', 'Zamknij');
        this.router.navigate(['/meetings']);
      } else {
        this.snackBar.open('Błąd podczas dodawania spotkania! Sprobuj ponownie!', 'Zamknij');
      }
      console.log(meeting);
    });
  }

  onChooseLocation(event) {
    console.log(event);
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
    console.log('Latitude: ' + this.latitude);
    console.log('Longitude: ' + this.longitude);
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  getMeeting(id: number) {
    this.httpService.getMeeting(id).subscribe(data => {
      this.meeting = data;
      console.log(data);
      this.fillWithData();
    });
  }

  updateMeeting() {
    this.getMeeting(this.updatedComponentId);
    this.meeting.Name = this.name.value;
    this.meeting.Description = this.description.value;
    this.meeting.EventDate = this.eventDate.value;
    this.meeting.Localization = new LocalizationInt();
    this.meeting.Localization.Latitude = this.latitude.toString();
    this.meeting.Localization.Longitude = this.longitude.toString();

    this.httpService.updateMeeting(this.meeting, this.meeting.EventId).subscribe(data => {
      console.log(data);
    });
  }

  fillWithData() {
    (<HTMLInputElement>document.getElementById('name')).value = this.meeting.Name.toString();
    (<HTMLInputElement>document.getElementById('description')).value = this.meeting.Description.toString();
    (<HTMLInputElement>document.getElementById('eventDate')).value = this.meeting.EventDate.toString();
    this.latitude = Number(this.meeting.Localization.Latitude);
    this.longitude = Number(this.meeting.Localization.Longitude);
  }
}
