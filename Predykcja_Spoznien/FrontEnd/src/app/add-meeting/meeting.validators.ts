import { AbstractControl, ValidationErrors } from '@angular/forms';

export class MeetingValidators {

  static shouldBeInFuture(control: AbstractControl): ValidationErrors | null{
    const dateTime = new Date(control.value);
    const currentDate = new Date();

    if (dateTime < currentDate) {
      return { shouldBeInFuture: true };
    }

    return null;
  }
}
