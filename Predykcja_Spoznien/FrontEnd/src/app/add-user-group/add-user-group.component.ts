import { AddUserInt } from './../all-groups/addUserInt';
import { Route, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HttpServiceLogin } from '../modal-login/http.service.login';
import { HttpServiceAllGroups } from '../all-groups/http.service';
import { MatSnackBar } from '@angular/material';
import { SESSION_STORAGE } from 'angular-webstorage-service';

@Component({
  selector: 'app-add-user-group',
  templateUrl: './add-user-group.component.html',
  styleUrls: ['./add-user-group.component.css']
})
export class AddUserGroupComponent implements OnInit {
  groupID: string;
  login: String;
  Email: AddUserInt;


  constructor(private loginService: HttpServiceLogin, private groupsService: HttpServiceAllGroups,
    private snackBar: MatSnackBar, private router: Router) { }

  ngOnInit() {
    if (!this.checkToken()) {
      this.snackBar.open('Nie masz uprawnien do tej czynnosci!');
      this.router.navigate(['/login']);
    } else {
      this.getFromLoacl();
      this.Email = new AddUserInt();
    }
  }

  checkToken() {
    if (this.loginService.AccesToken !== '') {
      return true;
    } else {
      return false;
    }
  }

  getFromLoacl() {
    this.groupID = sessionStorage.getItem('groupIdForAddUserGroup');
    console.log('From addUser: ' + this.groupID);
  }

  addUser() {
    this.groupsService.addUser(this.Email, parseInt(this.groupID, 10)).subscribe(res => {
      console.log(res);
      if (res !== null) {
        this.snackBar.open('Błąd podczas wysyłania zaproszenia', 'Zamknij');
      } else {
        this.snackBar.open('Wysłano zaproszenie', 'Zamknij');
      }
      this.router.navigate(['/groups']);
    });
  }
}
