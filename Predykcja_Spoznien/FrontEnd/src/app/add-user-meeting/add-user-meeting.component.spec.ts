import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserMeetingComponent } from './add-user-meeting.component';

describe('AddUserMeetingComponent', () => {
  let component: AddUserMeetingComponent;
  let fixture: ComponentFixture<AddUserMeetingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUserMeetingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
