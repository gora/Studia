import { AddUserInt } from './../all-groups/addUserInt';
import { Component, OnInit } from '@angular/core';
import { HttpServiceLogin } from '../modal-login/http.service.login';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { HttpServiceEvents } from '../all-meetings/http.service';

@Component({
  selector: 'app-add-user-meeting',
  templateUrl: './add-user-meeting.component.html',
  styleUrls: ['./add-user-meeting.component.css']
})
export class AddUserMeetingComponent implements OnInit {
  Email: AddUserInt;
  MeetingID: number;

  constructor(private loginService: HttpServiceLogin, private snackBar: MatSnackBar,
    private router: Router, private meetingService: HttpServiceEvents) { }

  ngOnInit() {
    if (!this.checkToken()) {
      this.snackBar.open('Nie masz dostepu do tej zawartosci!', 'Zamknij');
      this.router.navigate(['/login']);
    } else {
      this.Email = new AddUserInt();
    }
  }

  checkToken() {
    if (this.loginService.AccesToken !== '') {
      return true;
    } else {
      return false;
    }
  }

  addUser() {
    const id = sessionStorage.getItem('meetingIdForAddUserMeeting');
    this.meetingService.addUser(this.Email, parseInt(id, 10)).subscribe(res => {
      console.log(res);
      if (res !== null) {
        this.snackBar.open('Błąd podczas wysyłania zaproszenia', 'Zamknij');
      } else {
        this.snackBar.open('Wysłano zaproszenie', 'Zamknij');
      }
      this.router.navigate(['/meetings']);
    });
  }
}
