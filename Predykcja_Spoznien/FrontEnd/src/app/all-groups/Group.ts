export class GroupInt {
  public GroupId: String;
  public OwnerUserId?: String;
  public Name?: String;
  public Description?: String;
  public Users?: String;
}
