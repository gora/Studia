import { HttpServiceAllGroups } from './http.service';
import { Component, OnInit, Input, group } from '@angular/core';
import { Router } from '@angular/router';
import { GroupInt } from './Group';
import { HttpServiceLogin } from '../modal-login/http.service.login';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-all-groups',
  templateUrl: './all-groups.component.html',
  styleUrls: ['./all-groups.component.css']
})
export class AllGroupsComponent implements OnInit {

  GroupList: Array<GroupInt>;

  constructor(private router: Router, private httpService: HttpServiceAllGroups,
    private loginService: HttpServiceLogin, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    if (!this.checkToken()) {
      this.snackBar.open('Nie możesz przeglądać grup! Zaloguj się.', 'Zamknij');
      this.router.navigate(['/login']);
    } else {
      this.getGroups();
    }
  }

  checkToken() {
    const accessToken = this.loginService.AccesToken;

    if (accessToken !== '') {
      return true;
    } else {
      return false;
    }
  }

  getGroups() {
    this.httpService.getGroups().subscribe(groups => {
      console.log(groups);
      this.GroupList = groups;
    });
  }

  checkGroup(groupId: Number) {
    this.router.navigate(['/group', groupId]);
  }

  deleteGroup(id) {
    this.httpService.deleteGroup(id).subscribe();

    this.sleep(1000);
    location.reload();
  }

  sleep(miliseconds) {
    const start = new Date().getTime();

    for (let i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > miliseconds) {
        break;
      }
    }
  }
}
