import { AddUserInt } from './addUserInt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GroupInt } from './Group';
import { Observable } from 'rxjs/Observable';
import { SingleGroupInt } from '../group-info/SingleGroupInt';
import { HttpServiceLogin } from '../modal-login/http.service.login';
import { Headers, Http, HttpModule, Jsonp } from '@angular/http';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class HttpServiceAllGroups {
  accessToken: String = '';

  constructor(private http: HttpClient, private loginService: HttpServiceLogin) {
  }

  getGroups(): Observable<Array<GroupInt>> {
    this.accessToken = this.loginService.AccesToken;
    const headersForGroupsAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.get<Array<GroupInt>>('https://predictionofdelays.azurewebsites.net/api/account/Groups'
      , {headers: headersForGroupsAPI});
  }

  addGroup(newGroup: SingleGroupInt): Observable<SingleGroupInt> {
    this.accessToken = this.loginService.AccesToken;
    const headersForGroupsAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.post<SingleGroupInt>('https://predictionofdelays.azurewebsites.net/api/Groups',
      newGroup, {headers: headersForGroupsAPI}).map(res => res);
  }

  deleteGroup(id: number) {
    this.accessToken = this.loginService.AccesToken;
    const headersForGroupsAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.delete('https://predictionofdelays.azurewebsites.net/api/Groups/' + id,
    {headers: headersForGroupsAPI});
  }

  addUser(Email: AddUserInt, id: number) {
    this.accessToken = this.loginService.AccesToken;
    const headerForGroupsAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.post('http://predictionofdelays.azurewebsites.net/api/Groups/' + id + '/emailInvites', Email,
    {headers: headerForGroupsAPI});
  }

  deleteUser(groupId: Number, userId: string) {
    this.accessToken = this.loginService.AccesToken;
    const headersForGroupsAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.delete('http://predictionofdelays.azurewebsites.net/api/Groups/' + groupId + '/members/' + userId,
    {headers: headersForGroupsAPI});
  }
}
