import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllInvitesComponent } from './all-invites.component';

describe('AllInvitesComponent', () => {
  let component: AllInvitesComponent;
  let fixture: ComponentFixture<AllInvitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllInvitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllInvitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
