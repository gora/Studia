import { InvitesInt, GroupInv, EventInv, DelaysInt } from './invitesInt';
import { MatSnackBar } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { HttpServiceLogin } from '../modal-login/http.service.login';
import { Router } from '@angular/router';
import { HttpServiceInvites } from './http.service';

@Component({
  selector: 'app-all-invites',
  templateUrl: './all-invites.component.html',
  styleUrls: ['./all-invites.component.css']
})
export class AllInvitesComponent implements OnInit {
  invitesList: InvitesInt = new InvitesInt();
  invitesListGroups: Array<GroupInv> = new Array();
  invitesListEvents: Array<EventInv> = new Array();

  constructor(private loginService: HttpServiceLogin, private snackBar: MatSnackBar,
    private router: Router, private invitesService: HttpServiceInvites) { }

  ngOnInit() {
    if (!this.checkToken()) {
      this.snackBar.open('Nie masz uprawnien do tej czynnosci.', 'Zamknij');
      this.router.navigate(['/login']);
    } else {
      this.getInvites();
    }
  }

  checkToken() {
    if (this.loginService.AccesToken !== '') {
      return true;
    } else {
      return false;
    }
  }

  acceptInviteEvent(eventId, inviteId) {
    console.log('EventId: ' + eventId + ' inviteId: ' + inviteId);

    this.invitesService.acceptInviteEvent(inviteId, eventId).subscribe(res => {
      console.log(res);
      this.snackBar.open('Zaproszenie zaakceptowane', 'Zamknij');
      this.router.navigate(['/meetings']);
    });
  }

  deleteInviteEvent(eventId, inviteId) {
    this.invitesService.deleteInviteEvent(inviteId, eventId).subscribe(res => {
      console.log(res);
      this.snackBar.open('Zaproszenie odrzucone', 'Zaknij');
      this.router.navigate(['/meetings']);
    });
  }

  acceptInviteGroup(groupId, inviteId) {
    this.invitesService.acceptInviteGroup(inviteId, groupId).subscribe(res => {
      this.snackBar.open('Zaposzenie zaakceptowane', 'Zamknij');
      console.log(res);
      this.router.navigate(['/groups']);
    });
  }

  deleteInviteGroup(groupId, iviteId) {
    this.invitesService.deleteInviteGroup(iviteId, groupId).subscribe(res => {
      this.snackBar.open('Zaproszenie odrzucone', 'Zamknij');
      console.log(res);
      this.router.navigate(['/groups']);
    });
  }

  getInvites() {
    this.invitesService.getInvites().subscribe(invites => {
      this.invitesListEvents = invites.EventInvites;
      this.invitesListGroups = invites.GroupInvites;
    });
  }
}
