import { InvitesInt, DelaysInt } from './invitesInt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpServiceLogin } from '../modal-login/http.service.login';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class HttpServiceInvites {
  accessToken: String = '';

  constructor(private http: HttpClient, private loginService: HttpServiceLogin) {
  }

  getInvites(): Observable<InvitesInt> {
    this.accessToken = this.loginService.AccesToken;
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.get<InvitesInt>('https://predictionofdelays.azurewebsites.net/api/Account/invites',
      {headers: headersForInvitesAPI});
  }

  acceptInviteEvent(inviteId, eventId): Observable<any> {
    this.accessToken = this.loginService.AccesToken;
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.post<any>('https://predictionofdelays.azurewebsites.net/api/events/' + eventId + '/invites/' + inviteId,
    JSON.stringify(''), {headers: headersForInvitesAPI});
  }

  deleteInviteEvent(inviteId, eventId): Observable<any> {
    this.accessToken = this.loginService.AccesToken;
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.delete<any>('https://predictionofdelays.azurewebsites.net/api/Events/' + eventId + '/invites/' + inviteId,
    {headers: headersForInvitesAPI});
  }

  acceptInviteGroup(inviteId, groupId): Observable<any> {
    this.accessToken = this.loginService.AccesToken;
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.post<any>('https://predictionofdelays.azurewebsites.net/api/Groups/' + groupId + '/invites/' + inviteId,
    JSON.stringify(''), {headers: headersForInvitesAPI});
  }

  deleteInviteGroup(inviteId, groupId): Observable<any> {
    this.accessToken = this.loginService.AccesToken;
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.delete<any>('https://predictionofdelays.azurewebsites.net/api/Groups/' + groupId + '/invites/' + inviteId,
    {headers: headersForInvitesAPI});
  }

  insertLatency(eventId, delay: DelaysInt): Observable<DelaysInt> {
    this.accessToken = this.loginService.AccesToken;
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.post<DelaysInt>('https://predictionofdelays.azurewebsites.net/api/Events/' + eventId + '/delays', delay,
    {headers: headersForInvitesAPI});
  }

  getLatency(eventId, userId): Observable<DelaysInt> {
    this.accessToken = this.loginService.AccesToken;
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.get<DelaysInt>('https://predictionofdelays.azurewebsites.net/api/Events/' + eventId + '/delays/' + userId,
    {headers: headersForInvitesAPI});
  }
}
