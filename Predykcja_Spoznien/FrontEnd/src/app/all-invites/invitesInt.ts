import { UserInt } from '../group-info/UserInt';
import { MeetingInt } from '../all-meetings/meetingInt';
import { GroupInt } from '../all-groups/Group';
export class InvitesInt {
  public EventInvites: Array<EventInv>;
  public GroupInvites: Array<GroupInv>;
}

export class EventInv {
  public Invited: UserInt;
  public Sender: UserInt;
  public Event: MeetingInt;
  public EventInviteId: String;
}

export class GroupInv {
  public Invited: UserInt;
  public Sender: UserInt;
  public Group: GroupInt;
  public GroupInviteId: String;
}

export class DelaysInt {
  public amountOfMinutes: number;
}
