import { Component, OnInit } from '@angular/core';
import { HttpServiceEvents } from './http.service';
import { Router } from '@angular/router';
import { MeetingInt } from './meetingInt';
import { HttpServiceLogin } from '../modal-login/http.service.login';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-all-meetings',
  templateUrl: './all-meetings.component.html',
  styleUrls: ['./all-meetings.component.css']
})
export class AllMeetingsComponent implements OnInit {
  EventsList: Array<MeetingInt>;

  constructor(private router: Router, private httpService: HttpServiceEvents, private loginService: HttpServiceLogin,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    if (!this.checkToken()) {
      this.snackBar.open('Nie możesz przeglądać spotkań! Zaloguj się.', 'Zamknij');
      this.router.navigate(['/login']);
    } else {
      this.getEvents();
    }
  }

  checkToken() {
    const accessToken = this.loginService.AccesToken;

    if (accessToken !== '') {
      return true;
    } else {
      return false;
    }
  }

  getEvents() {
    this.httpService.getEvents().subscribe(event => {
      this.EventsList = event;
      console.log(event);
    });
  }

  checkMeeting(meetId: Number) {
    this.router.navigate(['/meet', meetId]);
  }

}
