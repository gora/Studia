import { HttpServiceLogin } from './../modal-login/http.service.login';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MeetingInt } from './meetingInt';
import { Observable } from 'rxjs/Observable';
import { SingleMeeting } from '../meeting-info/SingleMeeting';
import { AddUserInt } from '../all-groups/addUserInt';

@Injectable()
export class HttpServiceEvents {
  accessToken: String = '';

  constructor(private http: HttpClient, private loginService: HttpServiceLogin) {
  }

  getEvents(): Observable<Array<MeetingInt>> {
    this.accessToken = this.loginService.AccesToken;
    const headersForMeetingsAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.get<Array<MeetingInt>>('https://predictionofdelays.azurewebsites.net/api/account/Events',
      {headers: headersForMeetingsAPI}).map(dane => dane);
  }

  getMeeting(id: number): Observable<SingleMeeting> {
    this.accessToken = this.loginService.AccesToken;
    const headersForMeetingsAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.get<SingleMeeting>('https://predictionofdelays.azurewebsites.net/api/Events/' + id,
      {headers: headersForMeetingsAPI});
  }

  updateMeeting(meeting: SingleMeeting, eventId: number): Observable<SingleMeeting> {
    this.accessToken = this.loginService.AccesToken;
    const headersForMeetingsAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.put<SingleMeeting>('https://predictionofdelays.azurewebsites.net/api/Events/' + eventId, meeting,
    {headers: headersForMeetingsAPI});
  }

  addMeeting(meeting: SingleMeeting): Observable<SingleMeeting> {
    this.accessToken = this.loginService.AccesToken;
    const headersForMeetingsAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.post<SingleMeeting>('https://predictionofdelays.azurewebsites.net/api/Events/', meeting,
      {headers: headersForMeetingsAPI});
  }

  addUser(Email: AddUserInt, id: number) {
    this.accessToken = this.loginService.AccesToken;
    const headersForMeetingsAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken});

    return this.http.post('https://predictionofdelays.azurewebsites.net/api/Events/' + id + '/eventInvites', Email,
    {headers: headersForMeetingsAPI});
  }
}
