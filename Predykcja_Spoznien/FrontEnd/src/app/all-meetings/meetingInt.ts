import { SingleMeeting } from './../meeting-info/SingleMeeting';
import { UserInt } from './../group-info/UserInt';
import { LocalizationInt } from '../add-meeting/LocalizationInt';

export class MeetingInt {
  public ApplicationUser: String;
  public ApplicationUserId: String;
  public Event: SingleMeeting;
  public EventId: number;
  public MinutesOfDelay: number;
}
