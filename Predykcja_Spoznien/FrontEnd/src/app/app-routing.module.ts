import { AddUserGroupComponent } from './add-user-group/add-user-group.component';
import { AddUserMeetingComponent } from './add-user-meeting/add-user-meeting.component';
import { RegistrationComponent } from './registration/registration.component';
import { MainPageComponent } from './main-page/main-page.component';
import { AddGroupComponent } from './add-group/add-group.component';
import { AddMeetingComponent } from './add-meeting/add-meeting.component';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {ModalLoginComponent} from './modal-login/modal-login.component';
import { AllGroupsComponent } from './all-groups/all-groups.component';
import { HomeComponent } from './home/home.component';
import { AllMeetingsComponent } from './all-meetings/all-meetings.component';
import { GroupInfoComponent } from './group-info/group-info.component';
import { InfoAboutAuthorsComponent } from './info-about-authors/info-about-authors.component';
import { UsingApplicationComponent } from './using-application/using-application.component';
import { MeetingInfoComponent } from './meeting-info/meeting-info.component';
import { LocalizationComponent } from './localization/localization.component';
import { MessageSnackbarComponent } from './message-snackbar/message-snackbar.component';
import { GoogleMapComponent } from './google-map/google-map.component';
import { AllInvitesComponent } from './all-invites/all-invites.component';

const routes: Routes = [
    { path: 'meetings', component: AllMeetingsComponent },
    { path: 'groups', component: AllGroupsComponent, pathMatch: 'full' },
    { path: 'login', component: ModalLoginComponent, pathMatch: 'full' },
    { path: 'modalLogin', component: ModalLoginComponent },
    { path: 'group/:id', component: GroupInfoComponent },
    { path: 'meet/:id', component: MeetingInfoComponent },
    { path: 'groups', component: AllGroupsComponent },
    { path: 'infoAuthors', component: InfoAboutAuthorsComponent},
    { path: 'howToUse', component: UsingApplicationComponent},
    { path: 'addMeeting', component: AddMeetingComponent},
    { path: 'addMeeting/:id', component: AddMeetingComponent },
    { path: 'addGroup', component: AddGroupComponent},
    { path: 'startPage', component: MainPageComponent},
    { path: 'registration', component: RegistrationComponent},
    { path: 'localization', component: LocalizationComponent},
    {
      path: 'message',
      component: MessageSnackbarComponent,
      outlet: 'messageSnackbar'
    },
    {path: 'googleMap', component: GoogleMapComponent},
    {path: 'meet/:id/addUserMeeting', component: AddUserMeetingComponent},
    {path: 'group/:id/addUserGroup', component: AddUserGroupComponent},
    {path: 'allinvites', component: AllInvitesComponent}
  ];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
