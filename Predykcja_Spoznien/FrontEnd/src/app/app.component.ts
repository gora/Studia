import { Component, enableProdMode, TemplateRef, OnInit } from '@angular/core';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  enableGroup: boolean;

  ngOnInit() {
    this.router.navigate(['/startPage']);
    this.enableGroup = false;
  }

  constructor(private router: Router) { }

  setEnableGroup() {
    this.enableGroup = true;
  }
}
