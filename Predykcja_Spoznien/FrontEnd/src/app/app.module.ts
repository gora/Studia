import { HttpServiceInvites } from './all-invites/http.service';
import { AddUserGroupComponent } from './add-user-group/add-user-group.component';
import { HttpServiceEvents } from './all-meetings/http.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpServiceAllGroups } from './all-groups/http.service';
import { HttpServiceGroupInfo } from './group-info/http.service';
import { GeoLocationService } from './google-map/geo-location.service';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import {MatRadioModule} from '@angular/material/radio';

import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { GroupInfoComponent } from './group-info/group-info.component';
import { AllGroupsComponent } from './all-groups/all-groups.component';
import { AppRoutingModule } from './/app-routing.module';
import { ModalLoginComponent } from './modal-login/modal-login.component';
import { AllMeetingsComponent } from './all-meetings/all-meetings.component';
import { AddMeetingComponent } from './add-meeting/add-meeting.component';
import { HomeComponent } from './home/home.component';
import { InfoAboutAuthorsComponent } from './info-about-authors/info-about-authors.component';
import { UsingApplicationComponent } from './using-application/using-application.component';
import { AddGroupComponent } from './add-group/add-group.component';
import { MainPageComponent } from './main-page/main-page.component';
import { RegistrationComponent } from './registration/registration.component';
import { MeetingInfoComponent } from './meeting-info/meeting-info.component';
import { LocalizationComponent } from './localization/localization.component';
import { HttpServiceRegistration } from './registration/http.service';
import { MessageSnackbarComponent } from './message-snackbar/message-snackbar.component';
import { GoogleMapComponent } from './google-map/google-map.component';
import { AgmDirectionModule } from 'agm-direction';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpServiceLogin } from './modal-login/http.service.login';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddUserMeetingComponent } from './add-user-meeting/add-user-meeting.component';
import { AllInvitesComponent } from './all-invites/all-invites.component';

@NgModule({
  declarations: [
    AppComponent,
    GroupInfoComponent,
    AllGroupsComponent,
    ModalLoginComponent,
    AllMeetingsComponent,
    AddMeetingComponent,
    HomeComponent,
    InfoAboutAuthorsComponent,
    UsingApplicationComponent,
    AddGroupComponent,
    MainPageComponent,
    RegistrationComponent,
    MeetingInfoComponent,
    LocalizationComponent,
    MessageSnackbarComponent,
    GoogleMapComponent,
    NavbarComponent,
    AddUserGroupComponent,
    AddUserMeetingComponent,
    AllInvitesComponent
  ],
  imports: [
    HttpClientModule,
    HttpModule,
    BrowserModule,
    AppRoutingModule,
    MatSnackBarModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatIconModule,
    AgmDirectionModule, // agm-direction
    MatRadioModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDRIL0Hil69XlKdbMHCbwecfjJyV2oR-Ls',
      libraries: ['places']
    })
  ],
  providers: [HttpServiceGroupInfo, HttpServiceAllGroups,
    HttpServiceEvents, HttpServiceRegistration, HttpServiceLogin,
    MessageSnackbarComponent, GeoLocationService, HttpServiceInvites],
  bootstrap: [AppComponent]
})
export class AppModule { }
