import { Component, OnInit, ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import {HttpClient} from '@angular/common/http';
import { GeoLocationService } from './geo-location.service';


@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
  `]
})

export class GoogleMapComponent implements OnInit {
  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  dir = undefined;
  // to tests
  public lat: Number = 51.109023;
  public lng: Number = 17.060534;

  public longitudeDestination: number;
  public latitudeDestination: number;
  mapdistance: any;
  byroaddistance: any;
  byroadtime: any;
  withoutroaddistance: any;
  data: any;
  geolocationPosition: any;
  durationInMs: any; // Time from point to point (distance) in miliseconds

  newDate1: Date;


  transport: any = '';
  radioButtons = [
    {icon: 'directions_walk', meanOfTransport: 'walk'},
    {icon: 'directions_car', meanOfTransport: 'car'},
    {icon: 'directions_transit', meanOfTransport: 'transit'}
  ];


  @ViewChild('search')
  public searchElementRef: ElementRef;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, private http: HttpClient,
    private geolocation: GeoLocationService
  ) {
    this.geolocation.positionChanged.subscribe(location => {
      this.latitude = location.coords.latitude;
      this.longitude = location.coords.longitude;
      console.log('Lat:' + this.latitude);
    }); // console.log('Lat:' + location.coords.latitude));
  }

  ngOnInit() {
    // Setting google maps defaults
    this.zoom = 4;
    this.latitude = 51.1078852;
    this.longitude = 17.0385376;

    this.latitudeDestination = 51.1052862455; // Warsaw
    this.longitudeDestination = 17.055921443;


    // Create search FormControl
    this.searchControl = new FormControl();

    // Set current position
    this.setCurrentPosition();


    this.getGoogleMapsRequest();
    this.geolocation.enable();

    // Load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          // Verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          // Set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  // Method to get your current position
  public getCurrentPosition() {
    if (window.navigator && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
          position => {
              this.geolocationPosition = position,
                  console.log(position);
              console.log('Twoja obecna pozycja');
              console.log('Latitude ' + position.coords.latitude);
              console.log('Longitude ' + position.coords.longitude);
          },
          error => {
              switch (error.code) {
                  case 1:
                      console.log('Permission Denied');
                      break;
                  case 2:
                      console.log('Position Unavailable');
                      break;
                  case 3:
                      console.log('Timeout');
                      break;
              }
          }
      );
    }
  }

  onChooseLocation(event) {
      console.log(event);
      this.latitude = event.coords.lat;
      this.longitude = event.coords.lng;
  }

  // Route between two locations - second map
  getDirection() {
    this.dir = {
      // Origin Strzegomska 42B, Wrocław, Polska
      origin: { lat: 51.109023, lng: 17.060534 },
      // Destination: C-3, Janiszewskiego, 52-007 Wrocław, Polska
      destination: { lat: 51.113509, lng: 16.993845 }
    };
  }

  getGoogleMapsRequest() {
    const googlekey = 'AIzaSyDRIL0Hil69XlKdbMHCbwecfjJyV2oR-Ls';
    // const origins = this.latitude + ',' + this.longitude;
    const origins = '51.109023,17.060534';
    // const destinations = this.latitudeDestination + ',' + this.longitudeDestination;
    const destinations = '51.113509,16.993845';
    /*End here*/
    let httpResponse = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='
    + origins + '&destinations=' + destinations;
    if (this.transport === 'walk') {
      httpResponse += '&mode=walking';
    }
    if (this.transport === 'car') {
      httpResponse += '&mode=driving';
    }
    if (this.transport === 'transit') {
      httpResponse += '&mode=transit';
    }
    /*This is count by road map of the earth*/
    const value = this.http.get(httpResponse + '&key=' + googlekey + '').
    subscribe(data => {
      console.log(data);
      this.mapdistance = data;
      this.byroaddistance = this.mapdistance.rows[0].elements[0].distance.text;
      this.byroadtime = this.mapdistance.rows[0].elements[0].duration.text;
      // In miliseconds:
      this.durationInMs = this.mapdistance.rows[0].elements[0].duration.value;
      console.log('Czas drogi: ' + this.byroadtime);
      console.log('Dystans jadąc samochodem: ' + this.byroaddistance );
      console.log('Czas przebycia drogi w ms: ' + this.durationInMs);
    });
    // Position from GeoService
    console.log('x: ' + this.geolocation.positionLatitude);
    console.log('y: ' + this.geolocation.positionLongitude);
  }

  // test to count difference between datas
  timeDifference() {
      const date1 = '2017-03-05 11:26:16';
      const date2 = '2017-03-06 12:26:16';

      const diffInMs: number = Date.parse(date2) - Date.parse(date1);
      const diffInHours: number = diffInMs / 1000 / 60 / 60;
      const diffInMins: number = diffInMs / 1000 / 60;
      console.log('In hours difference: ' + diffInHours);
      console.log('In minutes difference: ' + diffInMins);


      this.newDate1 = new Date();
      console.log('Current date: ' + this.newDate1);
      this.newDate1.setMinutes(this.newDate1.getMinutes() + 20);
      console.log('Time after 20 mins added: ' + this.newDate1);
  }
}
