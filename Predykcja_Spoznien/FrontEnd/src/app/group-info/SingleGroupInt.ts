import { UserInt } from './UserInt';

export class SingleGroupInt {
  public GroupId: Number;
  public OwnerUserId: String;
  public Name: String;
  public Description: String;
  public Users: Array<UserInt>;

  public SingleGroupInt() {
  }
}
