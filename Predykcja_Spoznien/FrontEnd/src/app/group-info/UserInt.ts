export class UserInt {
  public Id: Number;
  public Email: String;
  public PhoneNumber: String;
  public UserName: String;
  public FirstName: String;
  public LastName: String;
}
