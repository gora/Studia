import { HttpServiceAllGroups } from './../all-groups/http.service';
import { HttpServiceGroupInfo } from './http.service';
import { SingleGroupInt } from './SingleGroupInt';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { UserInt } from './UserInt';
import {map, switchMap} from 'rxjs/operators';
import { HttpServiceLogin } from '../modal-login/http.service.login';
import { MatSnackBar } from '@angular/material';
import { SESSION_STORAGE } from 'angular-webstorage-service';

@Component({
  selector: 'app-group-info',
  templateUrl: './group-info.component.html',
  styleUrls: ['./group-info.component.css']
})
export class GroupInfoComponent implements OnInit {
  GroupInfno: SingleGroupInt;
  Informations: String;
  Users: Array<UserInt>;

  constructor(private route: ActivatedRoute, private httpService: HttpServiceGroupInfo, private loginService: HttpServiceLogin,
    private snackBar: MatSnackBar, private router: Router, private httpGroups: HttpServiceAllGroups) { }

  ngOnInit() {
    if (!this.checkToken()) {
      this.snackBar.open('Nie możesz przeglądać tej zawartosci! Zaloguj się.', 'Zamknij');
      this.router.navigate(['/login']);
    } else {
      this.route.params.pipe(
        map((params) => {
          return params.id;
        }),
        switchMap(id => this.getSingleGroup(id))
      ).subscribe(users => {
        console.log(users);
        this.GroupInfno = users;
        this.Informations = this.GroupInfno.Description;
        this.Users = this.GroupInfno.Users;
      });
    }
  }

  checkToken() {
    const accessToken = this.loginService.AccesToken;

    if (accessToken !== '') {
      return true;
    } else {
      return false;
    }
  }

  getSingleGroup(id: number) {
      return this.httpService.getSingleGroup(id);
  }

  saveInLocal() {
    sessionStorage.setItem('groupIdForAddUserGroup', this.GroupInfno.GroupId.toString());
    console.log(this.GroupInfno.GroupId);
  }

  deleteUser(userId: string) {
    console.log('User id: ' + userId);
    this.httpGroups.deleteUser(this.GroupInfno.GroupId, userId).subscribe(response => console.log(response));
  }

}
