import { SingleGroupInt } from './SingleGroupInt';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';

@Injectable()
export class HttpServiceGroupInfo {
  constructor(private http: HttpClient) {
  }


  getSingleGroup(id: number): Observable<SingleGroupInt> {
    return this.http.get<SingleGroupInt>('https://predictionofdelays.azurewebsites.net/api/Groups/' + id);
  }

  getGroups(): Observable<Array<SingleGroupInt>> {
    return this.http.get<Array<SingleGroupInt>>('https://predictionofdelays.azurewebsites.net/api/Groups');
  }
}
