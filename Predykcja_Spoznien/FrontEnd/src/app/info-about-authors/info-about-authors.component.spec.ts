import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoAboutAuthorsComponent } from './info-about-authors.component';

describe('InfoAboutAuthorsComponent', () => {
  let component: InfoAboutAuthorsComponent;
  let fixture: ComponentFixture<InfoAboutAuthorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoAboutAuthorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoAboutAuthorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
