import { HttpServiceInvites } from './../all-invites/http.service';
import { InvitesInt, EventInv, GroupInv } from './../all-invites/invitesInt';
import { HttpServiceAllGroups } from './../all-groups/http.service';
import { Component, OnInit } from '@angular/core';

import { HttpServiceEvents } from './../all-meetings/http.service';
import { HttpServiceLogin } from './../modal-login/http.service.login';
import { MeetingInt } from '../all-meetings/meetingInt';
import { GroupInt } from '../all-groups/Group';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  information: String = '';
  eventsList: Array<MeetingInt> = new Array();
  groupsList: Array<GroupInt> = new Array();
  invitesList: InvitesInt = new InvitesInt();
  invitesListEvent: Array<EventInv> = new Array();
  invitesListGroups: Array<GroupInv> = new Array();

  constructor(private meetingService: HttpServiceEvents, private loginService: HttpServiceLogin,
    private groupService: HttpServiceAllGroups, private router: Router, private invitesService: HttpServiceInvites) { }

  checkToken() {
    if (this.loginService.AccesToken !== '') {
      return true;
    } else {
      return false;
    }
  }

  ngOnInit() {
    const accessToken = this.loginService.AccesToken;

    if (accessToken === '') {
      this.information = 'Witaj w aplikacji! \nJeśli chcesz dowiedzieć ' +
        'się jak z niej korzystać przejdź do zakładki Menu -> Korzystanie z aplikacji.';
    } else {
      this.getEvents();
      this.getGroups();
      this.getInvites();
    }
  }

  getEvents() {
    let count: number;
    count = 1;

    this.meetingService.getEvents().subscribe(event => {
      while (count < 3 && count < event.length) {
        this.eventsList.push(event[(event.length) - count]);
        count += 1;
      }
    });
  }

  getGroups() {
    let count: number;
    count = 1;

    this.groupService.getGroups().subscribe(group => {
      while (count < 3 && count < group.length) {
        this.groupsList.push(group[(group.length) - count]);
        count += 1;
      }
    });
  }

  getInvites() {
    this.invitesService.getInvites().subscribe(invites => {
      console.log(invites);
      this.invitesListEvent = invites.EventInvites;
      this.invitesListGroups = invites.GroupInvites;
    });
  }

  checkGroup(groupId: Number) {
    this.router.navigate(['/group', groupId]);
  }

  checkMeeting(meetId: Number) {
    this.router.navigate(['/meet', meetId]);
  }

}
