import { LocalizationInt } from './../add-meeting/LocalizationInt';
import { UserInt } from './../group-info/UserInt';

export class SingleMeeting {
  public EventId: number;
  public OwnerUserId: String;
  public Localization: LocalizationInt;
  public Name: String;
  public EventDate: String;
  public Description: String;
  public Users: Array<UserInt>;

  public SingleMeeting() {
  }
}
