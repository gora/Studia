import { HttpServiceInvites } from './../all-invites/http.service';

import { DelaysInt } from './../all-invites/invitesInt';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { MatSnackBar } from '@angular/material';
import { HttpServiceEvents } from './../all-meetings/http.service';
import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { SingleMeeting } from './SingleMeeting';
import { UserInt } from '../group-info/UserInt';
import { HttpServiceLogin } from '../modal-login/http.service.login';
import { HttpClient } from '@angular/common/http';
import { GeoLocationService } from '../google-map/geo-location.service';
import { MapsAPILoader } from '@agm/core';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-meeting-info',
  templateUrl: './meeting-info.component.html',
  styleUrls: ['./meeting-info.component.css']
})
export class MeetingInfoComponent implements OnInit {
  meetingInfo: SingleMeeting;
  Users: Array<any>;

  informations: String;
  date: String;
  dateDisplay: String;
  delay: number;
  delayh: number;
  delaym: number;

  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  public longitudeDestination: number;
  public latitudeDestination: number;
  geolocationPosition: any;
  transport: any = '';
  mapdistance: any;
  byroaddistance: any;
  diffInMs: any;
  byroadtime: String;
  durationInMs: any; // Time from point to point (distance) in miliseconds


  @ViewChild('search')
  public searchElementRef: ElementRef;


  constructor(private route: ActivatedRoute, private httpService: HttpServiceEvents, private loginService: HttpServiceLogin,
    private snackBar: MatSnackBar, private router: Router, private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, private http: HttpClient,
    private geolocation: GeoLocationService,
    private invitesService: HttpServiceInvites) { }


  ngOnInit() {
    if (!this.checkToken()) {
      this.snackBar.open('Nie możesz przeglądać tej zawartosci! Zaloguj się.', 'Zamknij');
      this.router.navigate(['/login']);
    } else {
      this.route.params.pipe(
        map((params) => {
          return params.id;
        }),
        switchMap(id => this.getsingleMeeting(id))
      ).subscribe(meeting => {
        this.meetingInfo = meeting;
        this.Users = this.meetingInfo.Users;
        this.informations = meeting.Description;
        this.date = meeting.EventDate;

        this.dateDisplay = this.date;
        const rep = /T/gi;
        this.dateDisplay = this.dateDisplay.replace(rep, ' ');

        console.log(this.Users);

        // Setting google maps defaults
        this.zoom = 4;
        this.latitude = 51.1078852;
        this.longitude = 17.0385376;

        this.latitudeDestination = 51.1052862455; // Warsaw
        this.longitudeDestination = 17.055921443;


        // Create search FormControl
        this.searchControl = new FormControl();


        this.getGoogleMapsRequest();
        this.geolocation.enable();

        // Load Places Autocomplete
        this.mapsAPILoader.load().then(() => {
          const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
            types: ['address']
          });
          autocomplete.addListener('place_changed', () => {
            this.ngZone.run(() => {
              // get the place result
              const place: google.maps.places.PlaceResult = autocomplete.getPlace();

              // Verify result
              if (place.geometry === undefined || place.geometry === null) {
                return;
              }

              // Set latitude, longitude and zoom
              this.latitude = place.geometry.location.lat();
              this.longitude = place.geometry.location.lng();
              this.zoom = 12;
            });
          });
        });
      });
    }
  }

  checkToken() {
    const accessToken = this.loginService.AccesToken;

    if (accessToken !== '') {
      return true;
    } else {
      return false;
    }
  }

  getsingleMeeting(id: number) {
    return this.httpService.getMeeting(id);
  }

  saveInLocal() {
    sessionStorage.setItem('meetingIdForAddUserMeeting', this.meetingInfo.EventId.toString());
  }

  public getCurrentPosition() {
    if (window.navigator && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        position => {
          this.geolocationPosition = position,
            console.log(position);
          console.log('Twoja obecna pozycja');
          console.log('Latitude ' + position.coords.latitude);
          console.log('Longitude ' + position.coords.longitude);
        },
        error => {
          switch (error.code) {
            case 1:
              console.log('Permission Denied');
              break;
            case 2:
              console.log('Position Unavailable');
              break;
            case 3:
              console.log('Timeout');
              break;
          }
        }
      );
    }
  }

  getGoogleMapsRequest() {
    const googlekey = 'AIzaSyDRIL0Hil69XlKdbMHCbwecfjJyV2oR-Ls';
    // const origins = this.latitude + ',' + this.longitude;
    const destinations = this.meetingInfo.Localization.Latitude.toString() + ',' + this.meetingInfo.Localization.Longitude.toString();
    // const origins = '51.109023,17.060534';
    // const destinations = this.latitudeDestination + ',' + this.longitudeDestination;
    const origins = this.geolocation.positionLatitude + ',' + this.geolocation.positionLongitude;
    // Position from GeoService
    console.log('x: ' + this.geolocation.positionLatitude);
    console.log('y: ' + this.geolocation.positionLongitude);
    /*End here*/
    let httpResponse = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='
      + origins + '&destinations=' + destinations;
    if (this.transport === 'walk') {
      httpResponse += '&mode=walking';
    }
    if (this.transport === 'car') {
      httpResponse += '&mode=driving';
    }
    if (this.transport === 'transit') {
      httpResponse += '&mode=transit';
    }
    /*This is count by road map of the earth*/
    const value = this.http.get(httpResponse + '&key=' + googlekey + '').
      subscribe(data => {
        console.log(data);
        this.mapdistance = data;
        this.byroaddistance = this.mapdistance.rows[0].elements[0].distance.text;
        this.byroadtime = this.mapdistance.rows[0].elements[0].duration.text;
        // In miliseconds:
        this.durationInMs = this.mapdistance.rows[0].elements[0].duration.value;
        console.log('Czas drogi: ' + this.byroadtime);
        console.log('Dystans jadąc samochodem: ' + this.byroaddistance);
        console.log('Czas przebycia drogi w ms: ' + this.durationInMs);
        console.log('Opoznienie: ' + this.timeDifference());
        this.delay = this.timeDifference();
        console.log('Delay: ' + this.delay);
        this.insertLatency(this.meetingInfo.EventId, this.delay);
        this.delayh = (this.delay / 60);
        if (this.delayh < 0) {
          this.delayh = Math.ceil(this.delayh);
          this.delaym = this.delay - (this.delayh * 60);
        } else {
          this.delayh = Math.floor(this.delayh);
          this.delaym = this.delay - (this.delayh * 60);
        }
      });
  }

  timeDifference() {
    const oneHour = 1000 * 60;
    const date1 = new Date();
    const date2 = new Date(this.meetingInfo.EventDate.toString());

    const date1_ms = date1.getTime();
    const date2_ms = date2.getTime();

    this.diffInMs = date1_ms + (this.durationInMs * 1000);
    this.diffInMs = this.diffInMs - date2_ms;

    console.log('moja roznica: ' + this.diffInMs);


    return Math.round(this.diffInMs / oneHour);
  }

  // To connect
  insertLatency(eventId, minutesOfDelay) {
    const delay: DelaysInt = {
      amountOfMinutes: minutesOfDelay
    };
    this.invitesService.insertLatency(eventId, delay).subscribe(res => {
      console.log(res);
    });
  }

  // To connect
  getLatency(eventId, userId) {
    console.log(this.Users);
    this.invitesService.getLatency(eventId, userId).subscribe(latency => {
      console.log(latency);
    });
  }
}

