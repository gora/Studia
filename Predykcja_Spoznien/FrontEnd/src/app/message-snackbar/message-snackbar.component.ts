import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@Component({
  selector: 'app-message-snackbar',
  templateUrl: './message-snackbar.component.html',
  styleUrls: ['./message-snackbar.component.css']
})
export class MessageSnackbarComponent implements OnInit {

  // tutek <-- przekazywanie uzytkownika
  // http://brianflove.com/2018/03/16/ngrx-mat-snackbar/
  // https://material.angular.io/components/snack-bar/overview
  // https://medium.com/@usrlotus/custom-snackbar-using-angular2-material-67a0b576588f
  constructor(public snackBar: MatSnackBar) {

   }

  openSnackBar() {
    this.snackBar.open('Ktos tam sie spozni na spotkanie!', 'Zamknij');
    // this.snackBar.openFromComponent(MessageSnackbarDialogComponent, {
    //  duration: 500,
    // });
  }
  ngOnInit() {
  }
}
