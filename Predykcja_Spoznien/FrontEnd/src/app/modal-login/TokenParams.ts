export class TokenParams {
  public access_token: String;
  public token_type: String;
  public expires_in: number;
  public UserName: String;
  public issued: String;
  public expires: String;
}
