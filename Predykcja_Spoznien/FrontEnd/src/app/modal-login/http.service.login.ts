import { TokenParams } from './TokenParams';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Headers, Http, HttpModule } from '@angular/http';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class HttpServiceLogin {

  AccesToken: String = '';

  constructor(private http: Http) { }

  private TokenAPI = 'https://predictionofdelays.azurewebsites.net/token';

  login(userName: String, password: String): Observable<TokenParams> {
    const headersForTokenApi = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    const data = 'grant_type=password&username=' + userName + '&password=' + password;

    return this.http.post(this.TokenAPI, data, {headers: headersForTokenApi }).map(res => res.json());
  }

  logOut(): Observable<TokenParams> {
    this.TokenAPI = 'https://predictionofdelays.azurewebsites.net/Logout';
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Bearer ' + this.AccesToken});

    return this.http.post('https://predictionofdelays.azurewebsites.net/api/Account/Logout',
    {headers: headersForInvitesAPI}).map(res => res.json());
  }
}
