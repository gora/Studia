import { AbstractControl } from '@angular/forms';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

import { HttpServiceLogin } from './http.service.login';
import { TokenParams } from './TokenParams';

@Component({
  selector: 'app-modal-login',
  templateUrl: './modal-login.component.html',
  styleUrls: ['./modal-login.component.css']
})
export class ModalLoginComponent {

  token: TokenParams;

  constructor(private router: Router, private httpService: HttpServiceLogin, private snackBar: MatSnackBar) { }

  form = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.email]),
    grantType: new FormControl('password'),
    password: new FormControl('', [Validators.required,
    Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+=!()*]).{6,}$')]),
  });

  get username(): AbstractControl {
    return this.form.get('username');
  }

  get password() {
    return this.form.get('password');
  }

  registration() {
    this.router.navigate(['registration']);
  }

  login() {
    this.snackBar.open('Logowanie...', 'Zamknij');

    this.httpService.login(this.username.value, this.password.value).subscribe(data => {
      this.token = data;
      this.httpService.AccesToken = this.token.access_token;

      if (this.httpService.AccesToken !== '') {
        this.snackBar.open('Zalogowano!', 'Zamknij');
        this.router.navigate(['/startPage']);
      }

      console.log(this.token.access_token);
    });

    console.log(this.form);
  }
}
