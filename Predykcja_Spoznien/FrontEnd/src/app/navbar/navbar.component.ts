import { HttpServiceLogin } from './../modal-login/http.service.login';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  menuExpanded: boolean = false;
  groupsExpanded: boolean = false;
  eventsExpanded: boolean = false;
  profileExpanded: boolean = false;

  constructor(private authToken: HttpServiceLogin) { }

  accesToken: String;
  tokenExist: boolean;

  ngOnInit() {
    this.tokenExist = false;
  }

  checkToken() {
    this.accesToken = this.authToken.AccesToken;

    if (this.accesToken !== '') {
      this.tokenExist = true;
    }

    return this.tokenExist;
  }

  toggleDropdown(name: string, expand: boolean) {
    const hasTouchscreen = 'ontouchstart' in window;

    // Zablokuj dzialanie na urządzeniach z dotykowym wyswietlaczem
    if (!hasTouchscreen){
      if (name === 'menu') {
        this.menuExpanded = expand;
      } else if (name === 'groups') {
        this.groupsExpanded = expand;
      } else if (name === 'events') {
        this.eventsExpanded = expand;
      } else if (name === 'profile') {
        this.profileExpanded = expand;
      }
    }
  }

  logOut() {
    this.authToken.logOut().subscribe(res => {
      console.log(res);
    });
  }

}
