import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { UserLogin } from './UserLogin';

@Injectable()
export class HttpServiceRegistration {
  constructor(private http: HttpClient) {
  }

  registerUser(user: UserLogin): Observable<UserLogin> {
    return this.http.post<UserLogin>('https://predictionofdelays.azurewebsites.net/api/Account/Register', user);
  }
}
