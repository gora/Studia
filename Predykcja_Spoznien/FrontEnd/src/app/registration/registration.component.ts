import {FormControl, FormGroup} from '@angular/forms';
import { HttpServiceRegistration } from './http.service';
import { UserLogin } from './UserLogin';
import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { RegistrationValidators } from './registration.validators';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent{
  constructor(private httpService: HttpServiceRegistration) { }

  form = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    phoneNumber: new FormControl('', [Validators.required, Validators.pattern("[0-9]+")]),
    password: new FormControl('', [Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+=!()*]).{6,}$")]),
    confirmPassword: new FormControl('', [Validators.required]),
  }, RegistrationValidators.passwordsShouldMatch);

  get firstName(){
    return this.form.get('firstName');
  }

  get lastName(){
    return this.form.get('lastName');
  }

  get email(){
    return this.form.get('email');
  }

  get phoneNumber(){
    return this.form.get('phoneNumber');
  }

  get password(){
    return this.form.get('password');
  }

  get confirmPassword(){
    return this.form.get('confirmPassword');
  }

  register(){
    let user = new UserLogin();
    user.FirstName = this.firstName.value;
    user.LastName = this.lastName.value;
    user.Email = this.email.value;
    user.PhoneNumber = this.phoneNumber.value;
    user.Password = this.password.value;
    user.ConfirmPassword = this.confirmPassword.value;
    this.httpService.registerUser(user)
      .subscribe(response => {
        console.log(response);
      });
  }
}
