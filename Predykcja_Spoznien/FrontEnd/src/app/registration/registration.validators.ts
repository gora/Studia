import {ValidationErrors, AbstractControl} from '@angular/forms';

export class RegistrationValidators{
  static passwordsShouldMatch(control: AbstractControl): ValidationErrors | null{
    let password = control.get('password');
    let confirmPassword = control.get('confirmPassword');

    if (password.value !== confirmPassword.value)
        return { passwordsShouldMatch: true };

    return null;
  }
}
