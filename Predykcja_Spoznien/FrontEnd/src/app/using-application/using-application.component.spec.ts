import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsingApplicationComponent } from './using-application.component';

describe('UsingApplicationComponent', () => {
  let component: UsingApplicationComponent;
  let fixture: ComponentFixture<UsingApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsingApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsingApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
