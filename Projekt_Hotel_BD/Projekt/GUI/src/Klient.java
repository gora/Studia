
public class Klient {

	int id;
	String imie;
	String nazwisko;
	String nr_tel;
	String email;
	int adresid;

	public Klient(String imie, String nazwisko, String nr_tel, String email, int adresid) {
		super();
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.nr_tel = nr_tel;
		this.email = email;
		this.adresid = adresid;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getNr_tel() {
		return nr_tel;
	}

	public void setNr_tel(String nr_tel) {
		this.nr_tel = nr_tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAdresid() {
		return adresid;
	}

	public void setAdresid(int adresid) {
		this.adresid = adresid;
	}
}
