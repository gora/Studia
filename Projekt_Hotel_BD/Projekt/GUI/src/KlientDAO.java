import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class KlientDAO {
	
	public List<Klient> getAllKlient() throws Exception {
		List<Klient> lista = new ArrayList<>();
		Statement myStmt1 = null;
		ResultSet myRs = null;
		try {
			myStmt1 = Logowanie.myConn.createStatement();
			myRs = myStmt1.executeQuery("select * from klient");
			
			while (myRs.next()){
				Klient tempKlient = convertRowToKlient(myRs);
				lista.add(tempKlient);
			}
			return lista;
	}
		finally {
			close(myStmt1,myRs);
			}
	}

	private Klient convertRowToKlient(ResultSet myRs) throws SQLException {
		
		String imie = myRs.getString("imie");
		String nazwisko = myRs.getString("nazwisko");
		String email = myRs.getString("email");
		String nr_tel = myRs.getString("nr_telefonu");
		int adres_id = myRs.getInt("adresid");
		
		Klient tempKlient = new Klient(imie, nazwisko, email, nr_tel, adres_id);
		
		return tempKlient;
	}
	private static void close(Connection myConn, Statement myStmt1, ResultSet myRs)
			throws SQLException {

		if (myRs != null) {
			myRs.close();
		}

		if (myStmt1 != null) {
			
		}
		
		if (myConn != null) {
			myConn.close();
		}
	}
	
	private void close(Statement myStmt1, ResultSet myRs) throws SQLException {
		close(null, myStmt1, myRs);		
	}
}
