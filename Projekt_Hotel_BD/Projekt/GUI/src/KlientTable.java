import java.util.List;

import javax.swing.table.AbstractTableModel;

public class KlientTable extends AbstractTableModel {
	private String[] nazwy_kolumn = {"Imie", "Nazwisko","E-mail","nr_telefonu","adresId"};
	private List<Klient> klienci;
	public KlientTable(List<Klient> klienty) {
		klienci = klienty;
	}
	public int getColumnCount() {
		return nazwy_kolumn.length;
	}
	public int getRowCount() {
		return klienci.size();
	}
	public String getColumnName(int col) {
		return nazwy_kolumn[col];
	}
	
	public Object getValueAt(int row, int col) {
		Klient tempKlient = klienci.get(row);
		switch(col) {
		case 0:
			return tempKlient.getImie();
		case 1:
			return tempKlient.getNazwisko();
		case 2:
			return tempKlient.getNr_tel();
		case 3:
			return tempKlient.getEmail();
		case 4:
			return tempKlient.getAdresid();
		default:
			return tempKlient.getNazwisko();
		}
	}
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}
}
