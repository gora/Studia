import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;


public class ListaKlientow extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	//private String[] nazwy_kolumn = {"ID", "Imie", "Nazwisko","E-mail","Numer tel.","AdresId"};
	//private Object[][] data = {{"roman", "kowalski", "romankowalski420@interia.pl", "6828737417"}};
	//private List<Klient> klienci;
	private JTable table;
	private KlientDAO klientDAO;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ListaKlientow dialog = new ListaKlientow();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Create the dialog.
	 */
	public ListaKlientow() {
		try {
			klientDAO = new KlientDAO();
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE);
		}
		setTitle("Lista Klient\u00F3w");
		setBounds(100, 100, 538, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblPodajNazwisko = new JLabel("Podaj Nazwisko");
			lblPodajNazwisko.setBounds(93, 14, 74, 14);
			contentPanel.add(lblPodajNazwisko);
		}
		{
			textField = new JTextField();
			textField.setBounds(172, 11, 86, 20);
			contentPanel.add(textField);
			textField.setColumns(10);
		}
		{
			JButton btnSzukaj = new JButton("Szukaj");
			btnSzukaj.setBounds(263, 10, 63, 23);
			btnSzukaj.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					List<Klient> klient = null;
					try {
						klient = klientDAO.getAllKlient();
						KlientTable model = new KlientTable(klient);
						table.setModel(model);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
			contentPanel.add(btnSzukaj);
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 45, 502, 173);
			contentPanel.add(scrollPane);
			{
				//table = new JTable(data, nazwy_kolumn);
				scrollPane.setViewportView(table);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
