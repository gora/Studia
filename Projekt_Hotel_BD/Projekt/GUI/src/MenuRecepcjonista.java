import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuRecepcjonista {

	JFrame frmMenu;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuRecepcjonista window = new MenuRecepcjonista();
					window.frmMenu.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public MenuRecepcjonista() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMenu = new JFrame();
		frmMenu.setTitle("MENU");
		frmMenu.setBounds(100, 100, 450, 300);
		frmMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMenu.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Nowa Rezerwacja");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NowaRezerwacja nwrez = new NowaRezerwacja();
				nwrez.setVisible(true);
			}
		});
		btnNewButton.setBounds(159, 21, 119, 23);
		frmMenu.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Lista Rezerwacji");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setBounds(159, 55, 119, 23);
		frmMenu.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Lista Klient\u00F3w");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_2.setBounds(159, 89, 119, 23);
		frmMenu.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Lista Pokoi");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_3.setBounds(159, 123, 119, 23);
		frmMenu.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Wyloguj");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_4.setBounds(159, 191, 119, 23);
		frmMenu.getContentPane().add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("Meldowanie");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_5.setBounds(159, 157, 119, 23);
		frmMenu.getContentPane().add(btnNewButton_5);
	}

}
