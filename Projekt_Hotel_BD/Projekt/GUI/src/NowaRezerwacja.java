import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;

public class NowaRezerwacja extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private CallableStatement myStmt;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			NowaRezerwacja dialog = new NowaRezerwacja();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public NowaRezerwacja() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nr pokoju");
		lblNewLabel.setBounds(10, 11, 54, 14);
		contentPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Dane o rezyd.");
		lblNewLabel_1.setBounds(10, 111, 77, 14);
		contentPanel.add(lblNewLabel_1);
		
		JLabel lblIdentyfikator = new JLabel("status");
		lblIdentyfikator.setBounds(10, 36, 69, 14);
		contentPanel.add(lblIdentyfikator);
		
		textField = new JTextField();
		textField.setBounds(104, 8, 123, 20);
		contentPanel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(104, 33, 123, 21);
		contentPanel.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnListaKlientw = new JButton("Lista Klient\u00F3w");
		btnListaKlientw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ListaKlientow lista = new ListaKlientow();
				lista.setVisible(true);
			}
		});
		btnListaKlientw.setBounds(289, 57, 109, 23);
		contentPanel.add(btnListaKlientw);
		
		JLabel lblDataMeldunku = new JLabel("Pocz\u0105tek");
		lblDataMeldunku.setBounds(10, 61, 77, 14);
		contentPanel.add(lblDataMeldunku);
		
		JLabel lblDataWymeldowania = new JLabel("Koniec");
		lblDataWymeldowania.setBounds(10, 86, 100, 14);
		contentPanel.add(lblDataWymeldowania);
		
		textField_2 = new JTextField();
		textField_2.setBounds(104, 58, 123, 20);
		contentPanel.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(104, 83, 123, 20);
		contentPanel.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnNewButton = new JButton("Lista Rezerwacji");
		btnNewButton.setBounds(289, 82, 109, 23);
		contentPanel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Nowy Klient");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Nowy_Klient nwk = new Nowy_Klient();
				nwk.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(289, 7, 109, 23);
		contentPanel.add(btnNewButton_1);
		
		textField_4 = new JTextField();
		textField_4.setBounds(103, 108, 124, 20);
		contentPanel.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblIdKlienta = new JLabel("Id Klienta");
		lblIdKlienta.setBounds(10, 136, 46, 14);
		contentPanel.add(lblIdKlienta);
		
		textField_5 = new JTextField();
		textField_5.setBounds(104, 133, 123, 20);
		contentPanel.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblCena = new JLabel("Cena:");
		lblCena.setBounds(10, 161, 46, 14);
		contentPanel.add(lblCena);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBounds(50, 161, 46, 14);
		contentPanel.add(lblNewLabel_2);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				        java.util.Date dt;
				        java.util.Date dk;
				        java.sql.Date pocz = null;
				        java.sql.Date kon = null;
						try {
							dt = format.parse(textField_2.getText());
							dk = format.parse(textField_3.getText());
							pocz = new Date(dt.getTime());
							kon = new Date(dk.getTime());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}   
						int nrpok = Integer.parseInt(textField.getText());
						String status = textField_1.getText();
						String dane_o_rez = textField_4.getText();
						int idk = Integer.parseInt(textField_5.getText());
						try {
							myStmt = Logowanie.myConn.prepareCall("{call dodaj_rezerwacje(?,?,?,?,?,?,?,?)}");
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							myStmt.setInt(1,nrpok);
							myStmt.setString(2,status);
							myStmt.setDate(3,pocz);
							myStmt.setDate(4,kon);
							myStmt.setString(5,dane_o_rez);
							myStmt.setInt(6,300);
							myStmt.setInt(7, idk);
							myStmt.setInt(8, 1);
							myStmt.execute();
							
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
