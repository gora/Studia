used_locations = []


def gameOver(board, let):
    if board[0][0] == board[0][1] == board[0][2] == let or \
            board[1][0] == board[1][1] == board[1][2] == let or \
            board[2][0] == board[2][1] == board[2][2] == let or \
            board[0][0] == board[1][1] == board[2][2] == let or \
            board[0][2] == board[1][1] == board[2][0] == let or \
            board[0][0] == board[1][0] == board[2][0] == let or \
            board[0][1] == board[1][1] == board[2][1] == let or \
            board[0][2] == board[1][2] == board[2][2] == let :
        print("Congratulations!\n")
        return True

    if 9 == sum((pos == 'X' or pos == 'O') for pos in board):
        print("The game ends in a tie\n")
        return True


def getComputerMove(board, let):
    if let == 'X':
        player = 'O'
    else:
        player = 'X'

    for i in range(0, 3):
        for j in range(0, 3):
            if board[i][j] == '.':
                board[i][j] = let
                return;

                # if gameOver(board,let):
                #     return board[i][j]
                # else:
                #     break

    # for i in range(0, 3):
    #     for j in range(0, 3):
    #         if board[i][j] == '.':
    #             # board[i][j] = player
    #             if gameOver(board, player):
    #                 return board[i][j]

    # for i in range(0, 3):
    #     for j in range(0, 3):
    #         if board[i][j] == '.':
    #             return board[i][j]

def initialize_board():
    board = []

    for i in range(0, 3):
        row = []
        for j in range(0, 3):
            row.append(".")
        board.append(row)

    return board


def print_board(board):
    for i in range(0, 3):
        for j in range(0, 3):
            # print(board[i][j], end="\t")
            print(board[i][j]),
        print("\n")


def is_location_available(board, i, j):
    if i == -1 and j == -1:
        return False
    elif board[i][j] != ".":
        return False
    return True


def is_coordinate_valid(coordinate):
    if 0 <= coordinate < 3:
        return True
    return False


def ask_for_input(board):
    print("Which location you want to choose?")
    x = -1
    y = -1

    while not is_location_available(board, x, y):
        while not is_coordinate_valid(x):
            x = int(input("Enter x coordinate: "))
        while not is_coordinate_valid(y):
            y = int(input("Enter y coordinate: "))

    return [x, y]


def player_move(board, player_sign):
    location = ask_for_input(board)
    board[location[0]][location[1]] = player_sign


array = initialize_board()
print_board(array)
# print(ask_for_input(array))
while True:
    player_move(array, 'X')
    # print_board(array)
    if gameOver(array, 'X'):
        print_board(array)
        print("\nPLAYER won")
        break
    getComputerMove(array, 'O')
    print_board(array)
    if gameOver(array, 'O'):
        print_board(array)
        print("\nCOMPUTER won")
        break

